import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()


with open(os.path.join(here, 'requirements.txt')) as f:
    req_lines = [line.strip() for line in f.readlines()]

    def fix_url_line(line):
        if '#egg=' in line:
            return line.rsplit('#egg=', 1)[1]
        return line
    requires = [fix_url_line(line) for line in req_lines if line]


setup(
    name='kaiju',
    version='0.0.5',
    description='Kaiju - tools for async python',
    long_description=README + '\n\n' + CHANGES,
    long_description_content_type='text/x-rst',
    author='',
    author_email='',
    url='',
    packages=find_packages(),
    package_dir={'kaiju': 'kaiju'},
    include_package_data=True,
    python_requires=">=3.7.0",
    install_requires=requires,
    license='',
    zip_safe=False,
    classifiers=(
        'Programming Language :: Python :: 3.7',

    )
)