import asyncio

import aiormq
from rapidjson import *

from kaiju.rpc.rpc import JSONQueueServer, AbstractRPCClientService
from kaiju.rpc.jsonrpc import *
from .pool import RabbitMQPoolService

__all__ = ['RMQRPCQueueServer']


class RMQRPCQueueServer(JSONQueueServer):

    conn_exception_classes = (
        ConnectionError, TimeoutError, RuntimeError,
        aiormq.ConnectionChannelError, asyncio.streams.IncompleteReadError)

    APP_ID_HEADER = 'app_id'
    REPLY_TO_HEADER = 'reply_to'
    CORRELATION_ID_HEADER = 'correlation_id'
    DEADLINE_HEADER = 'request_deadline'
    SERVER_ID_HEADER = 'server_id'
    CONTENT_TYPE_HEADER = 'content_type'
    REQUEST_TIMEOUT_HEADER = 'request_timeout'

    def __init__(
            self, *args, exchange: str, queue: str, pool: RabbitMQPoolService,
            exchange_settings: dict = None, queue_settings: dict = None,
            auto_ack=True, consumer_id=None, **kws):
        """
        :param args:
        :param exchange: exchange name
        :param queue: queue name
        :param pool:
        :param exchange_settings:
        :param queue_settings:
        :param auto_ack:
        :param consumer_id:
        :param kws:
        """

        super().__init__(*args, **kws)
        if type(pool) is str:
            pool = self.app.services[pool]
        self._pool = pool

        if consumer_id is None:
            consumer_id = self.app['id']
        self.consumer_id = str(consumer_id)

        self.exchange = exchange
        self.exchange_settings = exchange_settings if exchange_settings else {}
        self.queue = queue
        self.queue_settings = queue_settings if queue_settings else {}
        self.auto_ack = auto_ack

    async def _init_exchange(self, ch: aiormq.Channel):
        self.logger.debug('Initializing an exchange "%s".', self.exchange)
        await ch.exchange_declare(self.exchange, **self.exchange_settings)
        self.logger.debug('Initialized an exchange "%s".', self.exchange)

    async def _init_queue(self, ch: aiormq.Channel):
        self.logger.debug('Initializing a queue "%s".', self.queue)
        await ch.queue_declare(self.queue, **self.queue_settings)
        await ch.queue_bind(self.queue, self.exchange, routing_key=self.queue)
        self.logger.debug('Initialized a queue "%s".', self.queue)

    async def _hook(self, message):
        headers = dict(message.header.properties)
        _headers = headers.get('headers', {})
        _headers.update(headers)
        data = loads(
            message.body, uuid_mode=UM_CANONICAL,
            datetime_mode=DM_ISO8601, number_mode=NM_DECIMAL,
            allow_nan=False)
        await self.request_queue.put((data, _headers))

    async def _loader(self):

        _init_exchange = self._init_exchange
        _init_queue = self._init_queue

        pool = self._pool
        queue = self.queue
        hook = self._hook
        auto_ack = self.auto_ack
        sleep = asyncio.sleep

        while not self._closing:
            async with pool.acquire() as ch:
                basic_consume = ch.basic_consume
                try:
                    await _init_exchange(ch)
                    await _init_queue(ch)
                    while 1:
                        await basic_consume(queue, hook, no_ack=auto_ack)
                except self.conn_exception_classes as err:
                    if not self._closing:
                        self.logger.error(
                            'Connection error in the consumer loop. [%s]: %s',
                            err.__class__.__name__, err)
                        pool.release(ch)
                        await sleep(0.1)
                        continue

                except asyncio.CancelledError:
                    pool.release(ch)
                except Exception as err:
                    self.logger.exception(
                        'Unexpected error in the consumer loop. [%s]: %s',
                        err.__class__.__name__, err)
                    raise

                self.logger.debug('Stopped the consumer loop.')
                break

    async def _worker(self):

        request_queue = self.request_queue
        output_queue = self._output_queue
        _process_headers = self._process_headers
        _server_id = self._server_id
        REPLY_TO_HEADER = self.REPLY_TO_HEADER

        while 1:

            data, headers = await request_queue.get()
            headers, result = await self.call(data, headers)
            reply_to = str(headers.get(REPLY_TO_HEADER))

            if data and reply_to:
                try:
                    data = dumps(
                        data, uuid_mode=UM_CANONICAL, ensure_ascii=False,
                        datetime_mode=DM_ISO8601, number_mode=NM_DECIMAL,
                        allow_nan=False, default=dict).encode()
                except (ValueError, TypeError) as err:
                    self.logger.error('Unprocessable response: "%s".', data)
                    await output_queue.put(
                        RPCError(
                            None, 'Internal server error. Unprocessable response: "%s".',
                            base_exc=err), headers, reply_to)
                else:
                    await output_queue.put((data, headers, reply_to))

            request_queue.task_done()

    async def _sender(self):

        output_queue = self._output_queue
        pool = self._pool
        Properties = aiormq.spec.Basic.Properties
        conn_exceptions = self.conn_exception_classes
        _server_id = self._server_id
        exchange = self.exchange
        correlation_id_headers = self.CORRELATION_ID_HEADER

        while 1:
            try:
                async with pool.acquire() as ch:
                    await self._init_exchange(ch)
                    basic_publish = ch.basic_publish
                    while 1:
                        data, headers, reply_to = await output_queue.get()
                        correlation_id = headers.get(correlation_id_headers)
                        prop = Properties(
                            app_id=_server_id,
                            content_type='application/json',
                            content_encoding='utf-8',
                            correlation_id=correlation_id)
                        await basic_publish(
                            data, exchange=exchange,
                            routing_key=reply_to, properties=prop)
            except conn_exceptions as err:
                self.logger.error(
                    'Connection error in a response loop. [%s]: %s',
                    err.__class__.__name__, err)
                pool.release(ch)
                output_queue.task_done()
                if self._closing:
                    break
                await output_queue.put((data, headers, reply_to))
                continue


class RMQRPCClient(AbstractRPCClientService, RMQRPCQueueServer):

    async def init_session(self):
        """This method should initialize a session or a pool upon init."""

        await RMQRPCQueueServer.init(self)

    @property
    def closed(self):
        return RMQRPCQueueServer.closed.fget(self)

    async def close_session(self):
        """This method should close the session if required."""

        await RMQRPCQueueServer.close(self)

    async def authenticate(self):
        """This method should perform authorization upon a service init.
        Leave it if no authorization is required."""

        pass
