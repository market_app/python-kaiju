import asyncio
from weakref import proxy
from typing import *

import aiormq

from kaiju.abc import Loggable, Contextable
from kaiju.services import ContextableService
from kaiju.helpers import repeat

__all__ = ['Pool', 'RabbitMQPoolService']


class _PoolAcquireCtx:
    """Connection context. Used in pool.acquire() command."""

    def __init__(self, pool):
        self._pool = proxy(pool)
        self._ch = None

    async def __aenter__(self):
        self._ch = ch = await self._pool.get()
        return ch

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self._pool.release(self._ch)
        self._ch = None


class Pool(Loggable, Contextable):
    """RabbitMQ connection pool.

    How to use:
    ***********

    .. code-block:: python

        async with Pool('amqp://guest:guest@localhost/', max_size=5, logger=my_service_logger) as pool:
            async with pool.acquire() as ch:
                ...

    """

    __slots__ = (
        '_min_size', '_max_size', '_dsn', '_connection', '_channels',
        '_available', '_conn_retries', '_retry_timeout', '_channel_task',
        'logger'
    )

    conn_exception_classes = (
        ConnectionError, TimeoutError, RuntimeError,
        asyncio.streams.IncompleteReadError,
        aiormq.exceptions.IncompatibleProtocolError
    )

    def __init__(
            self, dsn: str, min_size=2, max_size=16, conn_retries=3,
            retry_timeout=1, logger=None):
        """
        :param dsn: RabbitMQ connection string:
            "amqp://guest:guest@localhost/" etc.
        :param min_size: min (default) open channels
        :param max_size: max open channels
        :param conn_retries: number of retries when acquiring connection
            -1 for infinite
        :param retry_timeout: timeout (sec) between tries when connecting
        :param args:
        :param kws: here you can provide your parent logger instance for the pool
        """

        Loggable.__init__(self, logger=logger)

        max_size = int(max_size)
        if max_size <= 0:
            raise ValueError('max_size is expected to be greater than zero.')

        min_size = int(min_size)
        if min_size <= 0:
            raise ValueError(
                'min_size is expected to be greater or equal to zero.')

        if min_size > max_size:
            raise ValueError('min_size cannot be greater than max_size.')

        conn_retries = int(conn_retries)
        if conn_retries < 0 and conn_retries != -1:
            raise ValueError(
                'Connection retries must be greater or equal than zero or -1'
                ' for infinite tries.')

        retry_timeout = float(retry_timeout)
        if retry_timeout <= 0:
            raise ValueError('Retry timeout must be greater than zero.')

        self._min_size, self._max_size = min_size, max_size
        self._dsn = dsn
        self._conn_retries = conn_retries
        self._retry_timeout = retry_timeout

        self._connection = None   # aiormq connection object
        self._channels = []       # all channels
        self._available = []      # not used channels
        self._closing = False      # indicates that the pool will soon be closed
        self._channel_task = None  # task for background pool renewing

    async def init(self):
        await self._connect()
        await self._init_channels()

    async def close(self):
        self._closing = True
        if self._renew_task_is_running():
            await self._channel_task
        await asyncio.gather(*(ch.close() for ch in self.opened_channels))
        await self._connection.close()
        self._channels, self._available = [], []
        self._connection, self._channel_task = None, None
        self._closing = False

    @property
    def closed(self):
        return self._connection is None

    @property
    def opened_channels(self) -> List[aiormq.Channel]:
        """Lists all opened channels."""
        
        return [ch for ch in self._channels if not ch.is_closed]
    
    @property
    def unused_channels(self) -> List[aiormq.Channel]:
        """Lists all available unused channels."""

        return self._available

    async def get(self) -> aiormq.Channel:
        """Get an unused channel or create one."""

        self._remove_closed_channels()
        if self._available:
            ch = self._available.pop()
            return ch
        else:
            ch = await self._create_channel()
            return ch

    def release(self, ch):
        """Releases a channel back to the pool."""

        if not ch.is_closed:
            self._available.append(ch)
        if not self._renew_task_is_running() and not self._closing:
            self._channel_task = asyncio.ensure_future(self._purge_unused_channels())

    def acquire(self):
        """Acquire context."""

        return _PoolAcquireCtx(self)

    def _renew_task_is_running(self):
        return self._channel_task and not self._channel_task.done()

    async def _renew_channels(self):
        await self._purge_unused_channels()
        await self._init_channels()

    async def _purge_unused_channels(self):
        self._remove_closed_channels()
        if len(self._channels) > self._max_size and self._available:
            dn = len(self._channels) - self._max_size
            self.logger.debug('Removing %d unused channels.', dn)
            _to_remove, self._available = self._available[:dn], self._available[dn:]
            _to_remove = await asyncio.gather(*(ch.close() for ch in _to_remove))
            self._remove_closed_channels()
            self.logger.debug('Removed %d unused channels.', dn)

    async def _init_channels(self):
        if len(self._channels) < self._min_size:
            dn = self._min_size - len(self._channels)
            self.logger.debug('Creating %d new unused channels.', dn)
            new = await asyncio.gather(*(self._create_channel() for _ in range(dn)))
            self._available.extend(new)
            self.logger.debug('Created %d new unused channels.', dn)

    def _remove_closed_channels(self):
        self._channels = self.opened_channels
        self._available = [ch for ch in self._available if ch in self._channels]

    async def _create_channel(self):
        """Create and register a new connection channel."""

        self.logger.debug('Creating a new RabbitMQ channel.')
        if self._closing:
            raise ValueError('Pool is closing.')
        try:
            ch = await self._connection.channel()
        except self.conn_exception_classes as err:
            self.logger.error(
                'An error while creating a new channel. [%s]: %s.',
                err.__class__.__name__, err)
            if self._connection is None or self._connection.is_closed:
                await self._connect()
            ch = await self._connection.channel()
        self._channels.append(ch)
        self.logger.debug('Created a new RabbitMQ channel.')
        return ch

    async def _connect(self):
        """Establishes a new connection to an RMQ server."""

        self.logger.debug('Trying to connect to a RabbitMQ pool...')
        if self._conn_retries:
            conn = await repeat(
                aiormq.connect, self._conn_retries, self._dsn,
                exception_classes=self.conn_exception_classes,
                retry_timeout=self._retry_timeout)
        else:
            conn = await aiormq.connect(self._dsn)
        self.logger.info('Connected to a RabbitMQ pool.')
        self._connection = conn

    def __repr__(self):
        return f'Pool({self._dsn}, min_size={self._min_size},' \
               f' max_size={self._max_size}, conn_retries={self._conn_retries},' \
               f' retry_timeout={self._retry_timeout}, logger={self._logger})'

    def __len__(self):
        return len(self._channels)


class RabbitMQPoolService(ContextableService, Pool):

    service_name = 'rabbitmq_pool'

    def __init__(self, app, *args, logger=None, **kws):
        ContextableService.__init__(self, app=app, logger=logger)
        Pool.__init__(self, *args, **kws)
