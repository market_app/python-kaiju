import abc

from kaiju.services import ContextableService
from .service import RPCService

__all__ = [
    'AbstractRPCCompatible', 'AbstractRPCCompatibleService'
]


class AbstractRPCCompatible(abc.ABC):
    """An abstract RPC compatible service."""

    @property
    @abc.abstractmethod
    def namespace(self) -> str:
        """A string representing a service unique namespace."""

    @property
    @abc.abstractmethod
    def routes(self) -> dict:
        """A dictionary of service RPC routes."""


class AbstractRPCCompatibleService(AbstractRPCCompatible, ContextableService, abc.ABC):

    @property
    def namespace(self) -> str:
        return self.service_name

    async def init(self):
        await super().init()
        if RPCService.service_name in self.app:
            self.app[RPCService.service_name].register_namespace(
                self.namespace, self.routes)
