"""Tests for RabbitMQ simple RPC client.

**Requires docker to run.**
"""

import asyncio

import pytest

from ..pool import Pool
from kaiju.rpc.jsonrpc import *
from ..client import *
from .fixtures import *


@pytest.mark.integration
def test_client_ctx_sigint(rabbit_test_dsn, random_string, run_in_process):

    class _Client(AbstractRMQRPCClient):

        async def on_response(self, headers, responses):
            pass

        async def on_request(self, headers, request):
            pass

    async def _test_client_ctx():
        async with Pool(rabbit_test_dsn) as pool:
            exchange = random_string(8)
            async with _Client(pool, exchange, auto_delete_exchange=True) as publisher:
                async with _Client(pool, exchange, auto_delete_exchange=True, queues=['test']) as consumer:
                    asyncio.ensure_future(publisher.publish('test', RPCRequest(None, 'do.nothing', [1, 2, 3])))
                    await asyncio.sleep(1)

    run_in_process(_test_client_ctx, 1)


@pytest.mark.integration
async def test_client(rabbit_test_dsn, random_string, logger):
    """This test creates two independent clients bound to each other callbacks.

    A publisher posts message to its queue. A consumer receives the message and
    pushes it back to the publisher callback queue.

    Also this test will check if basic message formatters are compatible with
    JSON RPC 2.0 spec.
    """

    class Publisher(AbstractRMQRPCClient):
        """If a callback is received it stores it in a class variable"""

        responses = []

        async def on_response(self, headers, responses):
            self.responses.append(responses)

        async def on_request(self, headers, request):
            pass

    class Consumer(AbstractRMQRPCClient):
        """I method is wrong: returns a ValueError. Otherwise will return
        method arguments back to the request producer.
        """

        async def on_request(self, headers, request):
            if request.method == 'wrong':
                raise ValueError('method is wrong!')
            else:
                return RPCResponse(request.id, request.params)

        async def on_response(self, headers, responses):
            pass

    async with Pool(rabbit_test_dsn, logger=logger) as pool:
        exchange = random_string(8)
        async with Publisher(
            pool, exchange, auto_delete_exchange=True, logger=logger
        ) as publisher:
            async with Consumer(
                pool, exchange, auto_delete_exchange=True, callback='test',
                logger=logger
            ) as consumer:

                logger.info('send message')

                method, params = 'test', [1, 2, 3]
                request = RPCRequest(None, method, params)
                await asyncio.sleep(0.01)
                asyncio.ensure_future(publisher.publish('test', request))

                logger.info('send message back to the publisher')

                await asyncio.sleep(1)

                assert len(publisher.responses) == 1
                response = publisher.responses[-1]
                assert isinstance(response, RPCResponse)
                assert response.id == request.id
                assert response.result == request.params

                logger.info('testing batch request with normal requests and invalid requests')

                publisher.responses = []
                method, params = 'test', [1, 2, 3]
                normal_request = RPCRequest(None, method, params)
                bad_request = RPCRequest(None, 'wrong', params)
                request = [normal_request, bad_request]

                logger.info('send a batch and test if ids contain all request IDs')

                id = await publisher.publish(
                    consumer.callback, request,
                    correlation_id=normal_request.uuid)
                assert normal_request.uuid == id

                await asyncio.sleep(1)

                logger.info('batch should be returned as is')

                response = publisher.responses[-1]
                assert len(response) == len(request)

                logger.info(' method should return one invalid request error'
                            ' and one value error raised from `on_message`')

                error = next(
                    r for r in response
                    if isinstance(r, RPCError))

                logger.info('testing error traceback')

                err = eval(error.data['traceback']['repr'])
                assert isinstance(err, ValueError)

                assert error.id == bad_request.id

                logger.info('finished tests')
