"""Tests for RabbitMQ simple RPC service.

**Requires docker to run.**
"""

import asyncio
from time import time

import pytest

from ..pool import Pool
from ..service import RPCService
from .fixtures import *


@pytest.mark.int_benchmark
def test_rmq_rpc_performance(rabbit_test_dsn, random_string, performance_test, logger):
    """This test will create a simple RPC server and an RPC client and
    run a several requests in parallel. It prints total number of requests,
    time taken to process them and an RPS value.
    """

    async def _test_rpc_ctx(parallel=1, requests=1000):

        logger.setLevel('ERROR')

        counter = 0

        async def _do_call(c, s):
            nonlocal counter
            while 1:
                await c.call('server', 'do.sleep', (1, 2, 3))
                counter += 1

        def _do_sleep(*_, **__):
            return True

        methods = {
            'do': {
                'sleep': _do_sleep
            }
        }

        async with Pool(rabbit_test_dsn, logger=logger) as pool:
            exchange = random_string(8)
            async with RPCService(
                    pool=pool, exchange=exchange, queues=['server'], auto_ack=True,
                    unidentified_response_hist_size=0,
                    auto_delete_exchange=True, methods=methods,
                    task_cleaner_interval=10, senders=20,
                    processors=64, logger=logger
            ) as server:
                async with RPCService(
                    pool=pool, exchange=exchange, unidentified_response_hist_size=0,
                    auto_delete_exchange=True, max_request_timeout=5,
                    task_cleaner_interval=10, auto_ack=True,
                    senders=1, processors=64, logger=logger
                ) as client:
                    tasks = [
                        asyncio.ensure_future(_do_call(client, server))
                        for _ in range(parallel)
                    ]
                    t0 = time()
                    while counter < requests:
                        await asyncio.sleep(3)
                    t1 = time()
                    for task in tasks:
                        task.cancel()
                    return t1 - t0, counter

    requests, parallel, n = 5000, 64, 4
    print(f' RMQ JSON RPC Service simple client-server benchmark (best of {n}).\n')
    print(f'{parallel} connections')

    dt, counter, rps = performance_test(
        _test_rpc_ctx, runs=n, args=(parallel, requests))

    print(f'{dt}')
    print(f'{counter} requests')
    print(f'{rps} req/sec')


@pytest.mark.integration
def test_rmq_rpc_ctx_sigint(rabbit_test_dsn, random_string, run_in_process):

    async def _test_rpc_ctx():

        async def _do_sleep(*_, **__):
            await asyncio.sleep(0.01)

        methods = {
            'do': {
                'sleep': _do_sleep
            }
        }

        async with Pool(rabbit_test_dsn) as pool:
            exchange = random_string(8)
            async with RPCService(
                    pool=pool, exchange=exchange,
                    unidentified_response_hist_size=10,
                    auto_delete_exchange=True, max_request_timeout=5
            ) as client:
                async with RPCService(
                        pool=pool, exchange=exchange, queues=['server'],
                        unidentified_response_hist_size=10,
                        auto_delete_exchange=True, methods=methods,
                        task_cleaner_interval=0.1) as server:

                    while 1:
                        asyncio.ensure_future(client.call('server', 'do.sleep', (1, 2, 3)))
                        asyncio.ensure_future(client.call(server.callback, 'do.sleep', (1, 2, 3)))
                        await asyncio.sleep(0.01)

    run_in_process(_test_rpc_ctx, 1)


@pytest.mark.integration
async def test_rmq_rpc_service(rabbit_test_dsn, random_string, logger):

    def a_method(x, y):
        return x * y

    async def big_method(x, y):
        await asyncio.sleep(2)
        return x * y

    async def bad_method(x, y):
        raise ValueError('shit')

    async def long_method(x, y):
        await asyncio.sleep(10)
        return x * y

    methods = {
        'segment_1': {
            'method_1': a_method,
            'big_method': big_method,
            'bad_method': bad_method,
            'long_method': long_method
        }
    }

    async with Pool(rabbit_test_dsn, logger=logger) as pool:
        exchange = random_string(8)
        async with RPCService(
            pool=pool, exchange=exchange, unidentified_response_hist_size=10,
            auto_delete_exchange=True, max_request_timeout=5,
            logger=logger
        ) as client:
            async with RPCService(
                pool=pool, exchange=exchange, queues=['server'],
                unidentified_response_hist_size=10,
                auto_delete_exchange=True, methods=methods,
                task_cleaner_interval=0.1, logger=logger
            ) as server:

                x, y = 5, 6
                calls = [['segment_1.method_1', [x, n]] for n in range(5)]

                logger.info('normal call')

                await asyncio.sleep(0.1)
                result = await client.call('server', 'segment_1.method_1', [x, y])
                assert result == x * y

                logger.info('nowait call')

                await asyncio.sleep(0.1)
                request_id = await client.call_nowait(
                    'server', 'segment_1.method_1', [x, y], do_not_reply=False)
                headers, response = await client.unidentified_responses.get()
                assert response.uuid == request_id
                assert response.result == x * y

                logger.info('nowait multi-call')

                await asyncio.sleep(0.1)
                request_id = await client.call_multiple_nowait(
                    'server', calls, do_not_reply=False)
                headers, response = await client.unidentified_responses.get()
                assert response[0].uuid == request_id
                for call, r in zip(calls, response):
                    _x, _y = call[1]
                    assert r.result == _x * _y

                logger.info('testing multi-call')

                await asyncio.sleep(0.1)
                response = await client.call_multiple('server', calls)
                for call, r in zip(calls, response):
                    _x, _y = call[1]
                    assert r == _x * _y

                logger.info('testing unidentified queue size compaction')

                await asyncio.sleep(0.1)
                calls = [['method_1', [x, n]] for n in range(25)]
                await client.call_multiple_nowait('server', calls)
                await asyncio.sleep(0.1)
                assert client.unidentified_responses.qsize() <= client._unidentified_max_size

                logger.info('testing timeout')

                await asyncio.sleep(0.1)
                with pytest.raises(TimeoutError):
                    await client.call(
                        'server', 'segment_1.big_method', [x, y],
                        request_timeout=1)

                logger.info('testing invalid method args')

                await asyncio.sleep(0.1)
                with pytest.raises(TypeError):
                    await client.call('server', 'segment_1.method_1', [x])

                logger.info('testing invalid method')

                await asyncio.sleep(0.1)
                with pytest.raises(NameError):
                    await client.call('server', 'segment_1.method_unknown', [x, y])

                logger.info('testing bad method')

                with pytest.raises(ValueError):
                    await asyncio.sleep(0.1)
                    calls = [
                        ('segment_1.bad_method', [1, 2]),
                        ('segment_1.bad_method', [1, 2]),
                        ('segment_1.bad_method', [1, 2])
                    ]
                    await client.call_multiple('server', calls)

                logger.info('testing task cancellation')

                id = await client.call_nowait(
                    'server', 'segment_1.long_method', [x, y], request_timeout=10)
                result = await client.call('server', '.abort', {'id': id})
                assert result['id'] == id

                logger.info('testing other special commands')

                methods = await client.call('server', '.list')
                jobs = await client.call('server', '.jobs')
                get_help = await client.call('server', '.help', {'method': 'rpc.abort'})

                logger.info('finished tests')
