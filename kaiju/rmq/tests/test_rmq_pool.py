"""Tests for RabbitMQ connection pool and other connection functions.

**Requires docker to run.**
"""

import asyncio

import aiormq
import pytest

from kaiju.json import encoder, decoder

from .. import Pool
from .fixtures import *


@pytest.mark.integration
def test_pool_ctx_sigint(rabbit_test_dsn, random_string, run_in_process):

    async def _test_pool_ctx():

        async def on_message(msg):
            return

        async with Pool(rabbit_test_dsn) as pool:
            async with pool.acquire() as channel:
                qname = random_string(8)
                await channel.exchange_declare(
                    durable=False, auto_delete=True,
                    exchange='pytest', exchange_type='direct')
                await channel.queue_declare(
                    qname, durable=False, auto_delete=True, exclusive=True)
                await channel.queue_bind(
                    queue=qname, exchange='pytest',
                    routing_key=qname)
                msg = {'test': True}
                msg = encoder(msg).encode()
                await channel.basic_publish(
                    msg, exchange='pytest', routing_key=qname,
                    properties=aiormq.spec.Basic.Properties(
                        content_type='application/json',
                        content_encoding='utf-8'))
                await channel.basic_consume(qname, on_message)

    run_in_process(_test_pool_ctx, 1)


@pytest.mark.integration
async def test_pool_ctx(rabbit_test_dsn, random_string, logger):
    """Tests common aio rabbit pool workflow.

    Pool connection, pool and connection context, connection acquiring and
    release.
    """

    resp = None

    async def on_message(message):
        nonlocal resp
        resp = decoder(message.body.decode('utf-8'))

    # declaring pool with wrong arguments must rise a ValueError

    with pytest.raises(ValueError):
        Pool(rabbit_test_dsn, min_size=0)

    with pytest.raises(ValueError):
        Pool(rabbit_test_dsn, max_size=1)

    with pytest.raises(ValueError):
        Pool(rabbit_test_dsn, conn_retries=0)

    # connectivity tests

    size = 2

    async with Pool(
        rabbit_test_dsn, min_size=size, max_size=size, logger=logger
    ) as pool:
        async with pool.acquire() as channel:

            assert len(pool) == size

            logger.info('connection testing')

            qname = random_string(8)

            await channel.exchange_declare(
                durable=False, auto_delete=True,
                exchange='pytest', exchange_type='direct')
            await channel.queue_declare(
                qname, durable=False, auto_delete=True, exclusive=True)
            await channel.queue_bind(
                queue=qname, exchange='pytest',
                routing_key=qname)

            msg = {'test': True}
            msg = encoder(msg).encode()

            await channel.basic_publish(
                msg, exchange='pytest', routing_key=qname,
                properties=aiormq.spec.Basic.Properties(
                    content_type='application/json',
                    content_encoding='utf-8'))

            await channel.basic_consume(qname, on_message, no_ack=True)

        assert resp['test'] is True

        logger.info('checking that pool is still available and uses existing connection')

        async with pool.acquire() as ch1:
            async with pool.acquire() as ch2:
                async with pool.acquire() as ch3:
                    async with pool.acquire() as ch4:
                        assert len(pool) == max(size, 4)
                        await asyncio.sleep(0.1)

        logger.info('checking that pool actually closes channels exceeding its max limit')

        await asyncio.sleep(0.5)  # the pool won't let connection too fast
        assert len(pool) == size

        logger.info('finished tests')
