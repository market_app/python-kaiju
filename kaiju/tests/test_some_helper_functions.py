from collections import deque
from time import time

import pytest

from kaiju.helpers import *


@pytest.mark.benchmark
def test_helper_performance(performance_test):

    def _test_hash(num, size):
        d = {str(k): v for k, v in zip(range(size), range(size))}
        t0 = time()
        deque((hash_dict(d) for n in range(num)), maxlen=0)
        t1 = time()
        return t1 - t0, num

    def _test_templates(num):
        template = {
            "id": "[client_id]", "tag": "testing",
            "name": "f[Hello, {some_name}!]", "l": ["[a]", "[b]", "[a]"]
        }
        data = [{
            "client_id": n,
            "some_name": "shiteman",
            "a": "Sobaki",
            "b": "Koty",
            "c": "Unknown"
        } for n in range(num)]
        t0 = time()
        deque((fill_template(template, d) for d in data), maxlen=0)
        t1 = time()
        return t1 - t0, num
    
    num, runs = 3000, 3
    sizes = [10, 100, 1000]

    print(f'\n\nTesting helpers performance ({num} cycles, {runs} runs)...')

    print('\nDict hashing:')
    for size in sizes:
        dt, counter, rps = performance_test(_test_hash, args=(num, size), runs=runs)
        print(f'\t{size:>4} {rps:>10} sec^-1 {dt}')

    print('\nTemplates:')
    dt, counter, rps = performance_test(_test_templates, args=(num,), runs=runs)
    print(f'\t{rps:>10} sec^-1 {dt}')

    print()

