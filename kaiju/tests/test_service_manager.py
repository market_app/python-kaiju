import pytest

from kaiju.services import ServiceContextManager, Service, ContextableService


@pytest.mark.unit
async def test_service_context_manager_basic_functions(
        aiohttp_server, web_application, logger):

    class SimpleUnnamedService(Service):

        def __init__(self, x, *args, **kws):
            super().__init__(*args, **kws)
            self.x = x

        def __call__(self, *args, **kwargs):
            return self.x

    class SimpleUnnamedServiceWithDefaults(Service):

        def __init__(self, x, *args, **kws):
            super().__init__(*args, **kws)
            self.x = x

        def __call__(self, *args, **kwargs):
            return self.x

        @classmethod
        def service_defaults(cls) -> dict:
            return {
                'name': 'default_name'
            }

    class _ContextableService(ContextableService):

        service_name = 'contextable_service'

        def __init__(self, x, *args, **kws):
            super().__init__(*args, **kws)
            self.x = x
            self.y = None

        async def init(self):
            self.y = self.x

        async def close(self):
            self.y = None

        def call(self, *args, **kwargs):
            return self.y

    class ContextableFailedService(_ContextableService):

        service_name = 'contextable_failed'

        def __init__(self, *args, **kws):
            super().__init__(*args, **kws)
            self.y = 42

        async def init(self):
            raise ValueError()

    class ContextableDoubleFailedService(ContextableFailedService):

        service_name = 'contextable_double_failed'

        async def close(self):
            raise ValueError()

    settings = [
        {
            'cls': 'SimpleUnnamedService',
            'settings': {
                'x': 42
            }
        },
        {
            'cls': SimpleUnnamedServiceWithDefaults,
            'settings': {
                'x': 42
            }
        },
        {
            'cls': SimpleUnnamedService,
            'name': 'another_simple_unnamed',
            'settings': {
                'x': 43
            }
        },
        {
            'cls': _ContextableService,
            'settings': {
                'x': 44
            }
        },
        {
            'cls': ContextableFailedService,
            'required': False,
            'settings': {
                'x': 44
            }
        },
        {
            'cls': ContextableDoubleFailedService,
            'required': False,
            'settings': {
                'x': 44
            }
        },
        {
            'cls': 'SimpleUnnamedService',
            'name': 'unregistered',
            'registered': False,
            'settings': {
                'x': 42
            }
        }
    ]

    ServiceContextManager.register_class(SimpleUnnamedService)
    manager = ServiceContextManager(web_application, settings)

    # testing repr

    manager = ServiceContextManager(web_application, **manager.repr())

    # testing app initialization

    web_application.cleanup_ctx.extend(manager)

    await aiohttp_server(web_application)

    # checking that all needed services were registered
    assert web_application.SimpleUnnamedService is web_application.services.SimpleUnnamedService
    assert web_application['SimpleUnnamedService'] is web_application.services.SimpleUnnamedService

    # checking all services are OK
    assert web_application.services.SimpleUnnamedService() == 42
    assert web_application.default_name() == 42
    assert web_application.services.another_simple_unnamed() == 43
    assert web_application.services.contextable_service.call() == 44
    assert 'unregistered' not in web_application
