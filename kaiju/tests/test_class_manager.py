import pytest

from kaiju.abc import AbstractClassManager


@pytest.mark.unit
async def test_service_context_manager_basic_functions(logger):

    class Base:
        pass

    class Some(Base):

        def __init__(self, x, y):
            self.x = x
            self.y = y

    class Other:

        def __init__(self, x, y):
            self.x = x
            self.y = y

    logger.info('Testing basic operation.')

    class Manager(AbstractClassManager):
        _class = Base

    Manager.register_class(Some)

    logger.info('Testing failures.')

    with pytest.raises(TypeError):
        Manager.register_class(Other)

    with pytest.raises(TypeError):
        Manager.register_class(Base)

    manager = Manager()

    logger.info('Testing getters.')

    assert Manager.Some is manager['Some']
    assert Manager.Some is manager.Some
    assert Manager.Some is Some

    logger.info('Testing iteration.')

    for name, cls in Manager:
        assert cls is Some
