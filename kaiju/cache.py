import aredis

from kaiju.services import ContextableService

__all__ = ['RedisService']


class RedisService(aredis.StrictRedis, ContextableService):

    service_name = 'redis'

    def __init__(self, app, *args, logger=None, **kws):
        ContextableService.__init__(self, app=app, logger=logger)
        aredis.StrictRedis.__init__(self, *args, **kws)
