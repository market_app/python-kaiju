import logging
import logging.config

__all__ = ['init_logger', 'get_logger_settings']


def get_logger_settings(level: str):
    logger = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            "default": {
                "format": '%(asctime)s %(name)s %(levelname)-8s %(message)s',
                "datefmt": '%Y-%m-%d %H:%M:%S',
            },
        },
        "handlers": {
            "console": {
                "class": 'logging.StreamHandler',
                "formatter": 'default',
                "level": level
            },
        },
        'loggers': {
            'app': {
                'handlers': ['console'],
                'level': level,
                'propagate': True
            }
        }
    }
    return logger


def init_logger(settings) -> logging.Logger:
    logging.config.dictConfig(settings)
    logger = logging.getLogger('app')
    return logger
