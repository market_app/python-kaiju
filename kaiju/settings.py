import logging
import warnings
from argparse import ArgumentParser
from types import SimpleNamespace
from typing import *

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

from kaiju.services import ContextableService
from kaiju.helpers import recursive_update

__all__ = [
    'load_local_settings', 'create_settings_table',
    'settings', 'SettingsService'
]


def load_local_settings(root=None):
    cli_args = _get_cli_args()
    landscape = cli_args['cfg']
    cmd = cli_args['cmd']

    if cmd:
        cmd = cmd.lower()
        if landscape:
            settings_resolution_order = ['base', 'CLI', f'CLI_{cmd}', landscape, 'CLI_local', f'CLI_{cmd}_local']
        else:
            settings_resolution_order = ['base', 'CLI', f'CLI_{cmd}', 'CLI_local' f'CLI_{cmd}_local']
    else:
        if landscape:
            settings_resolution_order = ['base', landscape, 'local']
        else:
            settings_resolution_order = ['base', 'local']

    _settings = _load_local_settings(settings_resolution_order, root=root)

    if cli_args['host']:
        _settings['app_settings']['run']['host'] = cli_args['host']
    if cli_args['port']:
        _settings['app_settings']['run']['port'] = cli_args['port']
    if cli_args['path']:
        _settings['app_settings']['run']['path'] = cli_args['path']
    if cli_args['id']:
        _settings['app_settings']['id'] = cli_args['id']
    if cli_args['debug'] is not None:
        _settings['app_settings']['debug'] = cli_args['debug']
    if cli_args['loglevel']:
        loglevel = cli_args['loglevel'].upper()
        _settings['app_settings']['loglevel'] = loglevel
    _settings['app_settings']['run']['cmd'] = cmd

    return SimpleNamespace(**_settings)


def _get_default_cli_parser():
    _parser = ArgumentParser(
        prog='aiohttp web application',
        description='web application run settings'
    )
    _parser.add_argument(
        '--host', dest='host', default=None,
        help='web app host (default - from settings)'
    )
    _parser.add_argument(
        '--port', dest='port', type=int, default=None,
        help='web app port (default - from settings)'
    )
    _parser.add_argument(
        '--path', dest='path', default=None,
        help='web app socket path (default - from settings)'
    )
    _parser.add_argument(
        '--id', dest='id', default=None,
        help='web app unique id (default - from settings)'
    )
    _parser.add_argument(
        '--debug', dest='debug', default=None, type=bool,
        help='run in debug mode (default - from settings)'
    )
    _parser.add_argument(
        '--cfg', dest='cfg', default=None,
        help='web app config (default - local, fallback to base)'
    )
    _parser.add_argument(
        '--log', dest='loglevel', default=None,
        choices=tuple(logging._nameToLevel.keys())
    )
    _parser.add_argument(
        'cmd', metavar='COMMAND', default=None, nargs='?',
        help='optional management command to execute'
    )
    return _parser


def _get_cli_args(parser: ArgumentParser = None):
    if not parser:
        parser = _get_default_cli_parser()
    return parser.parse_args().__dict__


def _load_local_settings(settings_list: Iterable[str], root=None):
    if root is None:
        root = 'engine.settings'

    _settings = {}

    for setting in settings_list:

        setting = f'{root}.{setting}'

        try:
            module = __import__(setting, fromlist=['*'])
        except ModuleNotFoundError:
            #warnings.warn('%s settings file not found' % setting)
            pass
        else:
            setting = {
                k: getattr(module, k)
                for k in dir(module)
                if not k.startswith('_')
            }
            _settings.update(setting)

    return _settings


def create_settings_table(name, metadata, *columns):
    settings = sa.Table(
        name, metadata,
        sa.Column('landscape', sa_pg.VARCHAR(128), primary_key=True, nullable=False),
        sa.Column('app_id', sa_pg.VARCHAR(128), primary_key=True, nullable=True),
        sa.Column('settings', sa_pg.JSONB, nullable=False),
        * columns
    )
    return settings


settings = create_settings_table('settings', sa.MetaData())


class SettingsService(ContextableService):

    service_name = 'settings'
    table = settings

    DEFAULT_LANDSCAPE = 'dev'

    def __init__(self, app, landscape: str = DEFAULT_LANDSCAPE, logger=None):
        super().__init__(app=app, logger=logger)
        self.landscape = landscape
        self._settings = app.settings
        self._init = False

    @property
    def _engine(self):
        return self.app['db']

    def __getitem__(self, item):
        return self._settings.__getitem__(item)

    def __getattr__(self, item):
        return self._settings.__getitem__(item)

    def __setattr__(self, key, value):
        if self._init:
            raise RuntimeError(
                'Attribute assignment after initialization is not allowed.')

    def closed(self) -> bool:
        return self._init

    async def init(self):
        await self._load_settings()
        self._init = True

    async def _load_settings(self):
        app_id = self.app['id']
        self.logger.debug('Fetching %s/%s settings from db.', self.landscape, app_id)
        sql = self.table.select().with_only_columns([
            self.table.c.settings
        ])
        sql = sql.where(
            sa.and_(
                self.table.c.landscape == self.landscape,
                self.table.c.app_id == app_id
            )
        )
        settings = await self._engine.fetchrow(sql)
        if not settings:
            self.logger.debug(
                'Personal app settings not found. Fetching default %s settings from db.',
                self.landscape, app_id)
            sql = sql.where(
                sa.and_(
                    self.table.c.landscape == self.landscape,
                    self.table.c.app_id == None
                )
            )
            settings = await self._engine.fetchrow(sql)
            if not settings:
                self.logger.debug('No settings found in db.')
                return

        recursive_update(self._settings, settings['settings'])

    async def close(self):
        self._init = False
