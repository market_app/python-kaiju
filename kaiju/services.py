import abc
import inspect
from functools import partial
from collections import OrderedDict
from types import SimpleNamespace
from typing import *

from .abc import Loggable, Contextable, Serializable, AbstractClassManager, AbstractClassManagerMeta

__all__ = [
    'ServiceMeta', 'Service',
    'ServiceContextManager', 'ServiceSettings', 'ContextableService',
    'ServiceOfServices', 'AbstractClassManager'
]


class ServiceMeta(abc.ABCMeta):

    def __init__(cls, *args, **kws):
        super().__init__(*args, **kws)
        if abc.ABC not in cls.__bases__:
            cls._set_service_name(cls)
            cls._format_service_error_codes(cls)

    @staticmethod
    def _set_service_name(cls):
        if cls.service_name is None:
            cls.service_name = cls.__name__

    @staticmethod
    def _format_service_error_codes(cls):
        codes = {}
        for code in dir(cls.ErrorCodes):
            if not code.startswith('_'):
                value = getattr(cls.ErrorCodes, code)
                codes[code] = cls._create_error_code(value)
        cls.__ErrorCodes = SimpleNamespace(**codes)


class Service(Loggable, abc.ABC, metaclass=ServiceMeta):
    """A simple web application service."""

    service_name = None   #: you may define a custom service name here
    _ErrorCodes = None

    class ErrorCodes:
        """Define your custom error codes here if you dare."""

    def __init__(self, app=None, logger=None):
        """
        :param app: aiohttp web application
        :param logger: a logger instance (None for default)
        """

        if app is not None:
            if logger is None:
                logger = app.logger

        self.app = app
        Loggable.__init__(self, logger=logger)

    @classmethod
    def service_defaults(cls) -> dict:
        """You may specify a custom service defaults
         if no explicit service configuration params were provided"""

        return {}

    @property
    def errors(self) -> SimpleNamespace:
        """Use this for custom service error codes."""

        return self._ErrorCodes

    @classmethod
    def _create_error_code(cls, key: str):
        return f'{cls.service_name}.{key}'

    def discover_service(self, key=None, value=None, required=True):
        """Tries to discover a service and set it in the current class. The service
        will be set as a "key" attribute.

        Examples:

            self.discover_service(SomeServiceCls, 'service_name')
                - try to find "service_name" service and match against given class
            self.discover_service(SomeServiceCls)
                - try to find SomeServiceCls service by its `service_class` name
            self.discover_service('SomeServiceCls')
                - same but will look for SomeServiceCls first in the service classes manager
            self.discover_service(None, 'service_name')
                - try to find a service with service_name no matter which class it is

        :param key: service attribute key or a service class
        :param value: str for service name or service instance for direct service
            assignment or service class to discover it by `value.service_name`
            None for discovering it by `self.<key>.service_name
        :param required: raise error if it can't find the service
            otherwise None will be returned
        :returns: a service instance or None
        """

        if inspect.isclass(key) and issubclass(key, Service):
            cls = key
        elif isinstance(key, str):
            cls = self.app.services.get_class(key)
        elif key is None:
            cls = None
        else:
            raise ValueError(
                'Service key for discovery must be str,'
                ' or Service class or None, but got "%s".' % key)

        if value is None:
            if cls is None:
                service = None
            else:
                service = self.app.services.get(cls.service_name)
        elif isinstance(value, str):
            service = self.app.services.get(value)
        elif isinstance(value, Service):
            service = value
        else:
            raise ValueError(
                'Service value for discovery must be str,'
                ' or Service instance or None, but got "%s".' % value)

        if service is None:
            if required:
                raise KeyError(
                    'Service "%s" of class "%s" doesn\'t exist.'
                    ' Available classes: %s.'
                    ' Available services: %s.'
                    % (
                        value, key,
                        list(self.app.services._classes.keys()),
                        list(self.app.services._services.keys()))
                )
            else:
                self.logger.warning(
                    'Service dependence "%s" of class "%s" doesn\'t exist.',
                    value, key)
        else:
            if cls and not isinstance(service, cls):
                raise TypeError(
                    'Service "%s" was discovered but it\'s not a subclass of "%s".'
                    ' Either set "key=None" to avoid this check or set "value=None"'
                    ' to get a default service of provided class.' % (service, cls))

        return service


class ServiceOfServices(Service, abc.ABC):

    @abc.abstractmethod
    def register_service(self, service: Service):
        """Here you can create whatever code you want to register another
        service in it. The context manager will automatically try to perform
        the task if it will receive an appropriate instruction for it."""


class ContextableService(Service, Contextable, abc.ABC):
    """A service which is also a contextable."""


class ServiceContextManagerMeta(ServiceMeta, AbstractClassManagerMeta):

    def __init__(cls, *args, **kws):
        ServiceMeta.__init__(cls, *args, **kws)
        AbstractClassManagerMeta.__init__(cls, *args, **kws)


class ServiceContextManager(Service, AbstractClassManager, Serializable, metaclass=ServiceContextManagerMeta):
    """App cleanup ctx initialization for contextable services from a config list."""

    _class = Service
    service_name = 'services'

    DEFAULT_ENABLE_POLICY = True
    DEFAULT_REQUIRE_POLICY = True
    DEFAULT_REGISTER_POLICY = True

    _classes = {}

    def __init__(self, app, settings: Union[Iterable, OrderedDict], logger=None):
        """
        :param app: aiohttp web application
        :param settings: service settings list in order in which they will be
            initialized on app start, settings must be compatible with
            the `ServiceSettings` interface
            You can pass an ordered dictionary if you want, the keys will
            be ignored.
        :param logger: a logger instance (None for default)
        """

        super().__init__(app=app, logger=logger)
        self._set_app_attrs(app, self.service_name, self)

        self._run_configurations = []
        self._services = {}

        self._init_services(settings)

    def add(self, **params) -> Optional[Service]:
        """Add a new service to the initialization context.

        :params: same as for the `ServiceSettings` object initialization
        :returns: an initialized service or None if it is not enabled
        """

        return self._init_service(params)

    def add_from_module(self, module, **params) -> Generator[Service, None, None]:
        """Initializes multiple services from a module."""

        for obj in module.__dict__.values():
            if inspect.isclass(obj) and not inspect.isabstract(obj):
                if issubclass(obj, Service):
                    yield self.add(cls=obj, **params)

    @property
    def services(self):
        return iter(self._services.values())

    def __getitem__(self, item) -> Service:
        """Returns a service instance by its name.

        .. attention::

            A returned service doesn't always mean that it is ready to use,
            because its initialization may occur after the app was started
            and not when a service context manager was created.

        """

        try:
            return self._services[item]
        except KeyError:
            raise KeyError(
                'Service "%s" is not currently available.'
                'Available services: %s.'
                % (item, list(self._services.keys())))

    def get(self, key: str, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def __contains__(self, item):
        """Checks if service with such name exists."""

        if item in self._services:
            return bool(self._services[item])

        return False

    def __getattr__(self, item):
        """Returns a service instance by its name.

        .. attention::

            A returned service doesn't always mean that it is ready to use,
            because its initialization may occur after the app was started
            and not when a service context manager was created.

        """

        item = self.__getitem__(item)
        return item

    def __iter__(self):
        """Yield service contexts."""

        async def _terminate(app, service, config):
            self.logger.debug(
                'Closing "%s" service context.',
                config.cls.__name__)
            try:
                await service.close()
            except Exception as e:
                self.logger.error(
                    'Error closing "%s" service context. [%s]: %s',
                    config.cls.__name__, e.__class__.__name__, e)

        async def _self_ctx(app):
            self._set_app_attrs(app, self.service_name, self)
            self.logger.info(
                'Mapped a new service context manager "%s" -> "app.%s".',
                self.__class__.__name__, self.service_name)
            yield
            self._set_app_attrs(app, self.service_name, None)

        async def _service_ctx(app, config: ServiceSettings):

            self.logger.debug(
                'Initializing a new instance of "%s" service.',
                config.cls.__name__)

            service = self._services[config.name]

            if isinstance(service, Contextable):
                try:
                    self.logger.debug(
                        'Initializing "%s" service context.',
                        config.cls.__name__)
                    await service.init()
                except Exception as e:
                    self.logger.error(
                        'Error initializing "%s" service context. [%s]: %s',
                        config.cls.__name__, e.__class__.__name__, e)
                    await _terminate(app, service, config)
                    if config.required:
                        raise e

            yield

            if service:
                if isinstance(service, Contextable):
                    await _terminate(app, service, config)

        yield _self_ctx

        for config in self._run_configurations:
            config = partial(_service_ctx, config=config)
            yield config

    def repr(self):
        return {
            'settings': [
                config.repr()
                for config in self._run_configurations
            ]
        }

    @staticmethod
    def _set_app_attrs(app, service_name, service):
        app[service_name] = service
        setattr(app, service_name, service)

    @classmethod
    def get_class(cls, service_cls: str):
        return cls._classes.get(service_cls)

    @classmethod
    def _get_service_class(cls, service_cls):
        if isinstance(service_cls, str):
            service_cls = cls._classes[service_cls]
        else:
            if not issubclass(service_cls, Service):
                raise ValueError(
                    'A service context manager cannot initialize "%s" class '
                    'because it does not inherits from base "Service" class.'
                    % service_cls.__name__)
            if service_cls.__name__ not in cls._classes:
                cls.register_class(service_cls)
        return service_cls

    def _init_services(self, settings):
        if isinstance(settings, dict):
            _settings = []
            for key, value in settings.items():
                value['name'] = key
                _settings.append(value)
            settings = _settings
        for params in settings:
            self._init_service(params)

    def _init_service(self, params):
        cls = params['cls'] = self._get_service_class(params['cls'])
        self.logger.debug('Trying to create service of type "%s".', cls)
        run_configuration = ServiceSettings(**params)
        if run_configuration.name in self._services:
            self.logger.warn(
                'A service with name "%s" is already registered.',
                run_configuration.name)
            return

        if run_configuration.enabled:
            service_settings = params.get('settings', {})
            service = cls(app=self.app, logger=self.logger, **service_settings)
            service.service_name = run_configuration.name
            self._services[run_configuration.name] = service
            self._run_configurations.append(run_configuration)

            if run_configuration.register_in:
                for super_service in run_configuration.register_in:
                    if super_service in self._services:
                        super_service = self._services[super_service]
                        if isinstance(super_service, ServiceOfServices):
                            super_service.register_service(service)
        else:
            service = None

        if run_configuration.registered:
            self._set_app_attrs(self.app, run_configuration.name, service)
            if service:
                self.logger.info(
                    'Mapped a new service "%s" -> "app.%s.%s".',
                    run_configuration.cls.__name__, self.service_name,
                    run_configuration.name)

        self._services[run_configuration.name] = service

        return service


class ServiceSettings(Serializable):
    """Service settings specification."""

    __slots__ = (
        'cls', 'name', 'info', 'enabled', 'registered', 'required', 'settings',
        'register_in'
    )

    def __init__(
            self, cls: Type[Service], name: str = None, info: str = None,
            enabled=None, required=None, registered=None, settings: dict = None,
            register_in: Union[str, List[str]] = None):
        """
        :param cls: service class name or class itself
        :param name: service custom name (if None, then defaults will be used)
        :param info: human readable information about the service
        :param enabled: server won't be initialized if this was set to False
        :param required: a required service means that the app won't start if it fails
        :param registered: a registered service means that it will be added to
            the application dictionary and attributes
        :param settings: service custom settings
        :param register_in: perform a registration of a service in another service
        """

        _defaults = cls.service_defaults()

        if name:
            self.name = name
        elif 'name' in _defaults:
            self.name = _defaults['name']
        elif cls.service_name:
            self.name = cls.service_name
        else:
            self.name = cls.__name__

        self.cls = cls

        if info is not None:
            self.info = info
        elif 'info' in _defaults:
            self.info = _defaults['info']
        else:
            self.info = ''

        if enabled is not None:
            self.enabled = enabled
        elif 'enabled' in _defaults:
            self.enabled = _defaults['enabled']
        else:
            self.enabled = ServiceContextManager.DEFAULT_ENABLE_POLICY

        if registered is not None:
            self.registered = registered
        elif 'registered' in _defaults:
            self.registered = _defaults['registered']
        else:
            self.registered = ServiceContextManager.DEFAULT_REGISTER_POLICY

        if required is not None:
            self.required = required
        elif 'required' in _defaults:
            self.required = _defaults['required']
        else:
            self.required = ServiceContextManager.DEFAULT_REQUIRE_POLICY

        if settings is not None:
            self.settings = settings
        elif 'settings' in _defaults:
            self.registered = _defaults['settings']
        else:
            self.settings = {}

        if register_in is not None:
            if isinstance(register_in, str):
                self.register_in = [register_in]
            else:
                self.register_in = list(register_in)
        elif 'register_in' in _defaults:
            self.register_in = _defaults['register_in']
        else:
            self.register_in = []

    def repr(self):
        return {
            'cls': self.cls,
            'name': self.name,
            'info': self.info,
            'enabled': self.enabled,
            'registered': self.registered,
            'required': self.required,
            'register_in': self.register_in,
            'settings': self.settings
        }
