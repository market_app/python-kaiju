from kaiju.services import AbstractClassManager
from .abc import AbstractFileLoader

__all__ = ['Loaders']


class Loaders(AbstractClassManager):
    _class = AbstractFileLoader
