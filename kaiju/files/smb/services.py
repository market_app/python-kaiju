import asyncio
import io
import os
import tempfile
from weakref import proxy
from datetime import datetime
from pathlib import Path
from typing import *

from smb.SMBConnection import SMBConnection
from smb.base import OperationFailure

from kaiju.services import ContextableService
from kaiju.helpers import async_run_in_thread, repeat, random_string
from ..abc import AbstractFileTransportInterface
from ..errors import ErrorCode
from ..services import FileService

__all__ = ['SMBConnectionPool', 'SMBTransport']


class SMBConnectionPool(ContextableService):

    service_name = 'SMBConnectionPool'

    class _ConnectionContext:

        __slots__ = ('_connection', '_pool')

        def __init__(self, pool):
            self._pool = proxy(pool)
            self._connection = None

        async def __aenter__(self):
            self._connection = connection = await self._pool.get()
            return connection

        async def __aexit__(self, exc_type, exc_val, exc_tb):
            await self._pool.release(self._connection)
            self._connection = None

    conn_exception_classes = (
        ConnectionError, TimeoutError, ConnectionRefusedError,
        EOFError
    )

    MIN_SIZE = 2
    MAX_SIZE = 8
    CONN_RETRIES = 3

    def __init__(
            self, smb_settings: dict, connection_settings: dict,
            conn_retries=CONN_RETRIES, min_size=MIN_SIZE, max_size=MAX_SIZE,
            app=None, logger=None):
        """
        :param app:
        :param smb_settings: simba `SMBConnection` class settings
        :param connection_settings: `SMBConnection.connect` settings
        :param conn_retries: max number of retries when connecting
        :param min_size:
        :param max_size:
        :param logger:
        """

        super().__init__(app=app, logger=logger)

        self._smb_settings = smb_settings
        self._connection_settings = connection_settings
        self.min_size = max(1, int(min_size))
        self.max_size = max(self.min_size, int(max_size))
        self.conn_retries = int(conn_retries)

        self._closing = True
        self._connections = []
        self._available = []
        self._task = False

    def __len__(self):
        return len(self._connections)

    async def init(self):
        if self.closed:
            self.logger.debug('Initialization.')
            self._closing = False
            await self._init_connections()
            self.logger.debug('Initialization complete.')

    async def close(self):
        if not self.closed:
            self.logger.debug('Closing the pool.')
            self._closing = True
            await asyncio.gather(*(
                async_run_in_thread(connection.close)
                for connection in self._connections
            ))
            self._connections, self._available, self._task = [], [], False
            self.logger.debug('Pool closed.')

    async def closed(self) -> bool:
        return self._closing is False and self._connections

    def acquire(self):
        return self._ConnectionContext(self)

    @property
    def opened_connections(self) -> List[SMBConnection]:
        return [conn for conn in self._connections if not self._connection_closed(conn)]

    async def get(self) -> SMBConnection:
        """Get an unused channel or create one."""

        self._remove_closed_connections()

        while self._available:
            conn = self._available.pop()
            if conn.echo(b's') == b's':
                return conn
            else:
                conn.close()

        conn = await self._create_connection()
        self.logger.debug('Acquired a connection.')
        return conn

    async def release(self, connection):
        if not self._connection_closed(connection):
            self._available.append(connection)
        self.logger.debug('Released a connection.')
        await self._purge_unused_connections()

    async def _purge_unused_connections(self):
        if not self._task:
            self._task = True
            self._remove_closed_connections()
            if len(self._connections) > self.min_size and self._available:
                self.logger.debug('Closing unused connections.')
                dn = len(self._connections) - self.min_size
                _to_remove, self._available = self._available[:dn], self._available[dn:]
                _to_remove = await asyncio.gather(*(
                    async_run_in_thread(connection.close)
                    for connection in _to_remove
                ))
                self.logger.debug('Closed %d unused connections.', dn)
                self._remove_closed_connections()
            self._task = False

    def _remove_closed_connections(self):
        n = len(self)
        self._connections = self.opened_connections
        self._available = [ch for ch in self._available if ch in self._connections]
        n = n - len(self)
        if n:
            self.logger.debug('Removed %d closed connections.', n)

    async def _init_connections(self):
        if len(self._connections) < self.min_size:
            n = self.min_size - len(self._connections)
            self.logger.debug('Initializing %d new connections.', n)
            new = await asyncio.gather(*(
                self._create_connection()
                for _ in range(n)
            ))
            self._available.extend(new)

    async def _create_connection(self):

        def _connect(conn):
            conn.connect(**self._connection_settings)
            if conn.echo(b's') != b's':
                raise ConnectionError

        while len(self._connections) >= self.max_size:
            await asyncio.sleep(0.1)
        settings = {**self._smb_settings}
        settings['my_name'] = f'python_{random_string(8)}'
        connection = SMBConnection(**settings)
        await repeat(
            async_run_in_thread, self.conn_retries, _connect,
            logger=self.logger, max_exec_time=60,
            exception_classes=self.conn_exception_classes,
            conn=connection)
        self._connections.append(connection)
        return connection

    @staticmethod
    def _connection_closed(connection) -> bool:
        return not connection.sock or connection.sock._closed


class SMBTransport(ContextableService, AbstractFileTransportInterface):

    service_name = 'SMBTransport'
    DEFAULT_ERROR_SUBDIR = 'errors'
    RETRIES = 3

    file_service = FileService

    conn_exception_classes = (
        ConnectionError, TimeoutError, ConnectionRefusedError,
        EOFError, OperationFailure
    )

    def __init__(
            self, app, pool: Union[str, SMBConnectionPool],
            share: str, shared_dirs: Union[str, List[str]],
            failed_dir: str = None, dir: str = '.', retires: int = RETRIES,
            file_service=None, logger=None):

        super().__init__(app=app, logger=logger)
        if isinstance(pool, str):
            pool = self.app.services[pool]
        self._pool = pool
        self._share = share
        if isinstance(shared_dirs, str):
            shared_dirs = [shared_dirs]
        self._shared_dirs = shared_dirs
        self._failed_dir = failed_dir
        self._dir = str(dir)
        self.retries = max(0, int(retires))

        self.file_service = self.discover_service(self.file_service, file_service)

    async def init(self):
        self.logger.info('Checking that shared dirs exist.')
        dirs = []
        for share in self._shared_dirs:
            share = Path(share.strip('\\/'))
            if await self.exists(share):
                dirs.append(share)
                self.logger.debug('Shared dir "%s" successfully discovered.', share)
            else:
                self.logger.debug('Shared dir "%s" doesn\'t exist.', share)
        self._shared_dirs = dirs

        if not dirs:
            raise RuntimeError('No shared directories found.')

        self.logger.info('Ensuring that errors folder exists.')
        if self._failed_dir is None:
            self._failed_dir = self._shared_dirs[0] / self.DEFAULT_ERROR_SUBDIR
        self._failed_dir = Path(self._failed_dir)
        await self.create_dir(self._failed_dir)

    async def create_dir(self, uri: Path, shared_dir=None, exists_ok=True):

        def _create_dir(conn, uri: Path):
            conn.createDirectory(self._share, str(uri))

        if shared_dir:
            uri = shared_dir / uri

        if await self.exists(uri):
            if not exists_ok:
                raise ValueError('Directory "%s" already exists.', uri)
        else:
            async with self._pool.acquire() as conn:
                return await repeat(
                    async_run_in_thread, self.retries,
                    _create_dir, conn=conn, uri=uri,
                    exception_classes=self.conn_exception_classes,
                    logger=self.logger
                )

    async def clean_dir(self, uri):

        self.logger.debug('Cleaning share folder "%s".', uri)

        async for path in self.list(uri):
            await self.delete(path)

    async def exists(self, uri) -> bool:
        """Check if file or folder exists."""

        def _exists(conn, uri):
            try:
                conn.getAttributes(self._share, str(uri))
            except OperationFailure:
                return False
            else:
                return True

        async with self._pool.acquire() as conn:
            return await repeat(
                async_run_in_thread, self.retries,
                _exists, conn=conn, uri=uri,
                exception_classes=self.conn_exception_classes,
                logger=self.logger
            )

    async def has_new_files(self) -> bool:
        """Should return True if new downloadable files found in shared folders."""

        try:
            await self.list().__anext__()
        except StopAsyncIteration:
            return False
        else:
            return True

    async def list(self, path: Path = None, pattern='*') -> AsyncGenerator[Path, None]:
        """Should list all files in shared folders."""

        ignored_dirs = {'.', '..', self._failed_dir.name}

        def _list(path, conn):
            data = conn.listPath(self._share, str(path), pattern=pattern)
            for obj in data:
                if obj.filename not in ignored_dirs:
                    name = Path(path) / obj.filename
                    if obj.isDirectory:
                        for obj in _list(path=str(name), conn=conn):
                            yield obj
                    else:
                        yield name

        async with self._pool.acquire() as conn:
            if path is None:
                for shared_dir in self._shared_dirs:
                    for obj in _list(shared_dir, conn):
                        yield obj
            else:
                for obj in _list(path, conn):
                    yield obj

    async def download(self, uri: Path) -> tempfile.NamedTemporaryFile:
        """Should download a file to a local temp dir and return the location."""

        def _download(conn, uri):
            f = self.file_service.get_temp_file_sync(mode='wb', delete=False)
            try:
                conn.retrieveFile(self._share, str(uri), f, timeout=60)
                size = f.tell()
            except Exception as err:
                return err
            finally:
                if not f.closed:
                    f.close()
            return f, size

        async with self._pool.acquire() as conn:
            f = await repeat(
                async_run_in_thread, self.retries,
                _download, conn=conn, uri=uri,
                exception_classes=self.conn_exception_classes,
                logger=self.logger
            )
            if isinstance(f, Exception):
                raise f

            f, size = f

        self.logger.debug('Saved downloaded file "%s" -> "%s" (%d bytes).', uri, f.name, size)

        return f

    async def delete(self, uri: Path):
        """Should remove a downloaded file from a shared directory."""

        def _delete(conn, uri):
            try:
                conn.deleteFiles(self._share, str(uri))
            except OperationFailure:
                pass

        async with self._pool.acquire() as conn:
            await repeat(
                async_run_in_thread, self.retries,
                _delete, conn=conn, uri=uri,
                exception_classes=self.conn_exception_classes,
                logger=self.logger
            )

        self.logger.debug('Removed file "%s" from the share.', uri)

    async def mark_failed(self, uri: Path, reason: ErrorCode, message: str = '') -> (str, str):
        """Should mark a shared file as failed and (optionally) move it to
        another shared location.

        :returns: a new file location and a new meta file location.
        """

        def _mark_failed(conn, uri, reason, message):
            new_uri = self._failed_dir / uri.name

            try:
                conn.rename(self._share, str(uri), str(new_uri))
            except OperationFailure:
                conn.deleteFiles(self._share, str(new_uri))
                conn.rename(self._share, str(uri), str(new_uri))

            t = datetime.now().timestamp()
            s = f"""file: {uri.name}\r\npath: {uri}\r\nreason: {reason.value}\r\nmessage: {message}\r\ntime: {t}\r\n"""
            f = io.BytesIO(s.encode('utf-8'))
            meta_uri = new_uri.parent / f'{uri.stem}.txt'
            conn.storeFile(self._share, str(meta_uri), f)
            f.close()

            return Path(new_uri), Path(meta_uri)

        async with self._pool.acquire() as conn:
            await repeat(
                async_run_in_thread, self.retries,
                _mark_failed, conn=conn, uri=uri, reason=reason, message=message,
                exception_classes=self.conn_exception_classes,
                logger=self.logger
            )

        self.logger.debug('Marked file "%s" as failed due to "%s".', uri, reason.value)
