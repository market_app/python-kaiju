import asyncio
import pytest

from smb.SMBConnection import SMBConnection

from .fixtures import *


@pytest.mark.integration
async def test_smb_pool_connection_handling(smb_pool, logger):
    """Testing that pool accurately handles its connections."""

    async with smb_pool as pool:

        async with pool.acquire() as conn:
            async with pool.acquire() as conn2:
                assert type(conn) == type(conn2) == SMBConnection
                logger.info('Pool should return active connections without creating new ones if they are present.')
                assert len(pool) == pool.min_size

            async with pool.acquire() as conn2:
                logger.info('Pool should use the same existing connection.')
                assert len(pool) == pool.min_size

                async with pool.acquire() as conn3:
                    logger.info('Pool should create a new connection.')
                    assert len(pool) == pool.min_size + 1

        logger.info('Pool should return to its normal state.')
        assert len(pool) == pool.min_size

        async with pool.acquire() as conn:
            async with pool.acquire() as conn2:
                async with pool.acquire() as conn3:
                    logger.info(
                        'Acquire exceeding the max pool size limit should'
                        ' wait until other connection is released.')
                    ctx = pool.acquire()
                    task = asyncio.ensure_future(ctx.__aenter__())
                    await asyncio.sleep(0.1)
                    assert not task.done()

                connection = await task
                assert len(pool) == pool.min_size + 1
                del connection
                await ctx.__aexit__(None, None, None)
