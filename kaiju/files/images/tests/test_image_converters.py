import os

import pytest

from .fixtures import *
from ..file_converter import *


@pytest.mark.unit
def test_image_converter(test_image_file, converter_settings, logger):
    converter = ImageConverter(dir='./temp', settings=converter_settings)
    test_meta = {'test': True}
    for output, meta in converter.convert(test_image_file, **test_meta):
        logger.info(output.name)
        logger.info(meta)
        os.unlink(output.name)
