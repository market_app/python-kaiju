"""My very simple implementation of a TTL dict."""

from collections.abc import MutableMapping
from time import time

__all__ = ['TTLDict']


class TTLDict(MutableMapping):
    """A simple TTL dict mostly compatible with a normal one.

    Usage:
    ------

    >>> d = TTLDict({'sht': 666})
    >>> d.set_ttl(1)  # by default eq to TTLDict.TTL
    >>> assert d['sht'] == 666
    >>> del d['sht']
    >>> d.refresh()   # may be used to conserve some memory but usually it's not needed

    """

    TTL = 300  #: default TTL in seconds

    __slots__ = ('_ttl', 'dict', '_ttls')

    def __init__(self, *args, **kws):
        self._ttl = self.__class__.TTL
        self._ttls = []  # sorted list of item TTLs
        self.dict = dict()
        self.update(dict(*args, **kws))

    def __getitem__(self, item):
        value, t = self.dict[item]
        if time() - t < self._ttl:
            return value
        else:
            del self[item]
            raise KeyError(item)

    def __setitem__(self, key, value):
        t = time()
        self._ttls.append((key, t))
        self.dict[key] = (value, t)

    def __delitem__(self, key):
        del self.dict[key]
        _ttls = self._ttls
        for i, (k, t) in enumerate(_ttls):
            if key == k:
                del _ttls[i]

    def __len__(self):
        self.refresh()
        return len(self.dict)

    def __bool__(self):
        return bool(len(self))

    def __contains__(self, item):
        return self.get(item) is not None

    def __eq__(self, other):
        self.refresh()
        other.refresh()
        assert other.dict == self.dict

    def __iter__(self):
        return iter(self.keys())

    def values(self):
        self.refresh()
        return (v for v, t in self.dict.values())

    def keys(self):
        self.refresh()
        return self.dict.keys()

    def items(self):
        return zip(self.dict.keys(), self.values())

    def set_ttl(self, ttl):
        """Set your custom TTL here (in sec). By default the class uses
        `TTLDict.TTL` value."""

        ttl = float(ttl)
        if ttl <= 0:
            raise ValueError('TTL value must be greater than zero.')
        self._ttl = ttl

    def refresh(self):
        """Removes old records.

        .. note::

            This method is called each time one calls a `TTLDict.__len__`
            or any other method that must provide an actual dictionary state.

        """

        t1 = time()
        _ttls, _dict, _ttl = self._ttls, self.dict, self._ttl

        while _ttls:
            k, t = _ttls[0]
            if t + _ttl < t1:
                del _dict[k]
                del _ttls[0]
            else:
                break
