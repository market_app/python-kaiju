import csv
import pathlib

__all__ = ['weak_passwords']

weak_passwords = pathlib.Path(__file__).resolve().parent / 'passwords.csv'  #: default collection of weak passwords

with open(weak_passwords, 'r') as f:
    reader = csv.reader(f)
    weak_passwords = []
    for row in reader:
        password = row[0].strip()
        if password:
            weak_passwords.extend([
                password,
                password.capitalize(),
                password.upper(),
                password.lower()
            ])
    weak_passwords = frozenset(weak_passwords)
    del reader

del f
