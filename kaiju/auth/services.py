"""
Модуль предназначен для авторизации пользователя в системе
"""

import asyncio
import base64
import random
import re
import time
import os
import uuid
from datetime import datetime, timedelta
from enum import Enum
from typing import *

import bcrypt
import sqlalchemy as sa
from aiohttp_session import AbstractStorage, Session, SESSION_KEY, STORAGE_KEY, get_session
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

from kaiju.rest.exceptions import ValidationError, UnauthorizedError, Conflict, NotFound, NotAllowed
from kaiju.rpc import AbstractRPCCompatibleService, JSONRPCInterface
from kaiju.services import ContextableService
from kaiju.json import encoder, decoder
from .etc import weak_passwords
from .models import users, groups, sessions, permissions

__all__ = [
    'SessionService', 'AuthorizationService', 'PermissionTemplates',
    'UserService'
]


class PermissionTemplates(Enum):
    """Used for default group permissions."""

    ALL = 'ALLOW_ALL'
    NONE = 'ALLOW_NONE'


class AuthorizationService(AbstractRPCCompatibleService, ContextableService):
    """User authorization and management. User groups and permissions management."""

    class ErrorCodes(AbstractRPCCompatibleService.ErrorCodes):

        RPC_PERMISSION_DENIED = 'auth.rpc.permission_denied'

        USER_NOT_FOUND = 'auth.user.not_found'
        USER_EXISTS = 'auth.user.exists'
        USER_AUTH_FAILED = 'auth.user.authentication_failed'
        USER_IDENTICAL_PASSWORDS_SUPPLIED = 'auth.user.identical_passwords_supplied'
        USER_INVALID_EMAIL = 'auth.user.invalid_email'
        USER_INVALID_USERNAME = 'auth.user.invalid_username'
        USER_WEAK_PASSWORD = 'auth.user.weak_password'
        USER_INVALID_PASSWORD = 'auth.user.invalid_password'

        GROUP_NOT_FOUND = 'auth.group.not_found'
        GROUP_EXISTS = 'auth.group.exists'
        GROUP_CANT_EDIT = 'auth.group.cant_edit'
        GROUP_CANT_DELETE = 'auth.group.cant_delete'
        GROUP_PARENT_NOT_FOUND = 'auth.group.parent_group_not_found'
        GROUP_CANT_INHERIT_ITSELF = 'auth.group.cant_inherit_itself'
        GROUP_CANT_HAVE_PARENT = 'auth.group.cant_have_parent'
        GROUP_PARENT_MUST_BE_SYSTEM = 'auth.group.parent_group_must_be_system_group'

        PERMISSION_NOT_FOUND = 'auth.permission.not_found'
        PERMISSION_EXISTS = 'auth.permission.exists'

    service_name = 'auth'

    salt_rounds = 13                    #: OWASP recommendation
    bad_password_timeout = 0.5          #: timeout in sec if auth failed
    bad_password_timeout_jitter = 0.5   #: timeout jitter in sec if auth failed

    min_password_len = 12               #: OWASP recommendation
    max_password_len = 128              #: OWASP recommendation
    password_regex = re.compile(rf'^(?=.*\d).{{{min_password_len},{max_password_len}}}$')

    min_username_len = 4
    max_username_len = 32
    username_regex = re.compile(rf'^[\w0-9_-]{{{min_username_len},{max_username_len}}}$')

    email_regex = re.compile(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)')

    _username_regex = re.compile(username_regex)
    _password_regex = re.compile(password_regex)
    _email_regex = re.compile(email_regex)

    hidden_fields = {'password', 'salt'}    #: fields will be removed from "list users" or "get users" responses

    # redefine if you need a custom tables, use create_***_table functions
    # to bind them to your metadata
    user_table = users
    group_table = groups
    permissions_table = permissions

    def __init__(
            self, app=None,
            rpc_service_name=JSONRPCInterface.service_name,
            default_parent=None, group_settings=None, default_user_group=None,
            guest_user_group=None,
            salt_rounds=salt_rounds,
            weak_password_list=weak_passwords,
            min_password_len=min_password_len,
            max_password_len=max_password_len,
            min_username_len=min_username_len,
            max_username_len=max_username_len,
            bad_password_timeout=bad_password_timeout,
            bad_password_timeout_jitter=bad_password_timeout_jitter,
            logger=None):
        """
        :param app:
        :param rpc_service_name: rpc service name / key to take methods from
        :param salt_rounds: bcrypt algorithm strength
        :param weak_password_list: custom weak or compromised passwords list
        :param min_password_len:
        :param max_password_len:
        :param min_username_len:
        :param max_username_len:
        :param default_parent: default user group parent if None was specified
            upon group creation
        :param group_settings: system and default group definitions (they will be auto-created)
        :param default_user_group: default user group upon user creation / registration
        :param guest_user_group: not authorized users will use permissions from this group
        :param bad_password_timeout: (sec) default timeout if authentication fails
        :param bad_password_timeout_jitter: (sec) default timeout jitter if auth fails
        :param logger:
        """

        super().__init__(app=app, logger=logger)
        self.weak_password_list = frozenset(weak_password_list)
        self.salt_rounds = salt_rounds
        self.bad_password_timeout = bad_password_timeout
        self.bad_password_timeout_jitter = bad_password_timeout_jitter
        self.rpc_service_name = rpc_service_name
        self.default_user_group = default_user_group
        self.group_settings = group_settings
        self.default_parent = default_parent
        self.guest_user_group = guest_user_group
        self.min_username_len = min_username_len
        self.max_username_len = max_username_len
        self.min_password_len = min_password_len
        self.max_password_len = max_password_len

        self._browse_fields = [
            col for col in self.user_table.columns
            if col.key not in self.hidden_fields
        ]

        self.permission_namespaces = None
        self.permissions = None
        self._all_permissions = None
        self._none_permissions = None
        self.formatted_permissions = None
        self.not_formatted_permissions = None

    async def init(self):
        if self.app:
            rpc = self.app[self.rpc_service_name]
            self.permissions = set(rpc._methods.keys()) ^ set(rpc._permissions)
            self._all_permissions = {
                permission: True
                for permission in self.permissions
            }
            self._none_permissions = {
                permission: False
                for permission in self.permissions
            }
            self._none_permissions[f'{self.service_name}.auth'] = True
            self._none_permissions[f'{self.service_name}.register'] = True
            _g = dict(zip(self.permissions, (None for _ in range(len(self.permissions)))))
            self.formatted_permissions = self._format_permissions(_g)
            self.permission_namespaces = {group['id']: group['permissions'] for group in self.formatted_permissions}
            not_formatted_permissions = list(self.permissions)
            not_formatted_permissions.sort()
            self.not_formatted_permissions = tuple(not_formatted_permissions)

        await self._initialize_groups()

    @property
    def _engine(self):
        return self.app['db']

    @property
    def all_permissions(self):
        return dict(self._all_permissions)

    async def get_all_permissions(self):
        custom_permissions = await self._get_custom_permissions()
        permissions = self.all_permissions
        permissions.update({
            key: True
            for key in custom_permissions
        })
        return permissions

    @property
    def none_permissions(self):
        return dict(self._none_permissions)

    async def get_none_permissions(self):
        custom_permissions = await self._get_custom_permissions()
        permissions = self.none_permissions
        permissions.update({
            key: False
            for key in custom_permissions
        })
        return permissions

    @property
    def routes(self):
        return {

            # user methods

            'auth': self.auth,
            'register': self.register,
            'change_password': self.change_password,
            'guest_permissions': self.get_guest_user_permissions,

            # admin /manager methods

            'get': self.get_user,
            'exists': self.user_exists,
            'create': self.create_user,
            'update': self.update_user,
            'update_profile': self.update_profile,
            'list': self.list_users,

            # admin /manager permissions methods

            'permissions.list': self.list_permissions,
            'permissions.exists': self.permission_exists,
            'permissions.create': self.create_permission,
            'permissions.delete': self.delete_permission,

            # admin / manager group methods

            'groups.exists': self.group_exists,
            'groups.get': self.get_group,
            'groups.create': self.create_group,
            'groups.update': self.update_group,
            'groups.delete': self.delete_group,
            'groups.list': self.list_groups

        }

    async def get_guest_user_permissions(
            self, return_custom_permissions: bool = True,
            format_permissions: bool = False,
            resolve_parent_permissions: bool = True):

        if self.guest_user_group is None:
            return self.none_permissions
        else:
            group = await self.get_group(
                self.guest_user_group,
                return_custom_permissions=return_custom_permissions,
                format_permissions=format_permissions,
                resolve_parent_permissions=resolve_parent_permissions)
        return group['permissions']

    async def user_exists(self, user_id) -> bool:
        sql = self.user_table.select().with_only_columns([
            self.user_table.c.is_active
        ]).where(
            self.user_table.c.id == user_id
        )
        user = await self._engine.fetchrow(sql)
        return user is not None

    async def get_user(
            self, user_id, return_permissions: bool = True,
            return_custom_permissions: bool = True,
            format_permissions: bool = False) -> dict:
        """Returns info about a user."""

        sql = self.user_table.select().with_only_columns(
            self._browse_fields
        ).where(
            self.user_table.c.id == user_id
        )
        user = await self._engine.fetchrow(sql)

        if user is None:
            raise NotFound(
                'User doesn\'t exist.', key='id', value=user_id,
                code=self.ErrorCodes.USER_NOT_FOUND)

        if return_permissions:
            user = dict(user)
            if user['group_name'] is not None:
                group = await self.get_group(
                    user['group_name'], format_permissions=format_permissions,
                    return_custom_permissions=return_custom_permissions)
                user['permissions'] = group['permissions']
            else:
                user['permissions'] = self.none_permissions

        return user

    async def list_users(self, user_id=None, offset=0, limit=10) -> dict:
        """Lists info about users in the system."""
        condition = []
        sql = self.user_table.select().with_only_columns(
            self._browse_fields
        ).offset(offset).limit(limit).order_by(
            self.user_table.c.username)

        if isinstance(user_id, list) and user_id:
            condition.append(self.user_table.c.id.in_(user_id))
        elif isinstance(user_id, str) and user_id:
            condition.append(self.user_table.c.id == user_id)

        if condition:
            sql = sql.where(sa.and_(*condition))

        users = await self._engine.fetch(sql)
        return users

    async def create_user(
            self, username: str, email: str, password: str,
            return_permissions: bool = True,
            return_custom_permissions: bool = True,
            format_permissions: bool = False,
            **settings):
        """Adds a new user. Used by administrators or user managers."""

        self.validate_username(username)
        self.validate_email(email)
        password = self.validate_password(password)

        sql = self.user_table.select().where(
            sa.or_(
                self.user_table.c.username == username,
                self.user_table.c.email == email
            )
        )

        data = await self._engine.fetchrow(sql)

        if data is not None:
            raise Conflict(
                'User or e-mail address is already registered.', key=username,
                code=self.ErrorCodes.USER_EXISTS)

        salt, password = self._hash_password(username, password)

        settings['username'] = username
        settings['email'] = email
        settings['password'] = password
        settings['salt'] = salt
        settings['id'] = uuid.uuid4()

        sql = self.user_table.insert().values(settings)
        await self._engine.execute(sql)

        return await self.get_user(
            settings['id'], return_permissions=return_permissions,
            return_custom_permissions=return_custom_permissions,
            format_permissions=format_permissions)

    async def update_user(
            self, user_id,
            return_permissions: bool = True,
            format_permissions: bool = False,
            return_custom_permissions: bool = True,
            password=None,
            salt=None,
            created=None,
            **settings):
        """Updates user settings. Used by administrators or user managers."""

        if not await self.user_exists(user_id):
            raise NotFound(
                'User doesn\'t exist.', key=user_id,
                code=self.ErrorCodes.USER_NOT_FOUND)

        sql = self.user_table.update().values(settings).where(
            self.user_table.c.id == user_id)
        await self._engine.execute(sql)

        return await self.get_user(
            user_id, return_permissions=return_permissions,
            return_custom_permissions=return_custom_permissions,
            format_permissions=format_permissions)

    async def update_profile(self, user_id: str, settings: dict):
        user = await self.get_user(user_id, return_permissions=False)
        meta = user['meta']

        for key, value in settings.items():
            if value is None:
                if key in meta:
                    del meta[key]
            else:
                meta[key] = value

        sql = self.user_table.update().values(
            meta=meta
        ).where(
            self.user_table.c.id == user_id)

        await self._engine.execute(sql)

        return meta

    async def auth(
            self, username: str, password: str,
            return_permissions: bool = True,
            return_custom_permissions: bool = True,
            format_permissions: bool = False) -> dict:
        """User authorization."""

        sql = self.user_table.select().with_only_columns([
            self.user_table.c.id,
            self.user_table.c.password,
            self.user_table.c.salt
        ]).where(
            sa.and_(
                self.user_table.c.username == username,
                self.user_table.c.is_active == True,
                self.user_table.c.is_blocked == False
            )
        )
        user = await self._engine.fetchrow(sql)

        if user is None:
            raise UnauthorizedError(
                'User authentication failed.',
                code=self.ErrorCodes.USER_AUTH_FAILED)

        if not self._check_password(username, password, user['salt'], user['password']):
            raise UnauthorizedError(
                'User authentication failed.',
                code=self.ErrorCodes.USER_AUTH_FAILED)

        return await self.get_user(
            user['id'], return_permissions=return_permissions,
            return_custom_permissions=return_custom_permissions,
            format_permissions=format_permissions)

    async def register(
            self, username: str, password: str, email: str, full_name: str,
            settings: dict = None, return_permissions: bool = True,
            return_custom_permissions: bool = True,
            format_permissions: bool = False):
        """Register a new user."""

        if settings is None:
            settings = {}

        return await self.create_user(
            username=username,
            password=password,
            email=email,
            group_name=self.default_user_group,
            full_name=full_name,
            is_active=True,
            is_blocked=False,
            meta=settings,
            return_permissions=return_permissions,
            return_custom_permissions=return_custom_permissions,
            format_permissions=format_permissions)

    async def change_password(self, username: str, password: str, new_password: str):

        if password == new_password:
            raise ValidationError(
                'Old password matches the new one.',
                code=self.ErrorCodes.USER_IDENTICAL_PASSWORDS_SUPPLIED)

        new_password = self.validate_password(new_password)
        user = await self.auth(username=username, password=password)
        salt, password = self._hash_password(username, new_password)
        sql = self.user_table.update().where(
            self.user_table.c.username == user['username']
        ).values(
            password=password,
            salt=salt
        )
        await self._engine.execute(sql)
        return True

    async def list_permissions(self, format_permissions=True, return_custom_permissions=False) -> list:
        """Returns all available permissions.

        :param format_permissions: if True then the result will be formatted, if False,
            then it will be a flat list
        :param return_custom_permissions: return permissions from permission table as well
        """

        if return_custom_permissions:

            permissions = {key: None for key in self.not_formatted_permissions}
            custom_permissions = await self._get_custom_permissions()
            for permission in custom_permissions:
                permissions[permission] = None
            if format_permissions:
                return self._format_permissions(permissions)
            else:
                return list(permissions.keys())

        if format_permissions:
            return self.formatted_permissions
        else:
            return self.not_formatted_permissions

    async def get_group(
            self, name: str, format_permissions=False,
            return_custom_permissions=True,
            resolve_parent_permissions=True) -> dict:
        """Returns  a user group by its name.

        :raises KeyError: if the group or group's parent doesn't exist
        """

        sql = self.group_table.select().where(self.group_table.c.name == name)
        group = await self._engine.fetchrow(sql)

        if group is None:
            raise NotFound(
                'Group "%s" doesn\'t exists.' % name,
                key=name, code=self.ErrorCodes.GROUP_NOT_FOUND)

        parent = group['parent']

        if parent:
            sql = self.group_table.select().with_only_columns([
                self.group_table.c.permissions
            ]).where(self.group_table.c.name == parent)
            parent_group = await self._engine.fetchrow(sql)
            if parent_group is None:
                raise NotFound(
                    'Parent group "%s" doesn\'t exist.' % parent,
                    key=parent, code=self.ErrorCodes.GROUP_PARENT_NOT_FOUND)
            base_permissions = parent_group['permissions']
        else:
            base_permissions = PermissionTemplates.NONE.value

        group = dict(group)
        group_permissions = group['permissions']

        if resolve_parent_permissions:

            if return_custom_permissions:
                base_permissions, group_permissions = await asyncio.gather(
                    self._resolve_permissions(base_permissions),
                    self._resolve_permissions(group_permissions)
                )
            else:
                base_permissions = self._resolve_permissions_sync(base_permissions)
                group_permissions = self._resolve_permissions_sync(group_permissions)

            base_permissions.update(group_permissions)
            group_permissions = base_permissions

        elif return_custom_permissions:
            group_permissions = await self._resolve_permissions(group_permissions)
        else:
            group_permissions = self._resolve_permissions_sync(group_permissions)

        if format_permissions:
            group['permissions'] = self._format_permissions(group_permissions)
        else:
            group['permissions'] = group_permissions

        return group

    def _resolve_permissions_sync(self, permissions):
        if permissions == PermissionTemplates.ALL.value:
            return self.all_permissions
        elif permissions == PermissionTemplates.NONE.value:
            return self.none_permissions
        else:
            return permissions

    async def _resolve_permissions(self, permissions):
        if permissions == PermissionTemplates.ALL.value:
            return await self.get_all_permissions()
        elif permissions == PermissionTemplates.NONE.value:
            return await self.get_none_permissions()
        else:
            return permissions

    async def group_exists(self, name: str) -> bool:
        """Checks if a user group exists."""

        sql = self.group_table.select().with_only_columns([
            self.group_table.c.name
        ]).where(
            self.group_table.c.name == name
        )
        group = await self._engine.fetchrow(sql)
        return not (group is None)

    async def create_group(
            self, name: str, permissions: dict,
            parent: str = False, format_permissions=False):
        """Adds a new permission group.

        :param name: user group name
        :param permissions: custom group permissions
        :param parent: base group to take permissions from
            False (by default) means that the default parent will be used
        :param format_permissions: formatted permission output (by groups)
        """

        if parent is False:
            parent = self.default_parent
        group = await self._create_group(name=name, permissions=permissions, parent=parent)
        if format_permissions:
            group['permissions'] = self._format_permissions(group['permissions'])
        return group

    async def _create_group(
            self, name: str, parent: str = None, permissions: dict = None,
            system_group: bool = False, **settings):

        if parent:
            if parent == name:
                raise NotAllowed(
                    'Group can\'t inherit from itself.',
                    key=name, method='create',
                    code=self.ErrorCodes.GROUP_CANT_INHERIT_ITSELF)
            elif system_group:
                raise NotAllowed(
                    'System group "%s" can\'t inherit.' % name,
                    key=name, method='update',
                    code=self.ErrorCodes.GROUP_CANT_HAVE_PARENT)
            sql = self.group_table.select().with_only_columns([
                self.group_table.c.system_group
            ]).where(
                self.group_table.c.name == parent
            )
            exists, parent_group = await asyncio.gather(*(
                self.group_exists(name),
                self._engine.fetchrow(sql)
            ))
            if exists:
                raise Conflict(
                    'Group exists.', key=name,
                    code=self.ErrorCodes.GROUP_EXISTS)
            elif parent_group is None:
                raise NotFound(
                    'Parent group "%s" doesn\'t exist.' % parent,
                    key=parent,
                    code=self.ErrorCodes.GROUP_NOT_FOUND)
            elif not parent_group['system_group']:
                raise NotAllowed(
                    'Inheriting from non-system groups is not allowed.',
                    key=parent, method='create',
                    code=self.ErrorCodes.GROUP_PARENT_MUST_BE_SYSTEM)
        else:
            if await self.group_exists(name):
                raise Conflict(
                    'Group exists.', key=name,
                    code=self.ErrorCodes.GROUP_EXISTS)

        settings['name'] = name
        settings['parent'] = parent
        settings['permissions'] = await self._update_permissions({}, permissions)
        settings['system_group'] = system_group

        sql = self.group_table.insert().values(settings)
        await self._engine.execute(sql)

        return settings

    async def update_group(
            self, name: str, permissions: dict, parent: str = None,
            format_permissions=False, **settings) -> dict:
        """Updates permissions for a group.

        :param name: user group name
        :param permissions: custom permissions
        :param format_permissions: formatted permission output (by groups)
        :param parent: base group to take permissions from (ALLOW_NONE by default)
        """

        group = await self._update_group(
            name, permissions=permissions, parent=parent, _rpc=True, **settings)
        if format_permissions:
            group['permissions'] = self._format_permissions(group['permissions'])
        return group

    async def delete_group(self, name: str) -> bool:
        """Removes a specific group (if it is not a system group)."""

        sql = self.group_table.select().with_only_columns([
            self.group_table.c.parent,
            self.group_table.c.system_group
        ]).where(
            self.group_table.c.name == name
        )
        group = await self._engine.fetchrow(sql)
        if not group:
            raise NotFound(
                'Group "%s" doesn\'t exist.' % name, key=name,
                code=self.ErrorCodes.GROUP_NOT_FOUND)
        elif group['system_group'] is True:
            raise NotAllowed(
                'System group "%s" can\'t be removed.' % name,
                method='delete', key=name,
                code=self.ErrorCodes.GROUP_CANT_DELETE)

        parent = group['parent']

        if parent is None:
            parent = self.default_parent

        sql = self.user_table.update().where(
            self.user_table.c.group_name == name
        ).values(
            group_name=parent
        )
        await self._engine.execute(sql)

        sql = self.group_table.delete().where(self.group_table.c.name == name)
        await self._engine.execute(sql)

        return True

    async def list_groups(
            self, offset=0, limit=10, format_permissions=False,
            return_custom_permissions=False, resolve_parent_permissions=True,
            system_group=None):
        """Lists all available user groups."""

        sql = self.group_table.select()
        if system_group is not None:
            sql = sql.where(
                self.group_table.c.system_group == system_group)

        sql = sql.with_only_columns([
            self.group_table.c.name
        ]).offset(offset).limit(limit).order_by(self.group_table.c.name)
        groups = await self._engine.fetch(sql)
        groups = await asyncio.gather(*(
            self.get_group(
                group['name'], format_permissions=format_permissions,
                return_custom_permissions=return_custom_permissions,
                resolve_parent_permissions=resolve_parent_permissions)
            for group in groups
        ))

        return groups

    async def _update_group(
            self, name: str, permissions: dict, parent: str = None,
            _rpc=False, **settings) -> dict:

        sql = self.group_table.select().where(self.group_table.c.name == name)
        group = await self._engine.fetchrow(sql)

        if group is None:
            raise NotFound(
                'Group "%s" doesn\'t exists.' % name,
                key=name,
                code=self.ErrorCodes.GROUP_NOT_FOUND)

        elif parent == name:
            raise NotAllowed(
                'Group can\'t inherit from itself.',
                key=name, method='create',
                code=self.ErrorCodes.GROUP_CANT_INHERIT_ITSELF)

        elif group['system_group']:
            if _rpc:
                raise NotAllowed(
                    'Can\'t update system group "%s".' % name,
                    key=name, method='update',
                    code=self.ErrorCodes.GROUP_CANT_EDIT)

            elif parent:
                raise NotAllowed(
                    'System group "%s" can\'t have parents.' % name,
                    key=name, method='update',
                    code=self.ErrorCodes.GROUP_CANT_HAVE_PARENT)

        if not parent:
            parent = group['parent']

        group_permissions = group['permissions']

        if parent:
            sql = self.group_table.select().with_only_columns([
                self.group_table.c.system_group
            ]).where(
                self.group_table.c.name == parent
            )
            parent_group = await self._engine.fetchrow(sql)
            if parent_group is None:
                raise NotFound(
                    'Parent group "%s" doesn\'t exist.' % parent,
                    key=parent,
                    code=self.ErrorCodes.GROUP_PARENT_NOT_FOUND)

            elif not parent_group['system_group']:
                raise NotAllowed(
                    'Inheriting from non-system groups is not allowed.',
                    key=parent, method='update',
                    code=self.ErrorCodes.GROUP_PARENT_MUST_BE_SYSTEM)

        else:
            if group is None:
                raise NotFound(
                    'Group "%s" doesn\'t exists.' % name,
                    key=name,
                    code=self.ErrorCodes.GROUP_NOT_FOUND)

        settings['permissions'] = await self._update_permissions(group_permissions, permissions)
        if group['parent'] != parent:
            settings['parent'] = parent

        sql = self.group_table.update().where(
            self.group_table.c.name == name
        ).values(settings)
        await self._engine.execute(sql)

        group = dict(group)
        group.update(settings)
        return group

    async def create_permission(self, namespace: str, name: str, method: str, **params) -> dict:

        _name = f'{namespace}.{name}.{method}'

        if await self.permission_exists(_name):
            raise Conflict(
                'Permission already exists.',
                key=f'{namespace}.{name}.{method}',
                code=self.ErrorCodes.PERMISSION_EXISTS)
        sql = self.permissions_table.insert().values(
            namespace=namespace,
            name=name,
            method=method,
            enabled=True,
            **params
        )
        await self._engine.execute(sql)
        return {
            'key': f'{namespace}.{name}.{method}',
            'namespace': namespace,
            'name': name,
            'method': method,
            **params
        }

    async def delete_permission(self, namespace: str, name: str, method: str):

        if not await self._custom_permission_exists(namespace, name, method):
            raise NotFound(
                'Permission doesn\'t exist or can\'t be removed.',
                key=f'{namespace}.{name}.{method}',
                code=self.ErrorCodes.PERMISSION_NOT_FOUND)
        sql = self.permissions_table.delete().where(
            sa.and_(
                self.permissions_table.c.namespace == namespace,
                self.permissions_table.c.name == name,
                self.permissions_table.c.method == method
            )
        )
        await self._engine.execute(sql)
        return True

    async def permission_exists(self, name) -> bool:
        if name in self.permissions:
            return True
        args = name.split('.')
        if len(args) == 3:
            return await self._custom_permission_exists(*args)
        else:
            return False

    async def _custom_permission_exists(self, namespace: str, name: str, method: str) -> bool:
        sql = self.permissions_table.select().with_only_columns([
            self.permissions_table.c.enabled
        ]).where(
            sa.and_(
                self.permissions_table.c.namespace == namespace,
                self.permissions_table.c.name == name,
                self.permissions_table.c.method == method
            )
        )
        permission = await self._engine.fetchrow(sql)
        return permission is not None

    async def _get_custom_permissions(
            self, namespace: str = None, name: str = None,
            method: str = None) -> List[str]:

        sql = self.permissions_table.select()
        conditions = [self.permissions_table.c.enabled == True]
        if namespace:
            conditions.append(self.permissions_table.c.namespace == namespace)
        if name:
            conditions.append(self.permissions_table.c.name == name)
        if method:
            conditions.append(self.permissions_table.c.method == method)
        sql = sql.where(sa.and_(*conditions))
        permissions = await self._engine.fetch(sql)
        return [
            f'{permission["namespace"]}.{permission["name"]}.{permission["method"]}'
            for permission in permissions
        ]

    async def _update_permissions(self, base: dict, permissions: dict):
        """Updates current user group permission dict."""

        if base is None:
            base = {}
        if permissions is None:
            return base
        elif permissions == PermissionTemplates.ALL.value:
            return permissions
        elif permissions == PermissionTemplates.NONE.value:
            return permissions
        else:

            allowed_permissions = set(
                await self.list_permissions(
                    return_custom_permissions=True,
                    format_permissions=False))

            base = await self._resolve_permissions(base)

            for permission, value in permissions.items():
                if value is None:
                    if permission in base:
                        del base[permission]
                else:
                    if permission not in allowed_permissions:
                        raise ValidationError(
                            'Unknown permission flag "%s".' % permission,
                            key=permission, value=permission,
                            allowed_permissions=allowed_permissions,
                            code=self.ErrorCodes.PERMISSION_NOT_FOUND)
                    base[permission] = bool(value)
            return base

    @staticmethod
    def _format_permissions(permissions):
        formatted = {}
        for permission, value in permissions.items():
            group = permission.split('.')[0]
            permission = {
                'id': permission,
                'value': value
            }
            if group in formatted:
                formatted[group].append(permission)
            else:
                formatted[group] = [permission]

        permissions = []

        for group, methods in formatted.items():
            group = {
                'id': group,
                'permissions': methods
            }
            methods.sort(key=lambda x: ['id'])
            permissions.append(group)

        permissions.sort(key=lambda x: x['id'])
        return permissions

    async def _initialize_groups(self):
        if self.group_settings:
            await asyncio.gather(*(
                self._initialize_group(settings)
                for settings in self.group_settings
            ))

    async def _initialize_group(self, settings):
        try:
            group = await self._update_group(**settings)
        except NotFound:
            group = await self._create_group(**settings)
        return group

    def _check_password(self, username, password, salt, hashed) -> bool:
        result = bcrypt.checkpw(password.encode('utf-8'), hashed)
        return result

    def _hash_password(self, username, password, salt=None) -> (bytes, bytes):
        if salt is None:
            salt = bcrypt.gensalt(self.salt_rounds)
        hashed = bcrypt.hashpw(password.encode('utf-8'), salt)
        if not self._check_password(username, password, salt, hashed):
            raise RuntimeError('Internal error.')
        return salt, hashed

    def _get_timeout(self):
        return random.random() * self.bad_password_timeout_jitter + self.bad_password_timeout

    def validate_email(self, email: str):
        if not self._email_regex.fullmatch(email):
            raise ValidationError(
                f'Enter a valid e-mail address.',
                key='email', value=None,
                code=self.ErrorCodes.USER_INVALID_EMAIL)

    def validate_username(self, username: str):
        if not self._username_regex.fullmatch(username):
            raise ValidationError(
                f'Username must be from {self.min_username_len} up to'
                f' {self.max_username_len} characters,'
                f' only letters, numbers and _ . - are allowed.',
                key='username', value=None,
                min_characters=self.min_username_len,
                max_characters=self.max_username_len,
                code=self.ErrorCodes.USER_INVALID_USERNAME)

    def validate_password(self, password: str):
        password = password.strip(' \n\t\r')
        if password in self.weak_password_list:
            raise ValidationError(
                f'Password is too weak or compromised.',
                key='password', value=None,
                code=self.ErrorCodes.USER_WEAK_PASSWORD)

        elif not self._password_regex.fullmatch(password):
            raise ValidationError(
                f'Password must contain {self.min_password_len} up to'
                f' {self.max_password_len} characters at least one of them'
                f' must be a digit.',
                key='password', value=None,
                min_characters=self.min_password_len,
                max_characters=self.max_password_len,
                code=self.ErrorCodes.USER_INVALID_PASSWORD)

        return password


class SessionService(AbstractStorage, ContextableService):
    """Session management service."""

    service_name = 'sessions'
    encryption_strength = 1000  #: default cycles of session data encryption
    encryption_key_length = 32  #: default length of session data encryption key
    salt_rounds = 13            #: OWASP recommendation
    max_session_age = 60        #: default max session age

    # redefine if you need a custom tables, use create_***_table functions
    # to bind them to your metadata
    session_table = sessions

    def __init__(
            self, app=None, cookie_name=SESSION_KEY,
            domain=None, max_age=max_session_age, path='/',
            secure=None, httponly=True, encryption_strength=encryption_strength,
            encryption_key_length=encryption_key_length, app_key=None, salt=None,
            salt_rounds=salt_rounds,
            logger=None):
        """
        :param app:
        :param cookie_name: see aiohttp_session
        :param domain: see aiohttp_session
        :param max_age: see aiohttp_session
        :param path: see aiohttp_session
        :param secure: see aiohttp_session
        :param httponly: see aiohttp_session
        :param encryption_strength: cycles of session data encryption
        :param encryption_key_length: length of session data encryption key
        :param app_key: app encryption key
        :param salt: app encryption salt
        :param salt_rounds: encryption strength
        :param logger:
        """

        AbstractRPCCompatibleService.__init__(self, app=app, logger=logger)
        AbstractStorage.__init__(
            self, cookie_name=cookie_name, encoder=encoder, decoder=decoder,
            domain=domain, max_age=max_age, path=path, secure=secure,
            httponly=httponly)
        self.encryption_strength = encryption_strength
        self.encryption_key_length = encryption_key_length
        self.salt_rounds = salt_rounds
        if app_key is None:
            app_key = base64.b64encode(os.urandom(32)).decode('utf-8')
        if salt is None:
            salt = bcrypt.gensalt(self.salt_rounds)
        elif type(salt) is str:
            salt = salt.encode()
        self._key = self._gen_encryption_key(app_key, salt)

    @property
    def _engine(self):
        return self.app['db']

    async def init(self):
        # TODO: move this into tasks
        await self.delete_expired_sessions()

    async def create_new_session(
            self, request, session_data: dict, key: str = None,
            salt: bytes = None):
        """Generate a new session on user login.

        :param request: http request object
        :param session_data: custom session storage data
        :param key: not used
        :param salt: not used
        :return: a session object
        """

        _id = self._gen_session_id()

        timestamp = datetime.now()

        session_data = {
            'created': timestamp.timestamp(),
            'session': session_data
        }

        session = Session(
            identity=str(_id), data=session_data,
            new=True, max_age=self.max_age)

        expires = timestamp + timedelta(seconds=int(session.max_age))
        session_data = encoder(session_data).encode()

        if key and salt:
            key = self._gen_encryption_key(key, salt)
        else:
            key = self._key

        session_data = self._encrypt_session_data(key, session_data)

        sql = self.session_table.insert().values(
            id=_id,
            created=timestamp,
            data=session_data,
            max_age=session.max_age,
            expires=expires
        )

        await self._engine.execute(sql)

        return session

    def _create_empty_session(self):
        session_data = {
            'created': int(time.time()),
            'session': {
                'user_id': None
            }
        }
        return Session(
            identity=str(self._gen_session_id()), data=session_data,
            new=True, max_age=self.max_age)

    async def load_session(self, request, key: str = None , salt: bytes = None):
        """Load an existing session from the database.

        :param request: http request object
        :param key: not used
        :param salt: not used
        :return: a session object
        """

        session_id = self.load_cookie(request)

        if not session_id:
            return self._create_empty_session()

        if key and salt:
            key = self._gen_encryption_key(key, salt)
        else:
            key = self._key

        session_id = self._decrypt_session_data(key, session_id.encode())
        session_id = uuid.UUID(bytes=session_id)

        sql = sa.select(["*"]).where(self.session_table.c.id == session_id)
        session = await self._engine.fetchrow(sql)

        if not session:
            return self._create_empty_session()

        session_data = session['data']

        session_data = self._decrypt_session_data(key, session_data)
        session_data = decoder(session_data)

        session = Session(
            identity=str(session_id),
            data=session_data, new=False,
            max_age=session["max_age"])

        return session

    async def save_session(
            self, request, response, session,
            key: str = None, salt: bytes = None):
        """Update session record in the database.

        :param request: http request object
        :param response: http response object
        :param session: a new / updated session object
        :param key: not used
        :param salt: not used
        """

        if key and salt:
            key = self._gen_encryption_key(key, salt)
        else:
            key = self._key

        session_data = {
            "created": session.created,
            "session": session._mapping
        }
        session_data = encoder(session_data).encode()
        session_data = self._encrypt_session_data(key, session_data)
        expires = datetime.fromtimestamp(session.created + int(session.max_age))

        sql = self.session_table.update().where(
            self.session_table.c.id == session.identity
        ).values(
            data=session_data,
            expires=expires
        )

        await self._engine.execute(sql)

        session_id = uuid.UUID(session.identity).bytes
        session_id = self._encrypt_session_data(key, session_id)

        self.save_cookie(
            response,
            session_id.decode('utf-8'),
            max_age=session.max_age
        )

    async def delete_cookie(self, response):
        response.del_cookie(self._cookie_name)

    async def delete_session(self, request):
        """Clear the session and remove it from the db.

        :param request: http request object
        """

        session = await get_session(request)
        session.clear()
        sql = self.session_table.delete().where(
            self.session_table.c.id == session.identity
        )
        await self._engine.execute(sql)

    async def delete_expired_sessions(self):
        """Remove all expired sessions from the database."""

        self.logger.debug('Removing expired sessions.')
        sql = self.session_table.delete().where(
            self.session_table.c.expires <= datetime.now()
        )
        result = await self._engine.execute(sql)
        self.logger.info('Removed expired sessions. %s.', result)

    def _gen_session_id(self):
        return uuid.uuid4()

    def _gen_encryption_key(self, key: str, salt: bytes):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=self.encryption_key_length,
            salt=salt,
            iterations=self.encryption_strength,
            backend=default_backend()
        )
        key = base64.urlsafe_b64encode(kdf.derive(key.encode()))
        return key

    @staticmethod
    def _encrypt_session_data(key, data: bytes):
        f = Fernet(key)
        data = f.encrypt(data)
        return data

    @staticmethod
    def _decrypt_session_data(key, data: bytes):
        f = Fernet(key)
        data = f.decrypt(data)
        return data


class UserService(AbstractRPCCompatibleService):
    """User service what is compatible with `HTTPCompatibleJSONQueueServer`
    rpc service.

    It is used for session/cookie based authentication and profile management.
    It's supposed that normal users access this service to login / logout.
    """

    service_name = 'users'
    auth_service = AuthorizationService.service_name
    session_service = SessionService.service_name

    SESSION_REFRESH_INTERVAL = 300

    _session_refresh_key = 'updated'

    def __init__(
            self, app,
            session_refresh_interval: int = SESSION_REFRESH_INTERVAL,
            logger=None):
        """
        :param app:
        :param session_refresh_interval: user permissions stored in session's
            cache will be refreshed periodically, this parameter determines the
            refresh interval (in seconds), you shouldn't use too short intervals
            because it slows the request performance
        :param logger:
        """

        super().__init__(app=app, logger=logger)
        self._session_refresh_interval = session_refresh_interval

    @property
    def routes(self) -> dict:
        return {
            'register': self.register,
            'login': self.login,
            'logout': self.logout,
            'update_profile': self.update_profile,
            'change_password': self.change_password,
            'get': self.get_user
        }

    async def list(self, user_id=None, offset=0, limit=10):
        users = self.app.services[self.auth_service]
        return await  users.list_users(user_id=user_id, offset=offset, limit=limit)

    async def register(
            self, request, username: str, password: str, email: str,
            full_name: str, settings: dict = None):
        """Register a new user. Login will be performed automatically.

        :param request: passed automatically by an rpc service
        :param username:
        :param password:
        :param email:
        :param full_name:
        :param settings: user profile settings
        :return: a session object
        """

        users = self.app.services[self.auth_service]
        await users.register(username, password, email, full_name, settings=settings)
        return await self.login(request, username=username, password=password)

    async def login(self, request, username: str, password: str) -> dict:
        """Login a user and create a new session.

        :param request: passed automatically by an rpc service
        :param username:
        :param password:
        :return: a session object
        """

        users = self.app.services[self.auth_service]
        user = await users.auth(
            username=username, password=password, return_permissions=True,
            return_custom_permissions=True, format_permissions=False)
        sessions = request.get(STORAGE_KEY)
        data = self._get_session_data(user)
        session = await sessions.create_new_session(request, data)
        session['user_id'] = user['id']
        for key, value in data.items():
            session[key] = value
        request[SESSION_KEY] = session
        return {
            'settings': user['meta'],
            'permissions': user['permissions']
        }

    async def logout(self, request):
        """Logout an authorized user and clear the session.

        :param request: passed automatically by an rpc service
        """

        sessions = request.get(STORAGE_KEY)
        await sessions.delete_session(request)

    async def change_password(self, request, username: str, password: str, new_password: str):
        """Change user password to a new one.

        :param request: passed automatically by an rpc service
        :param username:
        :param password:
        :param new_password:
        :return: a session object
        """

        users = self.app.services[self.auth_service]
        await users.change_password(username, password, new_password)
        await self.logout(request)
        return await self.login(request, username,  new_password)

    async def update_profile(self, request, settings: dict) -> dict:
        """Updates user profile metadata.

        :param request: passed automatically by an rpc service
        :param settings: new settings
        :return: new settings
        """

        session = await self.get_session(request)
        user_id = session['user_id']
        users = self.app.services[self.auth_service]
        settings = await users.update_profile(user_id, settings)
        session['settings'] = settings
        return {
            'settings': settings
        }

    async def get_user(self, request, return_permissions=False):
        """Returns a complete information about the current logged user."""

        auth = self.app.services[self.auth_service]
        session = await get_session(request)

        if 'user_id' in session:  # session has a real user
            if session['user_id']:
                data = await auth.get_user(
                    session['user_id'], return_permissions=return_permissions)
                _data = {
                    'username': data['username'],
                    'full_name': data['full_name'],
                    'email': data['email'],
                    'group_name': data['group_name'],
                    'created': data['created'],
                    'settings': data['meta']
                }
                if return_permissions:
                    _data['permissions'] = data['permissions']
                return data

    async def get_session(self, request):
        """Writes a user session and session permissions. Update permissions
        if required."""

        session = await get_session(request)
        auth = self.app.services[self.auth_service]

        if session['user_id']:  # session has a real user
            last_updated = session.get('updated', 0)
            if last_updated + self._session_refresh_interval < time.time():
                user = await auth.get_user(
                    session['user_id'], return_permissions=True,
                    return_custom_permissions=True, format_permissions=False)
                data = self._get_session_data(user)
                session._mapping.update(data)

        if session.get('permissions') is None:
            session['permissions'] = await auth.get_guest_user_permissions()

        return session

    @staticmethod
    def _get_session_data(user: dict) -> dict:
        return {
            'user_id': user['id'],
            'settings': user['meta'],
            'permissions': user['permissions'],
            'updated': int(time.time())
        }
