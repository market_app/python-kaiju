from datetime import datetime

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = [
    'users', 'groups', 'sessions', 'permissions',
    'create_groups_table', 'create_users_table', 'create_sessions_table',
    'create_permissions_table'
]


def create_sessions_table(table_name: str, metadata: sa.MetaData, *columns: sa.Column):
    """
    :param table_name: custom table name
    :param metadata: custom metadata object
    :param columns: additional columns
    """

    return sa.Table(
        table_name, metadata,
        sa.Column('id', sa_pg.UUID, primary_key=True),
        sa.Column('created', sa.TIMESTAMP, nullable=False),
        sa.Column('data', sa_pg.BYTEA, nullable=False),
        sa.Column('max_age', sa.INTEGER, nullable=False),
        sa.Column('expires', sa.TIMESTAMP, nullable=False),
        *columns
    )


sessions = create_sessions_table('session', sa.MetaData())


def create_permissions_table(table_name: str, metadata: sa.MetaData, *columns: sa.Column):
    """
    :param table_name: custom table name
    :param metadata: custom metadata object
    :param columns: additional columns
    """

    permissions = sa.Table(
        table_name, metadata,
        sa.Column('namespace', sa.String, primary_key=True, nullable=False),
        sa.Column('name', sa.String, primary_key=True, nullable=False),
        sa.Column('method', sa.String, primary_key=True, nullable=False),
        sa.Column('enabled', sa.Boolean, nullable=False),
        *columns
    )
    return permissions


permissions = create_permissions_table('permissions', sa.MetaData())


def create_groups_table(table_name: str, metadata: sa.MetaData, *columns: sa.Column):
    """
    :param table_name: custom table name
    :param metadata: custom metadata object
    :param columns: additional columns
    """

    groups = sa.Table(
        table_name, metadata,
        sa.Column(
            'name', sa.String, unique=True, primary_key=True,
            comment='group name'),
        sa.Column(
            'parent', sa.ForeignKey(f'{table_name}.name'), nullable=True,
            comment='ref to a parent group and its permissions'),
        sa.Column(
            'system_group', sa.Boolean, nullable=False,
            comment='system groups cannot be manually edited or removed'),
        sa.Column(
            'permissions', sa_pg.JSONB,
            comment='user group permissions (available RPC methods)'),
        sa.Column('meta', sa_pg.JSONB, nullable=False,
                  default={}, server_default=sa.text("'{}'::jsonb")),
        *columns
    )
    return groups


groups = create_groups_table('user_groups', sa.MetaData())


def create_users_table(
        table_name: str, groups_table_name: str, metadata: sa.MetaData,
        *columns: sa.Column):
    """
    :param table_name: custom table name
    :param groups_table_name: user groups table name for foreign key references
    :param metadata: custom metadata object
    :param columns: additional columns
    """

    users = sa.Table(
        table_name, metadata,
        sa.Column(
            'id', sa_pg.UUID, primary_key=True,
            server_default=sa.text("uuid_generate_v4()")),
        sa.Column('username', sa.String, unique=True, nullable=False),
        sa.Column('email', sa.String, unique=True, nullable=False),
        sa.Column('password', sa_pg.BYTEA, nullable=False),
        sa.Column('salt', sa_pg.BYTEA, nullable=False),
        sa.Column('is_active', sa.Boolean, nullable=False, default=True),
        sa.Column('is_blocked', sa.Boolean, nullable=False, default=False),
        sa.Column('group_name', sa.ForeignKey(f'{groups_table_name}.name'), nullable=False),
        sa.Column(
            'created', sa.DateTime, nullable=False, default=datetime.utcnow,
            server_default=sa.func.timezone('UTC', sa.func.current_timestamp())),
        sa.Column(
            'meta', sa_pg.JSONB, nullable=False, default={},
            server_default=sa.text("'{}'::jsonb")),
        *columns
    )
    return users


users = create_users_table('users', 'user_groups', sa.MetaData())
