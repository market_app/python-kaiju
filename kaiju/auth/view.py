import asyncio
import functools
from urllib.parse import urlencode

import aiohttp_jinja2
from aiohttp_session import STORAGE_KEY
from aiohttp import web

from .services import *
from kaiju.helpers import unflatten_dict
from kaiju.json import encoder
from kaiju.rpc.rest import JSONRPCMethodView, JSONRPCView

__all__ = [
    'auth_required', 'LogoutView', 'LoginView', 'ProfileView', 'RegisterView',
    'RestrictedJSONRPCView', 'RestrictedJSONRPCMethodView'
]


def auth_required(method):
    """Auth decorator for custom views."""

    @functools.wraps(method)
    async def wrapper(view, *args, **kwargs):

        storage = view.request.get(STORAGE_KEY)
        redirect = view.request.path
        q = urlencode({'redirect': redirect})
        login_path = f'/login?{q}'

        if storage is None:
            return web.HTTPFound(login_path)
        else:
            session = await storage.load_session(view.request)
            if not session or session['user_id'] is None:
                return web.HTTPFound(login_path)

            view.request.session = session

        return await method(view, *args, **kwargs)

    return wrapper


class LogoutView(web.View):

    login_service = UserService.service_name
    form_template = 'auth/logout.html'
    route_name = 'logout'

    @aiohttp_jinja2.template(form_template)
    async def get(self):
        login_service = self.request.app.services[self.login_service]
        await login_service.logout(self.request)
        login = self.request.app.router[LoginView.route_name].url_for()
        return web.HTTPFound(login)


class ProfileView(web.View):

    login_service = UserService.service_name
    form_template = 'user/profile.html'
    route_name = 'profile'

    @auth_required
    @aiohttp_jinja2.template(form_template)
    async def get(self):
        return self.request.session.get('settings', {})

    @auth_required
    @aiohttp_jinja2.template(form_template)
    async def post(self):
        login_service = self.request.app.services[self.login_service]
        form_data = await self.request.post()
        form_data = dict(form_data)  # не знаю работает ли
        await login_service.update_profile(self.request, form_data)
        return self.request.app.router[self.route_name].url_for()


class RegisterView(web.View):

    login_service = UserService.service_name
    form_template = 'auth/register.html'
    route_name = 'register'

    @auth_required
    @aiohttp_jinja2.template(form_template)
    async def get(self):
        return self.request.session.get('settings', {})

    @auth_required
    @aiohttp_jinja2.template(form_template)
    async def post(self):
        login_service = self.request.app.services[self.login_service]
        form_data = await self.request.post()
        form_data = dict(form_data)  # не знаю работает ли
        await login_service.register(self.request, **form_data)
        redirect = self.request.query.get("redirect", "")
        q = urlencode({'redirect': redirect}) if redirect else ""
        location = self.request.app.router['index'].url_for()
        return web.HTTPFound(f'{location}?{q}')


class LoginView(web.View):

    login_service = UserService.service_name
    session_service = SessionService.service_name
    form_template = 'auth/login.html'
    route_name = 'login'

    @aiohttp_jinja2.template(form_template)
    async def get(self):

        session_service = self.request.app.services[self.session_service]
        session = await session_service.load_session(self.request)

        if session and session['user_id']:
            home_page = self.request.app.router['index'].url_for()
            return web.HTTPFound(home_page)

        return {
            "header_settings": {
                "current_page": "login"
            },
            "login": "",
            "password": "",
            "form_errors": {}
        }

    @aiohttp_jinja2.template(form_template)
    async def post(self):
        form_data = await self.request.post()
        password = form_data.get("password")
        username = form_data.get("login")
        login_service = self.request.app.services[self.login_service]
        await login_service.login(self.request, username=username, password=password)
        redirect = self.request.query.get("redirect", "")
        q = urlencode({'redirect': redirect}) if redirect else ""
        location = self.request.app.router['index'].url_for()
        return web.HTTPFound(f'{location}?{q}')


class RestrictedJSONRPCView(JSONRPCView):
    """JSON RPC HTTP view with session based authorization."""

    sessions = SessionService.service_name
    auth = AuthorizationService.service_name
    login_service = UserService.service_name

    async def post(self):
        """
        User session will be available in `request.session`
        User permissions flat map - `request.permissions`
        User permission map by category - `request.permission_groups`
        """

        login_service = self.request.app.services[self.login_service]

        session, body = await asyncio.gather(
            login_service.get_session(self.request),
            self._get_request_body()
        )
        permissions = session['permissions']
        self.request.session = session
        self.request.permissions = permissions
        self.request.permission_groups = unflatten_dict(permissions)
        rpc = self.request.app[self.rpc_service_name]

        headers, data = await rpc.call(body, self.request.headers, http_request=self.request)
        return web.json_response(data, dumps=encoder, headers=headers, status=200)


class RestrictedJSONRPCMethodView(JSONRPCMethodView):
    """JSON RPC HTTP view with session based authorization."""

    sessions = SessionService.service_name
    auth = AuthorizationService.service_name
    login_service = UserService.service_name

    async def post(self):
        """
        User session will be available in `request.session`
        User permissions flat map - `request.permissions`
        User permission map by category - `request.permission_groups`
        """

        login_service = self.request.app.services[self.login_service]

        session, body = await asyncio.gather(
            login_service.get_session(self.request),
            self._get_request_body()
        )
        permissions = session['permissions']
        self.request.session = session
        self.request.permissions = permissions
        self.request.permission_groups = unflatten_dict(permissions)
        path = self.request.match_info
        rpc = self.request.app[self.rpc_service_name]
        method = path.get('method')

        if method:
            if type(body) is list:
                for r in body:
                    r['method'] = method
            else:
                body['method'] = method

        headers, data = await rpc.call(body, self.request.headers, http_request=self.request)
        return web.json_response(data, dumps=encoder, headers=headers, status=200)
