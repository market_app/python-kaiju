"""Функции для работы с JSON.
"""

from rapidjson import *

try:
    from json import JSONDecodeError
except:
    JSONDecodeError = ValueError


def encoder(value):
    return dumps(
        value, uuid_mode=UM_CANONICAL, ensure_ascii=False, datetime_mode=DM_ISO8601,
        number_mode=NM_DECIMAL, allow_nan=False, default=dict)


def decoder(value):
    return loads(
        value, uuid_mode=UM_CANONICAL, datetime_mode=DM_ISO8601,
        number_mode=NM_DECIMAL, allow_nan=False)


def load(*args, **kws):
    return load(
        *args, uuid_mode=UM_CANONICAL, datetime_mode=DM_ISO8601,
        number_mode=NM_DECIMAL, allow_nan=False, **kws)
