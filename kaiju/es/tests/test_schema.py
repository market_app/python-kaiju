import pytest

from .fixtures import *


@pytest.mark.unit
def test_es_schema(es_test_index, logger):
    idx = es_test_index
    logger.info('Testing index schema and attributes.')
    assert idx.primary_key == idx.primary_field.name == 'id'
    logger.info('Finished tests.')
