import pytest

from .fixtures import *

from ..service import BulkError


@pytest.mark.integration
async def test_es_client_advanced(es_environment, es_document, logger):

    es, idx = es_environment

    logger.info('Testing bulk operations.')

    await es.insert(idx, [es_document('1000')])
    data = await es.get(idx, {'1000'}, fields=[])
    assert data[0]['id'] == '1000'
    await es.update(idx, [{'id': '1', 'percent': 42}])
    data = await es.get(idx, ['1'], fields=['percent'])
    assert len(data) == 1
    data = next(doc for doc in data if doc['id'] == '1')
    assert data['percent'] == 42

    data, bulk_result = await es.bulk({
        'get': [
            {'index': idx, 'doc': {'id': '1000'}}
        ],
        'insert': [
            {'index': idx, 'doc': es_document('1001')}
        ],
        'update': [
            {'index': idx, 'doc': {'id': 1, 'percent': 1}}
        ],
        'delete': [
            {'index': idx, 'doc': {'id': 2}}
        ]
    })

    assert data[idx.alias][0]['id'] == '1000'

    await es.delete(idx, ['1'])
    data = await es.exist(idx, {'1'})
    assert not data

    logger.info('Testing bulk exceptions.')

    with pytest.raises(BulkError) as e:
        await es.update(idx, [
            {'id': '1', 'percent': 2},
            {'id': '2', 'percent': 3},
            {'id': '3', 'percent': 3}])

    e = e.value
    assert '3' in e.accepted
    assert '1' in e.failed
    assert len(e) == len(e.errors) == 2

    logger.info('Testing search operations.')

    data = await es.search(
        idx, q='qween', fuzziness=1, highlight=True, limit=3, fields=[],
        search_fields=['text'], aggs=['tags'])

    logger.info(data)
    assert len(data['data']) <= 3
    assert len(data['highlights']) == len(data['data'])
    key, value = next(iter(data['highlights'].items()))
    assert 'queen' in value
    assert data['data'][0]['id'] == key
    aggs = data['aggs']['tags']
    assert 'a' in aggs or 'b' in aggs

    logger.info('Testing indexing operations')

    idx = await es.reindex(idx, {'settings': {'index': {'refresh_interval': '5s'}}}, update=True)
    name, settings = await es.get_index_settings(idx)
    assert settings['settings']['index']['refresh_interval'] == '5s'
    await es.refresh(idx)
    count = await es.get_index_doc_count(idx)
    assert count == ES_TEST_INDEX_SIZE - 2 + 2

    logger.info('Finished tests.')
