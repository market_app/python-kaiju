import asyncio
import errno
import random
import signal
import uuid
from datetime import datetime, timedelta
from types import SimpleNamespace
from typing import *

import asyncpg
import sqlalchemy as sa
from asyncpg import UniqueViolationError, ForeignKeyViolationError

from kaiju.helpers import fill_template
from kaiju.rpc import AbstractRPCCompatibleWithPermissions, AbstractRPCCompatibleService,\
    JSONRPCInterface, RPCClientError
from kaiju.services import ContextableService
from kaiju.db.services import SQLService
from kaiju.rpc.jsonrpc import *
from kaiju.rest.exceptions import *

from .models import tasks
from .task import Task, State, TaskCommandSet, TaskCommand

__all__ = ['TaskInterfaceService', 'TaskManager', 'TaskExecutor']


class TaskInterfaceService(AbstractRPCCompatibleWithPermissions, SQLService):
    """RPC interface for task table management."""

    service_name = 'tasks'
    table = tasks

    class ErrorCodes(SQLService.ErrorCodes):
        INVALID_COMMAND_FORMAT = 'invalid_command_format'

    class Permissions:
        VIEW_OTHERS_TASKS = 'view_others_tasks'
        MODIFY_OTHERS_TASKS = 'modify_others_tasks'

    DEFAULT_EXEC_TIME = 60  # default max task execution time (sec)
    MAX_EXEC_TIME = 600  # max allowed task execution time (sec)

    def __init__(
            self, app, max_exec_time=MAX_EXEC_TIME,
            default_exec_time=DEFAULT_EXEC_TIME,
            logger=None):

        super().__init__(app=app, logger=logger)
        self.max_exec_time = max(1, int(max_exec_time))
        self.default_exec_time = max(1, int(default_exec_time))

        self._max_exec_time = timedelta(seconds=self.max_exec_time)
        self._default_exec_time = timedelta(seconds=self.default_exec_time)

    @property
    def routes(self) -> dict:
        return {
            'get': self.get_tasks,
            'update': self.update_tasks,
            'create': self.create_tasks,
            'delete': self.delete_tasks,
            'list': self.list_tasks,
            'restart': self.restart_tasks,
            'abort': self.abort_tasks
        }

    def _get_default_deadline(self):
        t = datetime.now() + self._default_exec_time
        return t

    def _get_max_deadline(self):
        t = datetime.now() + self._max_exec_time
        return t

    def _get_deadline(self, deadline):
        if deadline:
            return min(deadline, self._get_max_deadline())
        else:
            return self._get_default_deadline()

    async def create_tasks(
            self, request, data: Union[dict, List[dict]],
            columns: Optional[Union[str, List[str]]] = '*'):

        def _create_task(data, author):
            try:
                cron = data.get('cron')
                cmd = data['cmd']
                deadline = data.get('exec_deadline')
                deadline = self._get_deadline(deadline)
                values = {
                    'name': data.get('name'),
                    'cron': cron,
                    'start_from': data.get('start_from'),
                    'exec_deadline': deadline,
                    'user_id': author
                }
            except KeyError:
                raise ValidationError(
                    'Not enough values in data.',
                    code=self.errors.NOT_ENOUGH_DATA)
            try:
                cmd = TaskCommandSet(**cmd)
            except KeyError:
                raise ValidationError(
                    'Invalid command format.',
                    code=self.errors.INVALID_COMMAND_FORMAT)
            task = Task(cmd, **values)
            if cron:
                task.iter()
            return task.repr()

        author = self.get_user(request)

        if type(data) is dict:
            data = [_create_task(data, author)]
            result = await self._create_objects(data, columns)
            return next(iter(result), None)
        else:
            data = [_create_task(obj, author) for obj in data]
            return await self._create_objects(data, columns)

    async def restart_tasks(self, request, id: Union[uuid.UUID, List[uuid.UUID]], exec_deadline: datetime = None):
        values = {
            'next_run': datetime.now(),
            'exec_deadline': self._get_deadline(exec_deadline)
        }
        sql = self.table.update().values(values).where(
            sa.and_(
                self.table.c.state.in_([State.ABORTED.value, State.FINISHED.value]),
                self.table.c.cron == None
            )
        ).returning(self.table.c.id)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.MODIFY_OTHERS_TASKS):
            sql = sql.where(self.table.c.user_id == author)

        if type(id) is uuid.UUID:
            sql = sql.where(self.table.c.id == id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'Task doesn\'t exist or is not visible.',
                    key=str(id), code=self.errors.NOT_FOUND)
            return data
        else:
            sql = sql.where(self.table.c.id.in_(id))
            return await self._engine.fetch(sql)

    async def abort_tasks(self, request, id: Union[uuid.UUID, List[uuid.UUID]]):
        values = {
            'state': State.ABORTED.value
        }
        sql = self.table.update().values(values).where(
            self.table.c.state == State.IDLE.value
        ).returning(self.table.c.id)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.MODIFY_OTHERS_TASKS):
            sql = sql.where(self.table.c.user_id == author)

        if type(id) is uuid.UUID:
            sql = sql.where(self.table.c.id == id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'Task doesn\'t exist or is not visible.',
                    key=str(id), code=self.errors.NOT_FOUND)
            return data
        else:
            sql = sql.where(self.table.c.id.in_(id))
            return await self._engine.fetch(sql)

    async def update_tasks(
            self, request, id: Union[uuid.UUID, List[uuid.UUID]],
            data: dict, columns: Optional[Union[str, List[str]]] = '*'):

        def _update_tasks(data):
            _data = {}
            _update_fields = {
                'name', 'cron', 'active', 'start_from',
                'exec_deadline'
            }
            for key, value in data.items():
                if key in _update_fields:
                    if key == 'exec_deadline':
                        value = self._get_deadline(value)
                    _data[key] = value
            return _data

        sql = self.table.update().values(_update_tasks(data))

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.MODIFY_OTHERS_TASKS):
            sql = sql.where(self.table.c.user_id == author)

        columns = self._sql_get_columns(columns)

        try:
            if type(id) is uuid.UUID:
                sql = sql.where(self.table.c.id == id)
                if columns:
                    sql = sql.returning(*columns)
                else:
                    sql = sql.returning(self.table.c.id)
                data = await self._engine.fetchrow(sql)
                if not data:
                    raise NotFound(
                        'Task doesn\'t exist or is not visible.',
                        key=str(id), code=self.errors.NOT_FOUND)
                return data
            else:
                sql = sql.where(self.table.c.id.in_(id))
                if columns:
                    sql = sql.returning(*columns)
                    return await self._engine.fetch(sql)
                else:
                    return await self._engine.execute(sql)
        except ForeignKeyViolationError:
            raise ValidationError(
                'Reference object does not exists.',
                code=self.errors.REFERENCE_DOES_NOT_EXISTS)
        except UniqueViolationError:
            raise ValidationError(
                'Object already exists.',
                code=self.errors.EXISTS)

    async def delete_tasks(self, request, id: Union[uuid.UUID, List[uuid.UUID]]):
        sql = self.table.delete().returning(self.table.c.id)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.MODIFY_OTHERS_TASKS):
            sql = sql.where(self.table.c.user_id == author)

        if type(id) is uuid.UUID:
            sql = sql.where(self.table.c.id == id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'Task doesn\'t exist or is not visible.',
                    key=str(id), code=self.errors.NOT_FOUND)
        else:
            sql = self.table.delete().where(self.table.c.id.in_(id))
            data = await self._engine.fetch(sql)
        return data

    async def get_tasks(self, request, id: Union[uuid.UUID, List[uuid.UUID]], columns='*'):
        sql = self.table.select()
        columns = self._sql_get_columns(columns)
        if columns:
            sql = sql.with_only_columns(columns)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.VIEW_OTHERS_TASKS):
            sql = sql.where(self.table.c.user_id == author)

        if type(id) is uuid.UUID:
            sql = sql.where(self.table.c.id == id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'Task doesn\'t exist.',
                    key=str(id), code=self.errors.NOT_FOUND)
        else:
            sql = sql.where(self.table.c.id.in_())
            data = await self._engine.fetch(sql)
        return data

    async def list_tasks(
            self, request, conditions: Union[List[dict], dict] = None,
            sort: Union[Union[Dict[str, str], str], List[Union[Dict[str, str], str]]] = None,
            columns: Optional[Union[str, List[str]]] = '*',
            offset: int = 0, limit: int = 10):

        sql = self.table.select()
        sql = self._sql_paginate(sql, offset, limit)
        if sort:
            sql = self._sql_sort(sql, sort)
        else:
            sql = sql.order_by(self.table.c.id)
        if conditions:
            sql = self._sql_get_conditions(sql, conditions)
        columns = self._sql_get_columns(columns)
        if columns:
            sql = sql.with_only_columns(columns)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.VIEW_OTHERS_TASKS):
            sql = sql.where(self.table.c.user_id == author)

        return await self._engine.fetch(sql)


class TaskManager(AbstractRPCCompatibleService, ContextableService):
    class MessageCodes:
        QUEUED = 'tasks.message.queued'
        ABORT = 'tasks.message.aborted'
        ERROR = 'tasks.message.error'
        EXEC = 'tasks.message.exec'
        FINISHED = 'tasks.message.finished'

    class ErrorCodes(SQLService.ErrorCodes):
        CANT_MODIFY_QUEUED = 'cannot_modify_queued'

    immutable_states = {
        State.QUEUED.value, State.EXEC.value
    }

    service_name = 'taskman'
    tasks_table = tasks

    NOTIFICATION_SERVICE = 'notifications'
    REDIS_SERVICE = 'redis'
    REFRESH_RATE = 1
    ABORT_SIGNAL = signal.SIGINT

    NOTIFY_QUEUED = True
    NOTIFY_ABORTED = True
    NOTIFY_EXECUTED = True
    NOTIFY_FINISHED = True

    def __init__(
            self, app, refresh_rate=REFRESH_RATE,
            notification_service=NOTIFICATION_SERVICE,
            redis_service=REDIS_SERVICE,
            notify_queued=NOTIFY_QUEUED,
            notify_aborted=NOTIFY_ABORTED,
            notify_executed=NOTIFY_EXECUTED,
            notify_finished=NOTIFY_FINISHED,
            logger=None):

        super().__init__(app=app, logger=logger)

        if type(notification_service) is str:
            notification_service = self.app.services[notification_service]
        self._notifications = notification_service

        if type(redis_service) is str:
            redis_service = self.app.services[redis_service]
        self._redis = redis_service

        self._notify_queued = notify_queued
        self._notify_aborted = notify_aborted
        self._notify_executed = notify_executed
        self._notify_finished = notify_finished
        self._refresh_interval = self.set_refresh_rate(refresh_rate)

        self._closing = None
        self._task = None

    @property
    def routes(self) -> dict:
        return {
            'set_refresh_rate': self.set_refresh_rate,
            'acquire': self.acquire_task,
            'write': self.write_task
        }

    async def redis_set_tasks(self, *tasks):

        async with await self._redis.pipeline(transaction=True) as pipe:
            for task_id, deadline in tasks:
                task_id_int = str(task_id.int)
                t = datetime.now().timestamp()
                dt = max(-1, round(deadline.timestamp() - t + 1))
                self.logger.info(
                    'Writing an awaiting Task[%s] to Redis with TTL=%d.',
                    task_id, dt)
                await pipe.set(task_id_int, None)
                await pipe.expire(task_id_int, dt)

            await pipe.execute()

    async def redis_pop_task(self):
        async with await self._redis.pipeline(transaction=True) as pipe:
            await pipe.execute_command('RANDOMKEY')
            result = await pipe.execute()
            key = result[0]
            if key:
                await pipe.delete(key)
                await pipe.execute()
                key = uuid.UUID(int=int(key))
                return key

    async def acquire_task(self, worker_id: uuid.UUID):
        task_id = await self.redis_pop_task()
        if task_id:
            self.logger.info('Acquired awaiting Task[%s] from Redis.', task_id)
            sql = self.tasks_table.update().where(
                self.tasks_table.c.id == task_id
            ).values(
                state=State.EXEC.value,
                state_change=datetime.now(),
                worker_id=worker_id,
                job_id=None
            ).returning(
                sa.literal_column('*')
            )
            task = await self._engine.fetchrow(sql)
            if task and self._notify_executed:
                await self._send_notifications([task], self.MessageCodes.EXEC)
            return task

    async def write_task(self, id, result, exit_code):
        sql = self.tasks_table.update().where(
            self.tasks_table.c.id == id
        ).values(
            state=State.FINISHED.value,
            state_change=datetime.now(),
            result=result,
            exit_code=exit_code,
            worker_id=None,
            job_id=None
        ).returning(
            self.tasks_table.c.id,
            self.tasks_table.c.user_id,
            self.tasks_table.c.notify
        )
        task = await self._engine.fetchrow(sql)
        if task and self._notify_finished:
            await self._send_notifications([task], self.MessageCodes.FINISHED)
        return True

    def set_refresh_rate(self, rate: int):
        self._refresh_interval = max(1, int(rate))
        return self._refresh_interval

    def terminate(self):
        self._closing = True

    async def init(self):
        self._closing = False
        self._task = asyncio.ensure_future(self._loop())

    async def close(self):
        if self._task and not self._task.done():
            self._task.cancel()
        self._task = None
        self._closing = None

    @property
    def _engine(self):
        return self.app['db']

    def _get_time(self):
        return datetime.now()

    async def _loop(self):
        while not self._closing:
            await self._abort_tasks_exceeded_deadline()
            await self._renew_finished_periodic_tasks()
            await self._queue_idle_tasks()
            await asyncio.sleep(self._refresh_interval)

    async def _queue_idle_tasks(self):
        t = self._get_time()
        sql = self.tasks_table.update().where(
            sa.and_(
                self.tasks_table.c.active == True,
                self.tasks_table.c.state == State.IDLE.value,
                self.tasks_table.c.next_run < t,
                sa.or_(
                    self.tasks_table.c.exec_deadline == None,
                    self.tasks_table.c.exec_deadline > t
                ),
                sa.or_(
                    self.tasks_table.c.start_from == None,
                    self.tasks_table.c.start_from < t
                )
            )
        ).values(
            state=State.AWAITING.value,
            state_change=t
        ).returning(
            self.tasks_table.c.id,
            self.tasks_table.c.exec_deadline,
            self.tasks_table.c.user_id,
            self.tasks_table.c.notify
        )
        tasks = await self._engine.fetch(sql)

        if tasks:
            await self.redis_set_tasks(*((task['id'], task['exec_deadline']) for task in tasks))
            if self._notify_queued:
                await self._send_notifications(tasks, self.MessageCodes.QUEUED)

    async def _abort_tasks_exceeded_deadline(self):
        t = self._get_time()
        sql = self.tasks_table.update().where(
            sa.and_(
                self.tasks_table.c.active == True,
                self.tasks_table.c.exec_deadline != None,
                self.tasks_table.c.exec_deadline < t,
                sa.not_(self.tasks_table.c.state.in_([State.ABORTED.value, State.FINISHED.value]
                                                     ))
            )
        ).values(
            state=State.ABORTED.value,
            state_change=t,
            exit_code=128 + self.ABORT_SIGNAL.value,  # UNIX behavior
            result=None
        ).returning(
            self.tasks_table.c.id,
            self.tasks_table.c.user_id,
            self.tasks_table.c.notify
        )
        tasks = await self._engine.fetch(sql)
        if tasks and self._notify_aborted:
            await self._send_notifications(tasks, self.MessageCodes.ABORT)

    async def _renew_finished_periodic_tasks(self):
        t = self._get_time()
        conditions = [
            self.tasks_table.c.active == True,
            self.tasks_table.c.state.in_(
                [State.FINISHED.value, State.ABORTED.value]
            ),
            self.tasks_table.c.cron != None
        ]
        sql = self.tasks_table.select().where(
            sa.and_(*conditions)
        )
        tasks = await self._engine.fetch(sql)

        if tasks:

            tasks = [Task(**task) for task in tasks]
            _sql = self.tasks_table.update()

            for task in tasks:
                task.iter()
                sql = _sql.where(
                    self.tasks_table.c.id == task.id
                ).values(
                    next_run=task.next_run,
                    exec_deadline=task.exec_deadline,
                    state=State.IDLE.value,
                    state_change=t
                )
                await self._engine.execute(sql)

    async def _send_notifications(self, tasks, message):
        _notifications = [
            {
                'message': message,
                'meta': task.get('result'),
                'user_id': task['user_id'],
                'task_id': task['id'],
                'author_id': None
            }
            for task in tasks
            if task.get('notify')
        ]
        if _notifications:
            self.logger.info(
                'Writing %d notifications with status "%s".',
                len(_notifications), message)
            await self._notifications._create_objects(_notifications, columns=None)


class TaskExecutor(AbstractRPCCompatibleWithPermissions, ContextableService):
    service_name = 'executor'

    PERMISSION_DENIED_EXIT_CODE = errno.EACCES
    REFRESH_RATE = 1
    REFRESH_JITTER = REFRESH_RATE / 3

    def __init__(
            self, app, rpc_service='rpc',
            auth_service_client='rpc_client',
            taskman_service_client='rpc_client',
            refresh_rate: int = REFRESH_RATE,
            logger=None):

        super().__init__(app=app, logger=logger)
        if type(rpc_service) is str:
            rpc_service = self.app.services[rpc_service]
        self._rpc = rpc_service
        if type(auth_service_client) is str:
            auth_service_client = self.app.services[auth_service_client]
        self._auth = auth_service_client
        if type(taskman_service_client) is str:
            taskman_service_client = self.app.services[taskman_service_client]
        self._taskman = taskman_service_client

        self.refresh_rate = max(1, int(refresh_rate))

        self._task = None
        self._closing = True

    async def init(self):
        self._closing = False
        self._task = asyncio.ensure_future(self.run())

    async def close(self):
        self._closing = True
        await self._task

    @property
    def routes(self) -> dict:
        return {}

    @property
    def closed(self) -> bool:
        return self._closing

    def _get_headers(self, task):
        headers = {
            JSONRPCInterface.APP_ID_HEADER: self.app['id'],
            JSONRPCInterface.DEADLINE_HEADER: int(task.exec_deadline.timestamp()) + 1
        }
        return headers

    async def run(self):

        while not self._closing:

            task = await self._acquire_task()
            task_id = task['id']

            self.logger.info('Acquired a new Task[%s].', task_id)

            try:
                user = await self._get_user_data(task.user_id)
            except NotFound:
                result = {
                    'id': None,
                    'result': RPCError(
                        id=None,
                        message='Not authorized.',
                        base_exc=UnauthorizedError(
                            'User doesn\'t exist or deactivated.',
                            id=task.user_id,
                            code=self.errors.PERMISSION_DENIED
                        )
                    ),
                    'on_failure_hook': None
                }
                exit_code = self.PERMISSION_DENIED_EXIT_CODE
                state = State.FINISHED
            else:
                request = self._create_fake_request(user)
                results, hooks, status = await self._run_task(task, request)
                results = list(results.items())
                results.sort()

                result = [
                    {
                        'id': cmd_id,
                        'result': result,
                        'on_failure_hook': hooks.get(cmd_id)
                    }
                    for cmd_id, result in results
                ]

                if len(result) == 1:
                    result = next(iter(result))
                elif len(result) == 0:
                    result = None

                exit_code = 0 if status else 1
                state = State.FINISHED

            task.result = result
            task.exit_code = exit_code
            task.change_state(state)

            await self._write_task(task_id, task)
            self.logger.info('Finished Task[%s] with status "%d".', task_id, task.exit_code)

    async def _acquire_task(self) -> Optional[Task]:
        task = None
        self.logger.debug('Acquiring a new task.')
        while not task:
            try:
                task = await self._taskman.call('taskman.acquire', {'worker_id': self.app['id']})
            except RPCClientError as e:
                self.logger.error('Error acquiring a task. %s.', e)
            if task and not isinstance(task, RPCError):
                task = task.result
                if task:
                    task = Task(**task)
                    return task
            await asyncio.sleep(self.refresh_rate + random.random() * self.REFRESH_JITTER)

    async def _write_task(self, task_id, task: Task):
        self.logger.debug('Writing Task[%s] result.', task_id)
        data = {
            'id': task_id,
            'result': task.result,
            'exit_code': task.exit_code
        }
        try:
            await self._taskman.call('taskman.write', data)
        except RPCClientError as e:
            self.logger.error('Error writing Task[%s] result. %s.', task_id, e)

    async def _get_user_data(self, user_id):
        self.logger.debug('Authenticating User[%s].', user_id)
        user = await self._auth.call(
            'auth.get', {
                'user_id': user_id,
                'return_permissions': True,
                'return_custom_permissions': True,
                'format_permissions': False
            }
        )
        self.logger.debug('Authenticated User[%s].')
        if not isinstance(user, RPCError):
            return user.result

    def _create_fake_request(self, user):
        request = SimpleNamespace(
            session={
                'user_id': user['id'],
                'permissions': user['permissions']
            },
            permissions=user['permissions']
        )
        return request

    async def _run_task(self, task: Task, request):

        async def _run_hook(hook):
            headers = {
                JSONRPCInterface.APP_ID_HEADER: self.app['id']
            }
            _h, result = await self._rpc.call(hook, headers, http_request=request)
            return result

        async def _run_hooks(hooks):
            results = {}
            for cmd_id, hook in hooks:
                result = await _run_hook(hook)
                results[cmd_id] = result
            return results

        def _extract_results(results):

            def _extract_data(_result):
                if isinstance(result, RPCResponse):
                    return result.result

            if type(results) is list:
                return [_extract_data(r) for r in results]
            else:
                return _extract_data(results)

        def _errors_in_result(result):
            if isinstance(result, RPCError):
                return True
            elif isinstance(result, list):
                e = next(iter(e for e in result if isinstance(e, RPCError)), None)
                if e:
                    return True
            return False

        self.logger.debug('Running Task[%s].', task.id)

        hooks, results, hook_results, status = [], {}, {}, True

        template_keys = TaskCommand.TemplateKeys

        template_data = {
            template_keys.COMMAND.value: None,
            template_keys.HOOK.value: None,
            template_keys.RESULT.value: None,
            template_keys.PREV_COMMAND.value: None,
            template_keys.PREV_HOOK.value: None,
            template_keys.PREV_RESULT.value: None
        }

        for cmd_id, cmd in enumerate(task.cmd):

            self.logger.debug('Executing stage %d.', cmd_id)

            template_data[template_keys.PREV_COMMAND.value] = template_data[template_keys.COMMAND.value]
            template_data[template_keys.PREV_HOOK.value] = template_data[template_keys.HOOK.value]
            template_data[template_keys.PREV_RESULT.value] = template_data[template_keys.RESULT.value]

            cmd_params = cmd.cmd.get('params', {})
            cmd_params = fill_template(cmd_params, template_data, default=None)
            template_data[template_keys.COMMAND.value] = cmd_params
            cmd.cmd['params'] = cmd_params

            headers = self._get_headers(task)

            # call a method and store its raw results

            self.logger.debug('Making a RPC call with params "%s".', cmd.cmd)

            _h, result = await self._rpc.call(cmd.cmd, headers, http_request=request)
            results[cmd_id] = result

            # extract data and

            data = _extract_results(result)

            template_data[template_keys.RESULT.value] = data
            hook_params = cmd.on_failure_hook.get('params', {}) if cmd.on_failure_hook else {}
            hook_params = fill_template(hook_params, template_data, default=None)
            template_data[template_keys.HOOK.value] = hook_params
            if hook_params:
                cmd.on_failure_hook['params'] = hook_params

            if _errors_in_result(result):

                self.logger.debug('Stage %d execution failed.', cmd_id)

                if not cmd.proceed_on_errors:

                    self.logger.debug('Finalizing Task[%s].', task.id)

                    hook_results = await _run_hooks(hooks)
                    if cmd.on_failure_hook:
                        result = await _run_hook(cmd.on_failure_hook)
                        hook_results[cmd_id] = result
                    status = False

                    break

            # register a hook call if required

            if cmd.on_failure_hook:
                hooks.append((cmd_id, cmd.on_failure_hook))

            # store result for the next command

        return results, hook_results, status
