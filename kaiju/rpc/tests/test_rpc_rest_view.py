import asyncio
import json
from time import time
from uuid import uuid4

import pytest
from aiohttp.cookiejar import DummyCookieJar

from kaiju.json import decoder, encoder
from .. import JSONRPCInterface, JSONQueueServer, JSONRPCView, JSONRPCMethodView
from ..jsonrpc import *
from .fixtures import *


@pytest.mark.benchmark
async def test_rest_rpc_queued_server_performance(
        aiohttp_client, web_application, logger):

    async def _test(parallel=1, requests=1000):

        logger.setLevel('ERROR')
        counter = 0

        async def _do_call(client):

            nonlocal counter
            headers = {
                JSONRPCInterface.APP_ID_HEADER: str(uuid4()),
                JSONRPCInterface.CONTENT_TYPE_HEADER: 'application/json'
            }
            data = {"method": "do.sleep", "params": [1, 2, 3]}
            while 1:
                _id = uuid4()
                data['id'] = _id.int
                headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(_id)
                await client.post(
                    '/rpc', data=encoder(data), headers=headers,
                    skip_auto_headers=['User-Agent'])
                counter += 1

        def _do_sleep(*_, **__):
            return True

        async with JSONQueueServer(max_parallel_tasks=16, logger=logger) as rpc:
            rpc.register_method('do', 'sleep', _do_sleep)
            web_application.router.add_view('/rpc', JSONRPCView)
            web_application.rpc = web_application['rpc'] = rpc
            client = await aiohttp_client(web_application)

            tasks = [
                asyncio.ensure_future(_do_call(client))
                for _ in range(parallel)
            ]

            t0 = time()
            while counter < requests:
                await asyncio.sleep(1)
            t1 = time()

            for task in tasks:
                task.cancel()
            await client.close()
            return t1 - t0, counter

    requests, parallel, n = 5000, 16, 5
    print(f'JSON RPC Queued Service simple benchmark (best of {n}).\n')
    print(f'{parallel} connections')

    dt, counter = await _test(parallel, requests)

    print(f'{round(dt, 2)} s')
    print(f'{counter} requests')
    print(f'{round(counter / dt, 1)} req/sec')


@pytest.mark.unit
async def test_rpc_rest_openapi_schema(rpc_compatible_service, logger):

    async with JSONQueueServer(logger=logger) as rpc:
        service = rpc_compatible_service(logger=logger)
        rpc.register_service(service)
        spec = rpc.get_spec()
        with open('test.json', 'w') as f:
            json.dump(spec, f)


@pytest.mark.unit
async def test_rpc_rest_view(
        aiohttp_client, web_application, rpc_compatible_service, logger):

    logger.info('Testing service context initialization.')

    async with JSONQueueServer(logger=logger) as rpc:

        service = rpc_compatible_service(logger=logger)
        rpc.register_service(service)
        web_application.router.add_view(JSONRPCView.route, JSONRPCView)
        web_application.rpc = web_application['rpc'] = rpc
        client = await aiohttp_client(web_application)

        logger.info('Testing basic functionality.')

        headers = {
            JSONRPCInterface.APP_ID_HEADER: str(uuid4()),
            JSONRPCInterface.CONTENT_TYPE_HEADER: 'application/json'
        }
        _id = uuid4()
        data = {'id': _id.int, 'method': 'm.echo', 'params': [1, 2, 3]}
        headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(_id)
        data = encoder(data)
        response = await client.post(JSONRPCView.route, data=data, headers=headers)
        assert response.status == 200
        text = await response.text()
        body = decoder(text)
        logger.info(body)
        assert body['result'] == [[1, 2, 3], {}]

        logger.info('Testing batch functionality.')

        data = [
            {'id': uuid4().int, 'method': 'm.echo', 'params': [1, 2, 3]},
            {'id': uuid4().int, 'method': 'm.echo', 'params': {'a': 1}},
            {'id': uuid4().int, 'method': 'm.echo', 'params': None}
        ]
        headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(uuid4())
        data = encoder(data)
        response = await client.post(JSONRPCView.route, data=data, headers=headers)
        assert response.status == 200
        text = await response.text()
        body = decoder(text)
        logger.info(body)
        assert [r['result'] for r in body] == [
            [[1, 2, 3], {}],
            [[], {'a': 1}],
            [[], {}]
        ]

        logger.info('All tests finished.')


@pytest.mark.unit
async def test_rpc_rest_method_view(
        aiohttp_client, web_application, rpc_compatible_service, logger):

    logger.info('Testing service context initialization.')

    async with JSONQueueServer(logger=logger) as rpc:

        service = rpc_compatible_service(logger=logger)
        rpc.register_service(service)
        web_application.router.add_view(JSONRPCMethodView.route, JSONRPCMethodView)
        web_application.router.add_view(JSONRPCView.route, JSONRPCMethodView)
        web_application.rpc = web_application['rpc'] = rpc
        client = await aiohttp_client(web_application)

        logger.info('Testing basic functionality.')

        headers = {
            JSONRPCInterface.APP_ID_HEADER: str(uuid4()),
        }
        _id = uuid4()
        data = {'id': _id.int, 'params': [1, 2, 3]}
        headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(_id)
        data = encoder(data)
        response = await client.post('/public/rpc/m.echo', data=data, headers=headers)
        assert response.status == 200
        text = await response.text()
        body = decoder(text)
        logger.info(body)
        assert body['result'] == [[1, 2, 3], {}]

        logger.info('Testing batch functionality.')

        data = [
            {'id': uuid4().int, 'params': [1, 2, 3]},
            {'id': uuid4().int, 'params': {'a': 1}},
            {'id': uuid4().int, 'params': None}
        ]
        headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(uuid4())
        data = encoder(data)
        response = await client.post('/public/rpc/m.echo', data=data, headers=headers)
        assert response.status == 200
        text = await response.text()
        body = decoder(text)
        logger.info(body)
        assert [r['result'] for r in body] == [
            [[1, 2, 3], {}],
            [[], {'a': 1}],
            [[], {}]
        ]

        logger.info('Testing basic functionality with no method in path provided.')

        headers = {
            JSONRPCInterface.APP_ID_HEADER: str(uuid4()),
        }
        _id = uuid4()
        data = {'id': _id.int, 'method': 'm.echo', 'params': [1, 2, 3]}
        headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(_id)
        data = encoder(data)
        response = await client.post('/public/rpc', data=data, headers=headers)
        assert response.status == 200
        text = await response.text()
        body = decoder(text)
        logger.info(body)
        assert body['result'] == [[1, 2, 3], {}]

        _id = uuid4()
        headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(_id)
        data = {'id': _id.int, 'method': 'server.methods.shit', 'params': None}
        response = await client.post('/public/rpc', json=data, headers=headers)
        response = await response.text()
        logger.info(data)
        logger.info(response)

        logger.info('All tests finished.')
