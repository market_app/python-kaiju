import asyncio
from time import time
from uuid import uuid4

import pytest

from .. import JSONRPCInterface, JSONQueueServer
from ..jsonrpc import *
from .fixtures import *


@pytest.mark.benchmark
async def test_rpc_queued_server_performance(performance_test, logger):

    async def _test(parallel=1, requests=1000):

        logger.setLevel('ERROR')

        counter = 0

        async def _do_call(rpc):
            nonlocal counter
            headers = {
                JSONRPCInterface.APP_ID_HEADER: uuid4()
            }
            data = {'method': 'do.sleep', 'params': [1, 2, 3]}
            while 1:
                _id = uuid4()
                data['id'] = _id.int
                headers[JSONRPCInterface.CORRELATION_ID_HEADER] = _id
                await rpc.call(data, headers)
                counter += 1

        def _do_sleep(*_, **__):
            return True

        async with JSONQueueServer(logger=logger) as rpc:
            rpc.register_method('do', 'sleep', _do_sleep)
            tasks = [
                asyncio.ensure_future(_do_call(rpc))
                for _ in range(parallel)
            ]
            t0 = time()
            while counter < requests:
                await asyncio.sleep(0.1)
            t1 = time()
            for task in tasks:
                task.cancel()
            return t1 - t0, counter

    requests, parallel, n = 5000, 16, 5
    print(f'JSON RPC Queued Service simple benchmark (best of {n}).\n')
    print(f'{parallel} connections')

    dt, counter, rps = performance_test(_test, runs=n, args=(parallel, requests))

    print(f'{dt}')
    print(f'{counter} requests')
    print(f'{rps} req/sec')


@pytest.mark.unit
async def test_rpc_queued_server(rpc_compatible_service, logger):
    logger.info('Testing service context initialization.')

    async with JSONQueueServer(
            max_parallel_tasks=5, deadline_check_interval=0.1,
            logger=logger) as rpc:

        service = rpc_compatible_service(logger=logger)
        rpc.register_service(service)

        headers = {
            JSONRPCInterface.APP_ID_HEADER: uuid4(),
            JSONRPCInterface.CORRELATION_ID_HEADER: uuid4()
        }

        logger.info('Testing basic requests.')

        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None}
        _headers, response = await rpc.call(data, headers)
        assert _headers[JSONRPCInterface.APP_ID_HEADER] == str(
            headers[JSONRPCInterface.APP_ID_HEADER])
        assert _headers[JSONRPCInterface.CORRELATION_ID_HEADER] == str(
            headers[JSONRPCInterface.CORRELATION_ID_HEADER])
        assert response.result == ((), {})

        data = {'id': uuid4().int, 'method': 'm.echo', 'params': 42}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert response.result == ((42,), {})

        data = {'id': uuid4().int, 'method': 'm.aecho', 'params': [1, 2, 3]}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert response.result == ((1, 2, 3,), {})

        data = {'id': uuid4().int, 'method': 'm.echo', 'params': {'x': True}}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert response.result == ((), {'x': True})

        logger.info('Testing multi requests.')

        data = [
            {'id': uuid4().int, 'method': 'm.echo', 'params': {'x': True}},
            {'id': uuid4().int, 'method': 'm.echo', 'params': 42},
            {'id': uuid4().int, 'method': 'm.aecho', 'params': [1, 2, 3]}
        ]
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert _headers[JSONRPCInterface.APP_ID_HEADER] == str(
            headers[JSONRPCInterface.APP_ID_HEADER])
        assert _headers[JSONRPCInterface.CORRELATION_ID_HEADER] == str(
            headers[JSONRPCInterface.CORRELATION_ID_HEADER])
        assert [r.result for r in response] == [
            ((), {'x': True}),
            ((42,), {}),
            ((1, 2, 3,), {})
        ]

        logger.info('Testing request error handling.')

        data = {'id': uuid4().int, 'method': 'm.unknown',
                'params': {'x': True}}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert _headers[JSONRPCInterface.APP_ID_HEADER] == str(
            headers[JSONRPCInterface.APP_ID_HEADER])
        assert _headers[JSONRPCInterface.CORRELATION_ID_HEADER] == str(
            headers[JSONRPCInterface.CORRELATION_ID_HEADER])
        assert isinstance(response, RPCMethodNotFound)

        data = {'id': uuid4().int}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RPCInvalidRequest)

        data = {'id': uuid4().int, 'method': 'm.unknown', 'params': True,
                'shit': 1}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RPCInvalidRequest)

        data = {'id': uuid4().int, 'method': 'm.sum',
                'params': {'x': 1, 'z': '2'}}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RPCInvalidParams)

        data = {'id': uuid4().int, 'method': 'm.fail', 'params': None}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RPCInternalError)

        logger.info('Testing timeouts.')

        data = {'id': uuid4().int, 'method': 'm.long_echo', 'params': None}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RPCRequestTimeout)

        data = {'id': uuid4().int, 'method': 'm.long_echo', 'params': None}
        headers[JSONRPCInterface.DEADLINE_HEADER] = 1000
        t = time()
        _headers, response = await rpc.call(data, headers)
        t = time() - t
        assert isinstance(response, RPCRequestTimeout)
        assert t < 1, 'should return immediately, because wrong deadline was specified.'

        data = {'id': uuid4().int, 'method': 'm.long_echo', 'params': None}
        del headers[JSONRPCInterface.DEADLINE_HEADER]
        headers[JSONRPCInterface.REQUEST_TIMEOUT_HEADER] = 4
        _headers, response = await rpc.call(data, headers)
        assert isinstance(response, RPCResponse)

        logger.info('Testing notifications.')

        data = {'id': None, 'method': 'm.echo', 'params': None}
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        t = time()
        response = await rpc.call(data, headers)
        t = time() - t
        assert response is None
        assert t < 1

        logger.info('Testing for parallel task execution')

        tasks = []

        for _ in range(4):
            data = {'id': uuid4().int, 'method': 'm.standard_echo', 'params': None}
            headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
            headers[JSONRPCInterface.CORRELATION_ID_HEADER] = str(uuid4())
            tasks.append(rpc.call(data, headers))

        t = time()
        responses = await asyncio.gather(*tasks)
        t = time() - t
        assert t <= 1

        logger.info('Testing bulk request error handling and notifications.')

        data = [
            {'id': None, 'method': 'm.echo', 'params': None},
            {'id': None, 'method': 'm.long_echo', 'params': None},
            {'id': uuid4().int, 'method': 'm.fail', 'params': None},
            {'id': uuid4().int, 'method': 'm.sum', 'params': {'x': 1, 'y': 2}}
        ]
        t = time()
        headers[JSONRPCInterface.DEADLINE_HEADER] = int(time() + 1)
        _headers, response = await rpc.call(data, headers)
        t = time() - t
        assert isinstance(response[0], RPCRequestTimeout)
        assert isinstance(response[1], RPCError)
        assert response[2].result == 1 + 2

        logger.info('Testing invalid headers.')

        wrong_headers = {
            JSONRPCInterface.DEADLINE_HEADER: 'str'
        }
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None},
        _headers, response = await rpc.call(data, wrong_headers)
        assert isinstance(response, RPCInvalidRequest)

        wrong_headers = {
            JSONRPCInterface.APP_ID_HEADER: 'not uuid'
        }
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None},
        _headers, response = await rpc.call(data, wrong_headers)
        assert isinstance(response, RPCInvalidRequest)

        wrong_headers = {
            JSONRPCInterface.CORRELATION_ID_HEADER: 'not uuid'
        }
        data = {'id': uuid4().int, 'method': 'm.echo', 'params': None},
        _headers, response = await rpc.call(data, wrong_headers)
        assert isinstance(response, RPCInvalidRequest)

        logger.info('All tests finished.')
