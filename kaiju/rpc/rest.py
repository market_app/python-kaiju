from aiohttp.web import json_response

from kaiju.json import encoder
from kaiju.rest.view import AbstractBaseJSONView
from .rpc import JSONRPCInterface

__all__ = ['JSONRPCView', 'JSONRPCMethodView', 'BaseJSONRPCView']


class BaseJSONRPCView(AbstractBaseJSONView):

    rpc_service_name = JSONRPCInterface.service_name


class JSONRPCView(BaseJSONRPCView):
    """A view compatible to the JSON RPC queued server.

    Bind it to an app route to serve JSON RPC requests:

         app.router.add_view('/rpc', AbstractJSONView)

    A body of each request must be a valid JSON RPC object.

    For more info see:

        <https://www.jsonrpc.org/specification>`_

    """

    route = '/public/rpc'

    async def post(self):
        body = await self._get_request_body()
        rpc = self.request.app[self.rpc_service_name]
        headers, data = await rpc.call(body, self.request.headers)
        return json_response(data, dumps=encoder, headers=headers, status=200)


class JSONRPCMethodView(BaseJSONRPCView):
    """A view compatible to the JSON RPC queued server
    but with method name support in URL, which makes it compatible with
    standard doc tools like swagger.

    You can bind it both to an app's method specific and method agnostic
    routes:

         app.router.add_view('/rpc/{method}', JSONRPCMethodView)
         app.router.add_view('/rpc', JSONRPCMethodView)

    In latter case you need to provide the "method" keyword in a request body.

    A body of each request must be a valid JSON RPC object, except that you
    don't need to provide "method" if you call it by a method specific route,
    i.e. the method name is present in the URL.

    For more info see:

        <https://www.jsonrpc.org/specification>`_

    """

    route = '/public/rpc/{method}'

    async def post(self):
        path = self.request.match_info
        method = path.get('method')
        body = await self._get_request_body()
        if method:
            if type(body) is list:
                for r in body:
                    r['method'] = method
            else:
                body['method'] = method
        rpc = self.request.app[self.rpc_service_name]
        headers, data = await rpc.call(body, self.request.headers)
        return json_response(data, dumps=encoder, headers=headers, status=200)
