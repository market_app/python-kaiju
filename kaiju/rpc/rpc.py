import abc
import asyncio
import inspect
import string
from time import time
from uuid import uuid4, UUID
from collections import ChainMap
from traceback import print_tb
from types import SimpleNamespace
from typing import *

import kaiju.rpc.jsonrpc as jsonrpc
from kaiju.helpers import set_value_to_dict, extract_value_from_dict
from kaiju.rpc.jsonrpc import *
from kaiju.rest.exceptions import RESTApiException, UnauthorizedError
from kaiju.services import ContextableService, Service, ServiceOfServices, Contextable, ServiceMeta
from .spec import *

__all__ = [
    'JSONRPCInterface', 'AbstractRPCCompatibleService',
    'AbstractRPCCompatibleWithPermissions',
    'AbstractJSONRPCServer',
    'JSONQueueServer', 'HTTPCompatibleJSONQueueServer',
    'RPCClientError', 'AbstractRPCClientService'
]


class AbstractRPCCompatibleService(Service, abc.ABC):
    """This service is compatible to RPC Interface and can be registered in
    JSONRPCInterface class."""

    @property
    def routes(self) -> dict:
        """A dictionary of service RPC routes."""

        return {}

    @property
    def permissions(self) -> dict:
        # compatibility
        return {}

    def _permissions_wrapper(self):
        return self.permissions

    @property
    def validators(self) -> dict:
        # compatibility
        return {}


class _AbstractRPCCompatibleWithPermissionsMeta(ServiceMeta, abc.ABCMeta):

    def __init__(cls, *args, **kws):
        super().__init__(*args, **kws)
        if abc.ABC not in cls.__bases__:
            cls._format_permission_codes(cls)

    @staticmethod
    def _format_permission_codes(cls):
        codes = {}
        for code in dir(cls.Permissions):
            if not code.startswith('_'):
                value = getattr(cls.Permissions, code)
                codes[code] = cls._create_permission_key(value)
        cls.__Permissions = SimpleNamespace(**codes)
        cls.permission_set = frozenset(codes.values())


class AbstractRPCCompatibleWithPermissions(
    AbstractRPCCompatibleService,
    metaclass=_AbstractRPCCompatibleWithPermissionsMeta):

    __Permissions = None
    permission_set = None

    class ErrorCodes(AbstractRPCCompatibleService.ErrorCodes):
        PERMISSION_DENIED = 'permission_denied'

    class Permissions:
        """Define your custom permissions here."""

    @staticmethod
    def get_user(request):
        return request.session.get('user_id')

    @staticmethod
    def has_permission(request, permission) -> bool:
        if request:
            return request.permissions.get(permission) is True
        else:
            return False

    def check_permission(self, request, permission):
        if not self.has_permission(request, permission):
            raise UnauthorizedError(
                'Method execution restricted for this user.',
                code=self.errors.PERMISSION_DENIED)
        return True

    @classmethod
    def _create_permission_key(cls, key: str):
        return f'{cls.service_name}.{key}'


class JSONRPCInterface(ContextableService, ServiceOfServices, abc.ABC):
    """A simple JSON RPC interface with method execution and management tasks.

    The interface does not contain any transports i.e. send/receive functions.
    """

    service_name = 'rpc'

    MAX_REQUEST_TIME = 60
    DEFAULT_REQUEST_TIME = 1
    DEADLINE_CHECK_INTERVAL = 0.5
    REQUEST_QUEUE_SIZE = 1000
    SYSTEM_METHODS_NAMESPACE = 'server'

    DEADLINE_HEADER = REQUEST_DEADLINE_HEADER
    APP_ID_HEADER = APP_ID_HEADER
    CORRELATION_ID_HEADER = CORRELATION_ID_HEADER
    SERVER_ID_HEADER = SERVER_ID_HEADER
    CONTENT_TYPE_HEADER = CONTENT_TYPE_HEADER
    REQUEST_TIMEOUT_HEADER = REQUEST_TIMEOUT_HEADER

    _zero_length_exception = ValueError('Batch of length 0 received.')

    def __init__(
            self, app=None, max_request_time: int = MAX_REQUEST_TIME,
            default_request_time: int = DEFAULT_REQUEST_TIME,
            deadline_check_interval: int = DEADLINE_CHECK_INTERVAL,
            debug: bool = None, logger=None):
        """
        :param app:
        :param max_request_time: maximum request time in sec for any request
        :param default_request_time: default request time in sec
        :param deadline_check_interval: dead requests checker interval
        :param debug: debug mode, if None then app.debug will be used
        :param logger:
        """

        super().__init__(app, logger=logger)

        self._server_id = str(app['id']) if app and 'id' in app else str(
            uuid4())

        self._max_request_time = max_request_time
        self._default_request_time = default_request_time
        self._deadline_check_interval = deadline_check_interval
        if debug is None:
            debug = self.app.debug
        self.debug = debug

        self._coros, self._functions = {}, {}
        self._methods = ChainMap(self._coros, self._functions)
        self._namespaces = {}
        self._permissions = set()

        self.register_namespace(self.SYSTEM_METHODS_NAMESPACE, {
            'tasks.list': self.tasks_list,
            'tasks.get': self.tasks_get,
            'tasks.cancel': self.tasks_cancel,
            'methods.list': self.methods_list,
            'methods.get': self.methods_get
        })

        self._tasks, self._deadlines = {}, {}
        self._deadline_monitor = None
        self._closed = True

    def register_namespace_from_jsonrpc_interface(self, interface):
        """Registers namespaces from another JSON RPC interface."""

        for namespace, methods in interface._namespaces.items():
            routes = {name: self._methods[name] for name in methods}
            self.register_namespace(namespace, routes)

    def register_namespace(self, namespace: str, routes: dict = None):
        """Registers a namespace with method map in an RPC server."""

        namespace = self._format_namespace(namespace)
        if namespace in self._namespaces:
            _namespace = self._namespaces[namespace]
        else:
            _namespace = self._register_namespace(namespace)

        if routes:
            for method_name, func in routes.items():
                self.register_method(namespace, method_name, func)

    def register_service(self, service: AbstractRPCCompatibleService):
        """Registers an rpc compatible service in the RPC namespaces."""

        self.register_namespace(service.service_name, service.routes)

        if isinstance(service, AbstractRPCCompatibleWithPermissions):
            self._permissions.update(service.permission_set)

    @staticmethod
    def _get_method_name(namespace: str, method: str):
        return f'{namespace}.{method}'

    def register_method(self, namespace: str, name: str, func: Callable):
        """Registers a single method a function in a namespace."""

        namespace = self._format_namespace(namespace)
        name = self._format_namespace(name)

        if namespace not in self._namespaces:
            _namespace = self._register_namespace(namespace)
        else:
            _namespace = self._namespaces[namespace]

        if name in _namespace:
            raise ValueError(
                'RPC method "%s" is already registered in namespace "%s".'
                % (name, namespace))

        full_name = self._get_method_name(namespace, name)

        if inspect.iscoroutinefunction(func):
            self._coros[full_name] = func
        else:
            self._functions[full_name] = func

        _namespace.add(name)

        self.logger.debug('Registered a new RPC method "%s".', full_name)

        return full_name, func

    def get_spec(self, base_spec=base_spec, base_route='/public/rpc') -> dict:
        """Generates a Swagger/OpenAPI spec for all registered RPC methods.

        :param base_spec: base swagger specification
        :param base_route: base API URI for RPC methods
        """

        base_route = base_route.rstrip('/')
        base_components = base_spec['components']
        paths = base_spec['paths']

        for component_type, _components in components.items():
            if component_type in base_components:
                base_components[component_type].update(_components)
            else:
                base_components[component_type] = _components

        tags = []

        paths[base_route] = base_path_spec

        for method_name, method in self._methods.items():
            route = base_route + f'/{method_name}'
            method_spec = get_method_spec(method_name, method)
            paths[route] = method_spec
            tag = f"rpc.{method_name.split('.')[0].lower()}"
            tags.append(tag)
            method_spec['post']['tags'].append(tag)

        tags = [
            {
                'name': tag
            }
            for tag in set(tags)
        ]

        tags.sort(key=lambda x: x['name'])
        tags.insert(0, base_rpc_tag)
        base_spec['tags'].extend(tags)

        return base_spec

    async def init(self):
        self._closed = False
        self._deadline_monitor = asyncio.ensure_future(self._check_queue_ttl())

    async def close(self):
        self._closed = True
        await asyncio.gather(*(task for task in self._tasks.values()),
                             return_exceptions=True)
        self._tasks, self._deadlines = {}, {}
        self._deadline_monitor.cancel()
        self._deadline_monitor = None

    @property
    def closed(self):
        return self._closed

    def get_requested_methods(self, data: Union[List, Dict]) -> frozenset:
        if type(data) is list:
            return frozenset(request['method'] for request in data)
        elif type(data) is dict:
            return frozenset((data['method'],))
        else:
            return frozenset()

    async def on_request(self, app_id: UUID, deadline: int, data: Union[List, Dict]):
        """This method can process both bulk and single requests."""

        if data:
            if type(data) is list:
                result = await asyncio.gather(*(
                    self._on_request(kws, deadline=deadline, app_id=app_id)
                    for kws in data))
                result = [r for r in result if r is not None]
                if result:
                    return result
            else:
                return await self._on_request(data, deadline=deadline, app_id=app_id)
        else:
            return RPCInvalidRequest(None, base_exc=self._zero_length_exception)

    @staticmethod
    def _format_namespace(namespace: str):
        namespace = namespace.strip(string.punctuation).strip(string.whitespace)
        return namespace

    def _register_namespace(self, namespace: str):
        if namespace.lower() == 'rpc':
            raise ValueError('Namespace "rpc" is reserved by an RPC.')
        elif not namespace:
            raise ValueError(
                'Empty or non-alphanumeric namespaces are not allowed.')
        elif namespace in self._namespaces:
            raise ValueError(
                'RPC namespace "%s" is already registered.' % namespace)

        self._namespaces[namespace] = _namespace = set()

        self.logger.debug('Registered a new namespace "%s".', namespace)

        return _namespace

    async def _on_request(self, kws: dict, deadline: int, app_id: UUID) -> \
            Union[RPCResponse, RPCError]:
        """Configured for both request object and json request body."""

        try:
            request = RPCRequest(**kws)
        except TypeError as e:
            return RPCInvalidRequest(kws.get('id'), base_exc=e)

        if self._closed:
            return RPCServerClosing(request.id)

        if deadline < time():
            return RPCRequestTimeout(request.id)

        try:
            method = self._methods[request.method]
        except KeyError:
            return RPCMethodNotFound(request.id, data={'method': request.method})

        task = None

        if request.id:
            id = (app_id, request.id)
            if id in self._tasks:
                task = self._tasks[id]
                self._deadlines[id] = deadline
        else:
            id = (app_id, uuid4().int)

        try:

            if not task:
                request_params = request.params

                try:
                    if request_params is None:
                        result = method()
                    elif type(request_params) is dict:
                        result = method(**request_params)
                    elif type(request_params) is list:
                        result = method(*request_params)
                    else:
                        result = method(request_params)
                except TypeError as exc:
                    return RPCInvalidParams(request.id, base_exc=exc)

                if request.method in self._coros:
                    self._tasks[id] = result = asyncio.ensure_future(result)
                    self._deadlines[id] = deadline
                    result = await result

            elif task.done():
                result = task.result()
            else:
                result = await task

        except asyncio.CancelledError:
            return RPCRequestTimeout(request.id)
        except Exception as exc:
            if self.debug:
                print_tb(exc.__traceback__)
            else:
                self.logger.exception(exc)
            return RPCInternalError(request.id, base_exc=exc, debug=self.debug)
        else:
            if request.id is not None:
                return RPCResponse(request.id, result=result)

    async def _check_queue_ttl(self):
        """Periodically checks awaiting requests for deadlines."""

        check_interval = self._deadline_check_interval

        while 1:
            if self._tasks:
                t = time()
                ttl_id = [id for id, ttl in self._deadlines.items() if t > ttl]
                for id in ttl_id:
                    task = self._tasks[id]
                    if not task.done():
                        task.cancel()
                    del self._deadlines[id]
                    del self._tasks[id]
            await asyncio.sleep(check_interval)

    # -- server special RPC methods --

    def tasks_list(self):
        """Lists all currently running tasks."""

        return [
            self.tasks_get(*_id)
            for _id, task in self._tasks.items()
        ]

    def tasks_get(self, app_id, request_id: int):
        """Returns a status of a single running task.

        :param app_id: application identifier (all hashable types are ok)
        :param request_id: RPC request identifier (integer)
        """

        _id = (app_id, request_id)
        task, deadline = self._tasks[_id], self._deadlines[_id]
        return {
            'app_id': app_id,
            'request_id': request_id,
            'deadline': deadline,
            'finished': task.done()
        }

    def tasks_cancel(self, app_id, request_id):
        """Cancels a task.

        :param app_id: application identifier (all hashable types are ok)
        :param request_id: RPC request identifier (integer)
        :returns True:
        """

        self.logger.debug('Cancels app %s request %s.', app_id, request_id)
        _id = (app_id, request_id)
        task, deadline = self._tasks.pop(_id), self._deadlines.pop(_id)
        task.cancel()
        self.logger.info('Cancelled app %s request %s.', app_id, request_id)
        return True

    def methods_list(self):
        """Lists all available methods."""

        methods = list(self._methods.keys())
        methods.sort()
        return methods

    def methods_get(self, method: str):
        """Returns a registered RPC method specification.

        :param method: method name as registered in RPC
        """

        m = self._methods[method]
        return {
            'method': method,
            'doc': inspect.getdoc(m),
            'spec': inspect.getfullargspec(m).__repr__()
        }


class AbstractJSONRPCServer(JSONRPCInterface):
    """A simple JSON RPC Server.
    """

    service_name = 'rpc'

    MAX_REQUEST_QUEUE_SIZE = 1000
    MAX_PARALLEL_TASKS = 1
    MAX_REQUEST_LOADERS = 1
    MAX_RESPONSE_SENDERS = 1

    def __init__(
            self, *args,
            max_request_queue_size: int = MAX_REQUEST_QUEUE_SIZE,
            max_parallel_tasks: int = MAX_PARALLEL_TASKS,
            max_loaders: int = MAX_REQUEST_LOADERS,
            max_senders: int = MAX_RESPONSE_SENDERS,
            **kws):
        """See JSONRPCInterface for more options.

        :param max_request_queue_size: max number of requests in queue
        :param max_parallel_tasks: maximum number of parallel processed tasks
        :param max_loaders: maximum number of parallel request data loaders
        :param max_senders: maximum number of parallel response senders
        """

        super().__init__(*args, **kws)

        self._max_request_queue_size = max_request_queue_size
        self._max_parallel_tasks = max_parallel_tasks
        self._max_loaders = max_loaders
        self._max_senders = max_senders

        self._output_queue = None
        self.request_queue = None
        self._workers = None
        self._loaders = None
        self._senders = None

        self._closing = False

    async def init(self):
        self._closing = False
        self.request_queue = asyncio.Queue(
            maxsize=self._max_request_queue_size)
        self._output_queue = asyncio.Queue(
            maxsize=self._max_request_queue_size)
        self._workers = [
            asyncio.ensure_future(self._worker())
            for _ in range(self._max_parallel_tasks)
        ]
        self._loaders = [
            asyncio.ensure_future(self._loader())
            for _ in range(self._max_loaders)
        ]
        self._senders = [
            asyncio.ensure_future(self._sender())
            for _ in range(self._max_senders)
        ]
        await super().init()

    async def close(self):
        self._closing = True
        for worker in self._loaders:
            worker.cancel()
        await asyncio.sleep(0.1)
        self._loaders = None
        await self.request_queue.join()
        self.request_queue = None
        await asyncio.gather(*(
            task for task in self._tasks.values()
        ), return_exceptions=True)
        self._closed = True
        self._tasks, self._deadlines = {}, {}
        self._deadline_monitor.cancel()
        self._deadline_monitor = None
        for worker in self._workers:
            worker.cancel()
        await self._output_queue.join()
        self._output_queue = None
        for worker in self._senders:
            worker.cancel()
        self._workers = None

    async def _worker(self):
        request_queue = self.request_queue
        output_queue = self._output_queue
        while 1:
            data = await request_queue.get()
            try:
                result = await self.on_request(*data)
                await output_queue.put(result)
            finally:
                request_queue.task_done()

    @abc.abstractmethod
    async def _loader(self):
        """The loader should get data from the client and put to the
        request queue."""

        while 1:
            data = ...
            await self.request_queue.put(data)

    @abc.abstractmethod
    async def _sender(self):
        """The sender should get data from the output queue and send
        it somehow to the client's callback."""

        while 1:
            headers, response = await self._output_queue.get()
            ...

    def _process_headers(self, headers):

        t = int(time()) + 1

        if headers:
            deadline = headers.get(self.DEADLINE_HEADER)
            timeout = headers.get(self.REQUEST_TIMEOUT_HEADER)
            if deadline:
                deadline = min(self._max_request_time + t, deadline)
            elif timeout:
                timeout = min(self._max_request_time, timeout)
                deadline = t + timeout
            else:
                deadline = t + self._default_request_time

            correlation_id = headers.get(self.CORRELATION_ID_HEADER)
            if correlation_id:
                correlation_id = correlation_id if type(
                    correlation_id) is UUID else UUID(correlation_id)
            else:
                correlation_id = uuid4()

            app_id = headers.get(self.APP_ID_HEADER)
            if app_id:
                app_id = app_id if type(app_id) is UUID else UUID(app_id)
            else:
                app_id = uuid4()

            correlation_id = UUID(
                str(correlation_id)) if correlation_id else uuid4()
        else:
            deadline = int(t + self.DEFAULT_REQUEST_TIME)
            app_id = uuid4()
            correlation_id = uuid4()

        return app_id, correlation_id, deadline


class JSONQueueServer(AbstractJSONRPCServer):
    """A simple JSON RPC server with queued processing."""

    def __init__(self, *args, **kws):
        """See AbstractJSONRPCServer for more options."""

        super().__init__(*args, **kws)

        self._callbacks = {}

        self.register_namespace(self.SYSTEM_METHODS_NAMESPACE, {
            'callbacks.list': self.callbacks_list,
            'callbacks.get': self.callbacks_get
        })

    async def close(self):
        await super().close()
        self._callbacks = None

    async def call(self, data: Union[List, Dict], headers: dict = None):
        """This method can process both bulk and single requests.

        :returns Union[RPCResponse, RPCError]: in case of single request
        :returns List[Union[RPCResponse, RPCError]]: in case of bulk request
        :returns None: if request(s) has "notify" status i.e. request_id wasn't
            specified
        """

        try:
            app_id, correlation_id, deadline = self._process_headers(headers)
        except (ValueError, TypeError) as err:
            return {}, RPCInvalidRequest(None, base_exc=err)

        _id = (app_id, correlation_id)
        if _id in self._callbacks:
            queue = self._callbacks[_id][0]
        else:
            queue = asyncio.Queue()
            self._callbacks[_id] = (queue, deadline)
        await self.request_queue.put((app_id, correlation_id, deadline, data))
        result = await queue.get()
        if _id in self._callbacks:
            del self._callbacks[_id]
        if result:
            headers = {
                self.APP_ID_HEADER: str(app_id),
                self.CORRELATION_ID_HEADER: str(correlation_id),
                self.SERVER_ID_HEADER: self._server_id
            }
            return headers, result

    async def _loader(self):
        """Not used."""

        return

    async def _worker(self):

        request_queue = self.request_queue
        callbacks = self._callbacks

        while 1:
            app_id, correlation_id, deadline, data = await request_queue.get()
            try:
                result = await self.on_request(app_id, deadline, data)
                _id = (app_id, correlation_id)
                if _id in callbacks:
                    callbacks[_id][0].put_nowait(result)
            finally:
                request_queue.task_done()

    async def _sender(self):
        """Not used."""

        return

    async def _check_queue_ttl(self):
        """Periodically checks awaiting requests for deadlines."""

        check_interval = self._deadline_check_interval
        tasks = self._tasks
        deadlines = self._deadlines
        callbacks = self._callbacks

        while 1:

            if self._tasks:
                t = time()
                ttl_id = [id for id, ttl in deadlines.items() if t > ttl]
                for id in ttl_id:
                    task = tasks[id]
                    if not task.done():
                        task.cancel()
                    del tasks[id]
                    del deadlines[id]

            # to both give client some time to receive a timeout message
            # and then remove the queue from the callback map
            await asyncio.sleep(check_interval)

            t = time()
            ttl_id = [(_id, queue) for _id, (queue, deadline) in
                      callbacks.items() if t > deadline + check_interval]
            for _id, queue in ttl_id:
                queue.put_nowait(RPCRequestTimeout(None))
                del callbacks[_id]

    # -- server special RPC methods --

    def callbacks_list(self):
        """Lists all currently active callbacks."""

        return [
            {
                'app_id': app_id,
                'correlation_id': correlation_id
            }
            for app_id, correlation_id in self._callbacks.keys()
        ]

    def callbacks_get(self, app_id: UUID, correlation_id: UUID):
        """Returns a status of a single callback.

        :param app_id: application identifier (all hashable types are ok)
        :param correlation_id: RPC request correlation id (for a whole batch)
        """

        _id = (app_id, correlation_id)
        callback_queue, deadline = self._callbacks[_id]
        return {
            'app_id': app_id,
            'correlation_id': correlation_id,
            'deadline': deadline
        }


class HTTPCompatibleJSONQueueServer(JSONQueueServer, AbstractRPCCompatibleWithPermissions):
    """Same as JSON Queue Server for RPC but can pass a http request argument.

    How to use:

    1. Declare a rpc method with 'request' as a first argument
    (to customize change `HTTPCompatibleJSONQueueServer.HTTP_REQUEST_ARG_NAME`)

        def some_rpc_method(request, x, y):
            ...

    2. When calling a method you can pass a request as an additional argument

        await rpc.call(body, headers, request=request)

    The rpc service will automatically determine whether a method needs a
    request data or not depending on the method's signature, so you could use
    a `request.session`, `request.permissions`
    and other HTTP request data in your custom method logic.

    Usually you don't need to be bothered by 2, just use a default
    `kaiju.auth.views.RestrictedJSONRPCView` HTTP view class.

    """

    HTTP_REQUEST_ARG_NAME = 'request'
    CHECK_PERMISSIONS_BEFORE_REQUEST = True

    def __init__(
            self, *args,
            check_permissions_before_request=CHECK_PERMISSIONS_BEFORE_REQUEST,
            **kws):
        self._http_compatible_methods = set()
        super().__init__(*args, **kws)
        self.check_permissions_before_request = check_permissions_before_request

    @property
    def routes(self) -> dict:
        pass

    def register_method(self, namespace: str, name: str, func: Callable):
        """Registers a single method a function in a namespace."""

        name, func = super().register_method(namespace, name, func)
        args = inspect.getfullargspec(func).args

        if args:
            if args[0] in {'self', 'cls'} and len(args) > 1:
                _arg = args[1]
            else:
                _arg = args[0]
            if self.HTTP_REQUEST_ARG_NAME == _arg:
                self._http_compatible_methods.add(name)

        return name, func

    async def call(self, data: Union[List, Dict], headers: dict = None, http_request=None):
        """This method can process both bulk and single requests.

        :returns Union[RPCResponse, RPCError]: in case of single request
        :returns List[Union[RPCResponse, RPCError]]: in case of bulk request
        :returns None: if request(s) has "notify" status i.e. request_id wasn't
            specified
        """

        try:
            app_id, correlation_id, deadline = self._process_headers(headers)
        except (ValueError, TypeError) as err:
            return {}, RPCInvalidRequest(None, base_exc=err)

        _id = (app_id, correlation_id)
        if _id in self._callbacks:
            queue = self._callbacks[_id][0]
        else:
            queue = asyncio.Queue()
            self._callbacks[_id] = (queue, deadline)
        await self.request_queue.put((app_id, correlation_id, deadline, data, http_request))
        result = await queue.get()
        if _id in self._callbacks:
            del self._callbacks[_id]
        if result:
            headers = {
                self.APP_ID_HEADER: str(app_id),
                self.CORRELATION_ID_HEADER: str(correlation_id),
                self.SERVER_ID_HEADER: self._server_id
            }
            return headers, result

    async def _worker(self):

        request_queue = self.request_queue
        callbacks = self._callbacks

        while 1:
            app_id, correlation_id, deadline, data, http_request = await request_queue.get()
            try:
                result = await self.on_request(app_id, deadline, data, http_request)
                _id = (app_id, correlation_id)
                if _id in callbacks:
                    callbacks[_id][0].put_nowait(result)
            finally:
                request_queue.task_done()

    async def on_request(
            self, app_id: UUID, deadline: int, data: Union[List, Dict],
            http_request=None):
        """This method can process both bulk and single requests."""

        if data:
            if type(data) is list:
                result = await asyncio.gather(*(
                    self._on_request(kws, deadline, app_id, http_request)
                    for kws in data))
                result = [r for r in result if r is not None]
                if result:
                    return result
            else:
                return await self._on_request(data, deadline, app_id, http_request)
        else:
            return RPCInvalidRequest(None, base_exc=self._zero_length_exception)

    async def _on_request(
            self, kws: dict, deadline: int, app_id: UUID,
            http_request=None) -> Union[RPCResponse, RPCError]:
        """Configured for both request object and json request body."""

        try:
            request = RPCRequest(**kws)
        except TypeError as e:
            return RPCInvalidRequest(kws.get('id'), base_exc=e)

        try:
            method = self._methods[request.method]
        except KeyError:
            return RPCMethodNotFound(request.id, data={'method': request.method})

        if self.check_permissions_before_request:
            try:
                self.check_permission(http_request, request.method)
            except UnauthorizedError as e:
                return RPCPermissionDenied(request.id, base_exc=e)

        if deadline < time():
            return RPCRequestTimeout(request.id)

        task = None

        if request.id:
            id = (app_id, request.id)
            if id in self._tasks:
                task = self._tasks[id]
                self._deadlines[id] = deadline
        else:
            id = (app_id, uuid4().int)

        try:

            if not task:
                request_params = request.params

                try:

                    if request.method in self._http_compatible_methods:
                        if request_params is None:
                            result = method(http_request)
                        elif type(request_params) is dict:
                            result = method(http_request, **request_params)
                        elif type(request_params) is list:
                            result = method(http_request, *request_params)
                        else:
                            result = method(http_request, request_params)
                    else:
                        if request_params is None:
                            result = method()
                        elif type(request_params) is dict:
                            result = method(**request_params)
                        elif type(request_params) is list:
                            result = method(*request_params)
                        else:
                            result = method(request_params)
                except TypeError as exc:
                    return RPCInvalidParams(request.id, base_exc=exc)

                if request.method in self._coros:
                    self._tasks[id] = result = asyncio.ensure_future(result)
                    self._deadlines[id] = deadline
                    result = await result

            elif task.done():
                result = task.result()
            else:
                result = await task

        except asyncio.CancelledError:
            return RPCRequestTimeout(request.id)
        except Exception as exc:
            if self.debug:
                print_tb(exc.__traceback__)
            else:
                self.logger.exception(exc)
            return RPCInternalError(request.id, base_exc=exc)
        else:
            if request.id is not None:
                return RPCResponse(request.id, result=result)


class RPCClientError(RESTApiException):
    """JSON RPC Python exception class. Catch it if you can!"""

    def __init__(self, *args, response=None, **kws):
        super().__init__(*args, **kws)
        self.response = response

    def __str__(self):
        return self.message


class AbstractRPCClientService(AbstractRPCCompatibleService, Contextable, abc.ABC):

    service_name = 'client'

    def __init__(self, app, auth=None, logger=None):
        super().__init__(app=app, logger=logger)
        self.auth = auth

    @property
    def routes(self) -> dict:
        return {
            'call': self.call,
            'call_multiple': self.call_multiple
        }

    async def init(self):
        await self.init_session()
        if self.auth:
            await self.authenticate()

    async def close(self):
        await self.close_session()

    @abc.abstractmethod
    async def rpc_request(
            self, request: Union[jsonrpc.RPCRequest, List[jsonrpc.RPCRequest]],
            headers: Optional[dict]) -> Union[dict, list]:
        """This method depends on a specific transport."""

    async def init_session(self):
        """This method should initialize a session or a pool upon init."""

    async def close_session(self):
        """This method should close the session if required."""

    async def authenticate(self):
        """This method should perform authorization upon a service init.
        Leave it if no authorization is required."""

    async def iter_call(
            self, method: str, params: Any = None, headers: dict = None,
            offset: int = 0, limit: int = 10,
            count_key='count', offset_key='offset', limit_key='limit',
            raise_exception=True, unpack_response=True) -> AsyncGenerator:
        """Iterates over data."""

        count = offset + 1

        if params is None:
            params = {}

        while count > offset:
            set_value_to_dict(params, offset_key, offset)
            set_value_to_dict(params, limit_key, limit)
            data = await self.call(
                method=method, params=params, headers=headers,
                raise_exception=raise_exception, unpack_response=unpack_response)
            if type(data) is RPCResponse:
                count = extract_value_from_dict(data.result, count_key, default=0)
            elif isinstance(data, RPCError):
                count = 0
            else:
                count = extract_value_from_dict(data, count_key, default=0)
            yield data

            offset += limit

    async def call(
            self, method: str, params: Any = None, id=False, headers: dict = None,
            raise_exception=True, unpack_response=True):
        """Call a single remote RPC method.

        :param method: RPC method name
        :param params: any RPC parameters
        :param headers: headers, see `kaiju.rpc.spec` for a list of available
            headers
        :param id: optional request id
        :param raise_exception: if True, then it will rise an exception if
            any errors have been returned
        :param unpack_response: if True, then in case of valid response
            not RPCResponse objects but actual result data will be returned

        :raises RPCException: if `raise_exception` was set to True and errors
        have been returned
        """

        request = jsonrpc.RPCRequest(id, method, params)
        response = await self.rpc_request(request, headers)
        response = self._process_response(response, unpack_response=unpack_response)
        if isinstance(response, jsonrpc.RPCError) and raise_exception:
            self._raise_error(response, [response], [response])
        return response

    async def call_multiple(
            self, *requests: dict, headers: dict = None,
            raise_exception=True, unpack_response=True):
        """Call multiple remote RPC methods in a single batch.

        :param requests: list of request objects (see `JSONRPCHTTPClient.call`
            for each request parameters)
        :param headers: headers, see `kaiju.rpc.spec` for a list of available
            headers
        :param raise_exception: if True, then it will rise an exception if
            any errors have been returned
        :param unpack_response: if True, then in case of valid response
            not RPCResponse objects but actual result data will be returned

        :raises RPCException: if `raise_exception` was set to True and errors
            have been returned
        """

        request = [
            jsonrpc.RPCRequest(**data)
            for data in requests
        ]
        response = await self.rpc_request(request, headers)
        errors, responses = self._process_response_batch(response, unpack_response=unpack_response)
        if errors and raise_exception:
            self._raise_error(errors[0], errors, responses)
        return response

    def _raise_error(self, error, errors, responses):
        message = str(error)
        if 'base_exc_message' in error.data:
            base_type = error.data['base_exc_type']
            msg = error.data["base_exc_message"]
            message = f'{message} [{base_type}] {msg}'
        self.logger.error(message)
        raise RPCClientError(message, errors=errors, responses=responses)

    @classmethod
    def _process_response_batch(cls, batch: List[dict], unpack_response: bool):
        errors, responses = [], []
        for data in batch:
            response = cls._process_response(data, unpack_response)
            if isinstance(response,  jsonrpc.RPCError):
                errors.append(response)
            responses.append(response)
        return errors, responses

    @staticmethod
    def _process_response(response: dict, unpack_response: bool):
        if 'error' in response:
            error = response['error']
            error_type = error.get('data', {}).get('type', jsonrpc.RPCError.__name__)
            error_type = getattr(jsonrpc, error_type, jsonrpc.RPCError)
            response = error_type(id=response['id'], **error)
        else:
            response = jsonrpc.RPCResponse(**response)
            if unpack_response:
                response = response.result
        return response
