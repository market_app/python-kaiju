import io
import functools
from typing import List


import sqlalchemy as sa
from asyncpg.exceptions import UndefinedTableError


def dump_sql(func, bind=False):
    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):
        out = io.StringIO()

        # noinspection PyUnusedLocal
        def dump(sql, *multiparams, **params):
            out.write('{};\n'.format(str(sql.compile(dialect=dump.dialect)).strip()))

        engine = sa.create_engine('postgres://', strategy='mock',
                                  executor=dump)
        dump.dialect = engine.dialect

        if bind:
            func(*args, bind=engine, **kwargs)
        else:
            func(engine, *args, **kwargs)

        return out.getvalue()
    return func_wrapper


async def check_table_exists(connection, table) -> bool:
    """Проверяет, существует ли таблица в базе.
    """

    try:
        await connection.execute(sa.select([table]).limit(1))
    except UndefinedTableError:
        return False
    else:
        return True


async def check_db_exists(connection, name: str) -> bool:
    """Проверяет, существует ли база данных."""

    try:
        result = await connection.fetchrow(f"SELECT 1 FROM pg_database WHERE datname = '{name}';")
    except Exception as err:
        raise RuntimeError('ошибка при выполнении команды "_check_db_exists": %s' % err)
    return result is not None


async def create_db(connection, name: str):
    try:
        await connection.execute(f"CREATE DATABASE {name} WITH ENCODING 'UTF8';")
    except Exception as err:
        raise RuntimeError('ошибка при создании базы %s' % name) from err


async def create_table(connection, metadata, tables: list):
    """Создание таблицы"""
    async with connection.transaction():
        await connection.execute(
            dump_sql(metadata.create_all, bind=True)(tables=tables)
        )


async def check_table_health(connection, table) -> List[Exception]:
    errors = []
    try:
        data = await connection.fetchrow(table.select().limit(1))
    except Exception as err:
        err = RuntimeError('ошибка при соединении с таблицей %s: %s' % (table.name, err))
        errors.append(err)
    else:
        if not data:
            err = Warning('нет данных в таблице %s' % table.name)
            errors.append(err)
    return errors
