import abc
import uuid
from typing import *

import sqlalchemy as sa
from asyncpg import ForeignKeyViolationError, UniqueViolationError

from kaiju.rpc.rpc import AbstractRPCCompatibleService
from kaiju.rest.exceptions import ValidationError, NotFound
from kaiju.jsonschema import *

__all__ = ['SQLService']


class SQLService(AbstractRPCCompatibleService, abc.ABC):
    """Just a base SQL service interface with common commands and errors."""

    table = None  #: here should be your table
    virtual_columns = {}

    class ErrorCodes(AbstractRPCCompatibleService.ErrorCodes):
        FIELD_DOES_NOT_EXISTS = 'field_does_not_exists'
        NOT_FOUND = 'not_found'
        EXISTS = 'exists'
        INVALID_ORDERING_COMMAND = 'invalid_ordering_command'
        INVALID_PAGINATION_OFFSET = 'invalid_pagination_offset'
        INVALID_PAGINATION_LIMIT = 'invalid_pagination_limit'
        NOT_ENOUGH_DATA = 'not_enough_data'
        REFERENCE_DOES_NOT_EXISTS = 'reference_object_does_not_exists'

    @property
    def _engine(self):
        return self.app['db']

    async def _list_objects(
            self, conditions: Union[List[dict], dict] = None,
            sort: Union[Union[Dict[str, str], str], List[Union[Dict[str, str], str]]] = None,
            columns: Optional[Union[str, List[str]]] = '*',
            offset: int = 0, limit: int = 10):

        sql = self.table.select()
        if columns:
            columns = self._sql_get_columns(columns)
            if columns:
                sql = sql.with_only_columns(columns)
        sql = self._sql_paginate(sql, offset, limit)
        if sort:
            sql = self._sql_sort(sql, sort)
        else:
            sql = sql.order_by(self.table.c.id)
        if conditions:
            sql = self._sql_get_conditions(sql, conditions)
        return await self._engine.fetch(sql)

    async def _delete_objects(self, id: Union[Any, Collection], columns=None):
        sql = self.table.delete()
        columns = self._sql_get_columns(columns)

        if isinstance(id, Collection) and not type(id) is str:
            sql = sql.where(self.table.c.id.in_(id))
            if columns:
                sql = sql.returning(*columns)
                data = await self._engine.fetch(sql)
                return data
        else:
            sql = sql.where(self.table.c.id == id)
            if columns:
                sql = sql.returning(*columns)
                data = await self._engine.fetchrow(sql)
                if not data:
                    raise NotFound(
                        'Task doesn\'t exist or is not visible.',
                        key=str(id), code=self.errors.NOT_FOUND)
                return data

        await self._engine.execute(sql)

    async def _get_objects(self, id: Union[Any, Collection], columns='*'):
        sql = self.table.select()
        columns = self._sql_get_columns(columns)
        if columns:
            sql = sql.with_only_columns(columns)
        if isinstance(id, Collection) and not type(id) is str:
            sql = sql.where(self.table.c.id.in_(id))
            data = await self._engine.fetch(sql)
        else:
            sql = sql.where(self.table.c.id == id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'File doesn\'t exist.',
                    key=str(id), code=self.errors.NOT_FOUND)
        return data

    async def _create_objects(self, data, columns):
        sql = self.table.insert().values(data)
        columns = self._sql_get_columns(columns)
        try:
            if columns is not None:
                sql = sql.returning(*columns)
                return await self._engine.fetch(sql)
            else:
                return await self._engine.execute(sql)
        except ForeignKeyViolationError:
            raise ValidationError(
                'Reference object does not exists.',
                code=self.errors.REFERENCE_DOES_NOT_EXISTS)
        except UniqueViolationError:
            raise ValidationError(
                'Object already exists.',
                code=self.errors.EXISTS)

    def _sql_get_column(self, column: str):
        try:
            return self.table.columns[column]
        except KeyError:
            raise ValidationError(
                'Requested field doesn\'t exists',
                field=column,
                code=self.ErrorCodes.FIELD_DOES_NOT_EXISTS)

    def _sql_get_columns(self, columns):
        if columns == '*':
            virtual = [
                sa.text(value + f' AS {name}')
                for name, value in self.virtual_columns.items()
            ]
            return [sa.literal_column('*'), *virtual]
        elif columns is None:
            return []
        elif type(columns) is str:
            if columns in self.virtual_columns:
                return [sa.text(self.virtual_columns[columns] + f' AS {columns}')]
            else:
                return [self._sql_get_column(columns)]
        else:
            _columns = []
            for key in set(columns):
                if key in self.virtual_columns:
                    _columns.append(sa.text(self.virtual_columns[key] + f' AS {key}'))
                else:
                    _columns.append(self._sql_get_column(key))
            return _columns

    def _sql_paginate(self, sql, offset, limit):
        if offset:
            try:
                offset = max(0, int(offset))
            except ValueError:
                raise ValidationError(
                    'Invalid offset pagination.',
                    key='offset', value=offset,
                    code=self.ErrorCodes.INVALID_PAGINATION_OFFSET)
            sql = sql.offset(offset)
        if limit:
            try:
                limit = max(1, int(limit))
            except ValueError:
                raise ValidationError(
                    'Invalid limit pagination.',
                    key='limit', value=limit,
                    code=self.ErrorCodes.INVALID_PAGINATION_LIMIT)
            sql = sql.limit(limit)
        return sql

    def _sql_sort(self, sql, ordering):

        def _get_ordering(o):
            if isinstance(o, dict):
                key, name = next(iter(o.items()), None)
            else:
                key = 'asc'
                name = o
            if type(name) is str:
                column = self._sql_get_column(name)
            else:
                column = name
            if key == 'desc':
                column = sa.desc(column)
            elif key == 'asc':
                pass
            else:
                raise ValidationError(
                    'Invalid ordering requested.',
                    value=key,
                    code=self.ErrorCodes.INVALID_ORDERING_COMMAND)
            return column

        if type(ordering) is list:
            sql = sql.order_by(*(_get_ordering(o) for o in ordering))
        else:
            sql = sql.order_by(_get_ordering(ordering))
        return sql

    def _sql_get_conditions(self, sql, conditions):

        def _get_conditions(condition):
            if type(condition) is dict:
                _condition = []
                for key, value in condition.items():
                    column = self._sql_get_column(key)
                    if type(value) is list:
                        _c = column.in_(value)
                    else:
                        _c = column == value
                    _condition.append(_c)
                return sa.and_(*_condition)
            elif type(condition) is list:
                _condition = [_get_conditions(c) for c in condition]
                return sa.or_(*_condition)
            else:
                column = self._sql_get_column(condition)
                return column.isnot(None)

        conditions = _get_conditions(conditions)
        return sql.where(conditions)
