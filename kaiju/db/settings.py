DB = {
    'settings': {
        'tablespace': '',
        'user': 'root',  # TODO: fix default user
    },
    'pool': {
        'host': 'localhost',
        'port': 5432,
        'database': 'engine',
        'min_size': 4,
        'max_size': 64,
        'max_queries': 100000,
        'max_inactive_connection_lifetime': 30
    },
    'users': {
        'root': {
            'credentials': {
                'user': 'postgres',
                'password': 'postgres'
            },
            'role': 'rwx',
            'comment': 'рутовый юзер, которому доступны все действия,'
                       ' включая создание и удаление таблиц, данный юзер'
                       ' должен быть создан вручную'
        },
        'default': {
            'credentials': {
                'user': 'engine_default',
                'password': 'engine_default'
            },
            'role': 'rw',
            'comment': 'пользователь для обычных операций с базой во время работы'
                       ' приложения (создание, редактирование, чтение записей)'
        }
    },
    'extensions': ["uuid-ossp"]
}