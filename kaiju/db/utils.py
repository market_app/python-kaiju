import logging

import asyncpgsa
from asyncpgsa.connection import _dialect, SAConnection

from kaiju.json import encoder, decoder
from kaiju.services import ContextableService
from .roles import *
from .tables import *

LOGGER = logging.getLogger(__name__)

#
def _encoder(value):
    return value

async def _init_connection(connection: SAConnection):
    await connection.set_type_codec('jsonb', encoder=encoder, decoder=decoder, schema='pg_catalog')


async def _init_pool(credentials, settings, logger=LOGGER):
    logger.debug('подготовка пула коннектов')

    _dialect._json_serializer = _encoder
    _dialect._json_deserializer = decoder
    try:
        new_pool = await asyncpgsa.create_pool(
            **credentials,
            **settings,
            init=_init_connection,
            dialect=_dialect,
        )
    except Exception as err:
        logger.error('не удалось инициализировать базу: %s', err)
    else:
        return new_pool


async def _init_root_pool(db_name: str, db_settings, logger=LOGGER):
    settings = dict(db_settings["pool"])
    settings['database'] = db_name
    settings['min_size'] = 1
    settings['max_size'] = 1

    return await _init_pool(credentials=db_settings["users"]["root"]["credentials"], settings=settings, logger=logger)


async def _create_db_if_not_exists(db_settings, logger=LOGGER):
    async with await _init_root_pool('postgres', db_settings, logger=logger) as root_pool:
        name = db_settings["pool"]['database']

        logger.debug('проверяет базу %s', name)

        async with root_pool.acquire() as conn:
            if not await check_db_exists(conn, name):
                logger.info('создает базу %s', name)

                await create_db(conn, name)


async def _create_roles(connection, logger=LOGGER):
    logger.debug('проверка групп базы')

    for role in ROLES:
        logger.debug('проверка группы %s', role.name)
        # try:
        if not await check_role_exists(connection, role.name):
            logger.info('создает группу %s', role.name)
            await role.init_script(connection, role.name)
        else:
            logger.debug('группа %s в порядке', role.name)
        # except Exception as err:
        #     raise RuntimeError('ошибка во время создания ролей пользователей') from err


async def _create_users(connection, db_settings, logger=LOGGER):
    logger.debug('проверка пользователей базы')

    db_users = db_settings["users"]

    for user, user_settings in db_users.items():
        if user != 'root':
            logger.debug('проверка пользователя %s', user)
            credentials = user_settings['credentials']
            try:
                if not await check_role_exists(connection, credentials['user']):
                    logger.info('создает пользователя %s', user)

                    await create_user(
                        connection, credentials['user'],
                        credentials['password'],
                        role=user_settings['role'])
                else:
                    logger.debug('пользователь %s в порядке', user)

            except Exception as err:
                raise RuntimeError(
                    'ошибка при создании пользователя %s в базе данных: %s'
                    % (user, err)) from err


async def _init_db(connection, model, logger=LOGGER):
    logger.debug('проверяет таблицы в postgres')
    not_created_tables = []

    for table in model.metadata.sorted_tables:
        logger.debug('проверяет таблицу %s', table.name)
        try:

            if not await check_table_exists(connection, table):
                not_created_tables.append(table)
                continue
            else:
                logger.debug('таблица %s в порядке', table.name)

            for exc in await check_table_health(connection, table):
                logger.error(exc)

        except Exception as err:
            logger.error('ошибка инициализации таблицы %s: %s', table.name, err)
            raise err

    if not not_created_tables:
        return

    logger.info('создаёт таблицы %s', str([table.name for table in not_created_tables]))

    for table in not_created_tables:
        logger.info('добавляет таблицу %s', table)

        await connection.execute(
            dump_sql(model.metadata.create_all, bind=True)(tables=[table])
        )

        for exc in await check_table_health(connection, table):
            logger.error(exc)


async def _create_extensions(connection: SAConnection, extensions: list):
    for ext in extensions:
        extension = await connection.fetchrow(f"""SELECT 1 FROM pg_extension WHERE extname='{ext}';""")
        if not extension:
            await connection.execute(f"""CREATE EXTENSION "{ext}";""")


async def _create_users_and_tables(db_settings, model, logger=LOGGER):
    async with await _init_root_pool(db_settings["pool"]['database'], db_settings, logger=logger) as root_pool:
        async with root_pool.acquire() as conn:
            await _create_roles(conn, logger=logger)

            await _create_users(conn, db_settings, logger=logger)
            await _create_extensions(conn, db_settings.get("extensions", []))
            await _init_db(conn, model, logger=logger)


async def db_ctx(app):
    db_settings = app.db_settings
    await _create_db_if_not_exists(db_settings, app.logger)
    await _create_users_and_tables(db_settings, model=app.model, logger=app.logger)

    user = db_settings["settings"]["user"]

    db_pool = await _init_pool(
        credentials=db_settings['users'][user]["credentials"],
        settings=db_settings["pool"],
        logger=app.logger
    )
    app.db = db_pool
    app["db"] = db_pool
    yield
    await app.db.close()


class DatabaseService(ContextableService):
    """A simple database pool initialization service."""

    service_name = 'db'

    def __init__(self, app=None, logger=None, **settings):
        super().__init__(app=app, logger=logger)
        self._settings = settings
        self._pool = None

    def __getattr__(self, item):
        if self._pool is not None:
            return getattr(self._pool, item)
        else:
            return super().__getattribute__(item)

    async def init(self):
        await _create_db_if_not_exists(self._settings, self.logger)
        await _create_users_and_tables(self._settings, model=self.app.model,
                                       logger=self.logger)
        user = self._settings["settings"]["user"]
        self._pool = await _init_pool(
            credentials=self._settings['users'][user]["credentials"],
            settings=self._settings["pool"],
            logger=self.logger)

    async def close(self):
        await self._pool.close()

    @property
    def closed(self) -> bool:
        return self._pool is None
