import pytest

from ..service import LdapService
from .fixtures import *


@pytest.mark.integration
async def test_ldap_service(ldap_test_settings, logger):
    async with LdapService(app=None, **ldap_test_settings, logger=logger) as ldap:
        pass
