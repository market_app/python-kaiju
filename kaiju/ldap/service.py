import asyncio
from concurrent.futures import ThreadPoolExecutor

import ldap.asyncsearch
import ldap

from kaiju.services import ContextableService


class LdapService(ContextableService):

    def __init__(
            self, app, host: str, user: str = None,
            password: str = None, logger=None):
        """
        :param app:
        :param host:
        :param user:
        :param password:
        :param logger:
        """

        super().__init__(app=app, logger=logger)
        self._loop = asyncio.get_event_loop()

        self._host = host
        self._user = user
        self._password = password

        self._connection = None

    async def init(self):
        await self._connect()

    async def close(self):
        self._connection = None

    @property
    def closed(self) -> bool:
        return self._connection is None

    async def authorize(self, username: str, password: str):
        self.logger.debug('Checking user "%s".', username)
        with ThreadPoolExecutor(max_workers=1) as tp:
            await self._loop.run_in_executor(
                tp, self._connection.simple_bind_s, username, password)
        self.logger.info('User "%s" logged in.', username)

    async def _connect(self):
        self.logger.debug('Connecting to "%s" as "%s".', self._host, self._user)
        with ThreadPoolExecutor(max_workers=1) as tp:
            self._connection = await self._loop.run_in_executor(
                tp, ldap.initialize, self._host)
            if self._user:
                await self._loop.run_in_executor(
                    tp, self._connection.simple_bind_s, self._user, self._password)
        self.logger.info('Connected to "%s" as "%s".', self._host, self._user)
