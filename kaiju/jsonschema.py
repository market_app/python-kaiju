"""JSONSchema tempates and shortcuts.

See `def validate` if you want to wrap your function with a validator.
"""

import abc
import inspect
from typing import Union, Dict, List, Collection

import fastjsonschema
from fastjsonschema.exceptions import JsonSchemaException

from kaiju.abc import Serializable
from kaiju.rest.exceptions import ValidationError

__all__ = [

    'validate', 'custom_formatters',

    # types

    'JSONSchemaObject', 'Enumerated',
    'String', 'Number', 'Integer', 'Array', 'Object', 'Generic',

    # keywords

    'JSONSchemaKeyword', 'AnyOf', 'OneOf', 'AllOf', 'Not',

    # aliases

    'GUID', 'Date', 'DateTime', 'Time', 'Null', 'Constant',

]


class JSONSchemaObject(Serializable, abc.ABC):
    """You shouldn't use this type directly. Instead you can use on of the
    following non-abstract types."""

    __slots__ = ('default', 'title', 'description', 'examples', 'enum', 'nullable')

    def __init__(
            self, title: str = None, description: str = None, default=None,
            examples: list = None, enum: list = None, nullable: bool = False):
        self.default = default
        self.title = title
        self.description = description
        self.examples = examples
        self.enum = enum
        self.nullable = nullable

    def repr(self) -> dict:
        """Here should be your object JSON Schema definition."""

        return {
            key: getattr(self, key, None)
            for key in self.__slots__
            if not key.startswith('_') and getattr(self, key, None) is not None
        }


class Boolean(JSONSchemaObject):
    """Boolean `True` or `False` are accepted."""

    __slots__ = ('default', 'title', 'description', 'nullable')

    def __init__(self, title: str = None, description: str = None, default=None, nullable=False):
        """
        :param default: optional default value for non-existing params
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        """

        self.default = default
        self.title = title
        self.description = description
        self.nullable = nullable

    def repr(self) -> dict:
        return {
            'type': 'boolean',
            **super().repr()
        }


class Enumerated(JSONSchemaObject):
    """Value can be one from the list. Use `Enumerated` type if you need to
    have different data types in one enum. Otherwise it's recommended to use
    a specific data type with `enum=` argument passed into it."""

    __slots__ = ('default', 'title', 'description', 'enum')

    def __init__(self, enum: list, title: str = None, description: str = None, default=None):
        """
        :param enum: required list of allowed values
        :param default: optional default value for non-existing params
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        """

        self.default = default
        self.title = title
        self.description = description
        self.enum = enum


class String(JSONSchemaObject):
    """Text/string data type."""

    STRING_FORMATS = frozenset({
        'date-time', 'time', 'date',
        'email', 'idn-email', 'hostname', 'idn-hostname', 'ipv4', 'ipv6',
        'uri', 'uri-reference', 'iri', 'iri-reference', 'regex'
    })

    __slots__ = tuple([
        *JSONSchemaObject.__slots__, 'minLength', 'maxLength', 'pattern', 'format',
    ])

    def __init__(
            self, title: str = None, description: str = None, default: str = None,
            examples: List[str] = None, enum: List[str] = None, minLength: int = None,
            maxLength: int = None, pattern: str = None, format: str = None,
            nullable=None):
        """
        :param default: optional default value for non-existing params
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        :param examples: optional list of examples
        :param enum: optional list of allowed values for this parameter
        :param minLength: optional minimum string length
        :param maxLength: optional maximum string length
        :param pattern: optional regex validation pattern
        :param format: optional specific string format (see `String.STRING_FORMATS`)
        """

        super().__init__(
            title=title, description=description, default=default,
            examples=examples, enum=enum, nullable=nullable)
        self.minLength = minLength
        self.maxLength = maxLength
        self.pattern = pattern
        if format and format not in self.STRING_FORMATS:
            raise JsonSchemaException(
                'Invalid string format "%s".'
                'Must be one of: "%s".' % (format, list(self.STRING_FORMATS)))
        self.format = format

    def repr(self) -> dict:
        return {
            'type': 'string',
            **super().repr()
        }


class DateTime(JSONSchemaObject):
    """Datetime object alias."""

    __slots__ = tuple([*JSONSchemaObject.__slots__, 'format'])

    def __init__(self, *args, **kws):
        super().__init__(*args, **kws)
        self.format = 'date-time'


class Date(JSONSchemaObject):
    """Datetime object alias."""

    __slots__ = tuple([*JSONSchemaObject.__slots__, 'format'])

    def __init__(self, *args, **kws):
        super().__init__(*args, **kws)
        self.format = 'date'


class Time(JSONSchemaObject):
    """Datetime object alias."""

    __slots__ = tuple([*JSONSchemaObject.__slots__, 'format'])

    def __init__(self, *args, **kws):
        super().__init__(*args, **kws)
        self.format = 'date'


class GUID(JSONSchemaObject):
    """UUID object alias."""

    __slots__ = tuple([*JSONSchemaObject.__slots__, 'format'])

    def __init__(self, *args, **kws):
        super().__init__(*args, **kws)
        self.format = 'uuid'


class Constant(Enumerated):
    """Value is a predefined constant."""

    __slots__ = tuple([*Enumerated.__slots__, 'type'])

    def __init__(self, const, title: str = None, description: str = None, type: str = None):
        """
        :param const: required value of a constant
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        """

        super().__init__(enum=[const], title=title, description=description)
        self.type = type


class _Null(Constant):

    __slots__ = Constant.__slots__

    def __init__(self):
        super().__init__(const=None, title=None, description=None)


Null = _Null()


class Number(JSONSchemaObject):
    """Numeric datatype (use it for floats)."""

    __slots__ = tuple([
        *JSONSchemaObject.__slots__, 'multipleOf', 'minimum', 'exclusiveMinimum',
        'maximum', 'exclusiveMaximum'
    ])

    def __init__(
            self, title: str = None, description: str = None, default: float = None,
            examples: List[float] = None, enum: List[float] = None,
            multipleOf: float = None,
            minimum: float = None, maximum: float = None,
            exclusiveMinimum: float = None,
            exclusiveMaximum: float = None, nullable=None):
        """
        :param default: optional default value for non-existing params
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        :param examples: optional list of examples
        :param enum: optional list of allowed values for this parameter
        :param multipleOf: optional divider of the value
        :param minimum: optional minimum value
        :param maximum: optional maximum value
        :param exclusiveMinimum: optional
        :param exclusiveMaximum: optional
        """

        super().__init__(
            title=title, description=description, default=default,
            examples=examples, enum=enum, nullable=nullable)
        self.multipleOf = multipleOf
        self.minimum = minimum
        self.maximum = maximum
        self.exclusiveMinimum = exclusiveMinimum
        self.exclusiveMaximum = exclusiveMaximum

    def repr(self) -> dict:
        return {
            'type': 'number',
            **super().repr()
        }


class Integer(Number):

    __slots__ = Number.__slots__

    def __init__(
            self, title: str = None, description: str = None, default: int = None,
            examples: List[int] = None, enum: List[int] = None,
            multipleOf: int = None, minimum: int = None, maximum: int = None,
            exclusiveMinimum: int = None,
            exclusiveMaximum: int = None, nullable=None):
        """
        :param default: optional default value for non-existing params
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        :param examples: optional list of examples
        :param enum: optional list of allowed values for this parameter
        :param multipleOf: optional divider of the value
        :param minimum: optional minimum value
        :param maximum: optional maximum value
        :param exclusiveMinimum: optional
        :param exclusiveMaximum: optional
        """

        super().__init__(
            title=title, description=description, default=default,
            examples=examples, enum=enum, multipleOf=multipleOf,
            minimum=minimum, maximum=maximum, exclusiveMinimum=exclusiveMinimum,
            exclusiveMaximum=exclusiveMaximum, nullable=nullable)

    def repr(self) -> dict:
        return {
            'type': 'integer',
            **super().repr()
        }


class Array(JSONSchemaObject):
    """A list, set or tuple definition."""

    __slots__ = tuple([
        *JSONSchemaObject.__slots__, 'items', 'contains',
        'additionalItems', 'uniqueItems', 'minItems', 'maxItems'
    ])

    def __init__(
            self, items: Union[JSONSchemaObject, Collection[JSONSchemaObject], dict, Collection[dict]] = None,
            contains: Union[dict, JSONSchemaObject] = None, title: str = None,
            description: str = None, examples: list = None, additionalItems: bool = None,
            uniqueItems: bool = None, minItems: int = None, maxItems: int = None,
            nullable=None):
        """
        :param items: optional schema of contained items, you can pass a list
            for a tuple schema definition
        :param contains: optional condition on item that must present in the array
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        :param examples: optional list of examples
        :param additionalItems: optional defines if additional items are allowed
        :param uniqueItems: optional defines if only unique items are allowed
        :param minItems: optional min length of an array
        :param maxItems: optional max length of an array
        """

        super().__init__(
            title=title, description=description, default=None,
            examples=examples, enum=None, nullable=nullable)
        if isinstance(items, Collection):
            items = list(items)

        if items:
            if isinstance(items, JSONSchemaObject):
                self.items = items.repr()
            elif isinstance(items, dict):
                self.items = items
            elif isinstance(items, Collection):
                self.items = []
                for item in items:
                    if isinstance(item, JSONSchemaObject):
                        item = item.repr()
                    self.items.append(item)
        else:
            self.items = None

        if contains:
            if isinstance(contains, JSONSchemaObject):
                self.contains = contains.repr()
            elif isinstance(items, dict):
                self.contains = contains
        else:
            self.contains = None

        self.additionalItems = additionalItems
        self.uniqueItems = uniqueItems
        self.minItems = minItems
        self.maxItems = maxItems

    def repr(self) -> dict:
        return {
            'type': 'array',
            **super().repr(),
        }


class Object(JSONSchemaObject):
    """A JSON object (dictionary) definition."""

    __slots__ = tuple([
        *JSONSchemaObject.__slots__,
        'properties', 'propertyNames', 'required', 'patternProperties',
        'additionalProperties', 'minProperties', 'maxProperties'
    ])

    def __init__(
            self, properties: Union[Dict[str, JSONSchemaObject], Dict[str, dict]] = None,
            patternProperties: Union[Dict[str, JSONSchemaObject], Dict[str, dict]] = None,
            propertyNames: dict = None,
            additionalProperties: bool = None, minProperties: int = None,
            maxProperties: int = None, required: List[str] = None,
            title: str = None, enum=None,
            description: str = None, default=None,
            examples: list = None, nullable=None, **kws):
        """
        :param properties: optional field definitions
        :param patternProperties: optional field pattern definitions (can validate
            multiple fields by regex name patterns)
        :param propertyNames:
        :param contains: optional condition on item that must present in the array
        :param title: optional parameter title doc
        :param description: optional parameter description doc
        :param examples: optional list of examples
        :param additionalItems: optional defines if additional items are allowed
        :param uniqueItems: optional defines if only unique items are allowed
        :param minItems: optional min length of an array
        :param maxItems: optional max length of an array
        :param kws: you can pass properties as keyword args as well
        """

        super().__init__(
            title=title, description=description, default=default,
            examples=examples, enum=enum, nullable=nullable)

        _properties = {}
        if kws:
            _properties.update(kws)
        if properties:
            _properties.update(properties)

        if _properties:
            self.properties = {}
            for key, value in _properties.items():
                if isinstance(value, JSONSchemaObject):
                    value = value.repr()
                self.properties[key] = value
        else:
            self.properties = None

        if patternProperties:
            self.patternProperties = {}
            for key, value in properties.items():
                if isinstance(value, JSONSchemaObject):
                   value = value.repr()
                self.patternProperties[key] = value
        else:
            self.patternProperties = None

        if propertyNames is not None and 'pattern' not in propertyNames:
            raise JsonSchemaException('propertyNames param must have "pattern" attribute.')
        self.propertyNames = propertyNames

        self.additionalProperties = additionalProperties
        self.minProperties = minProperties
        self.maxProperties = maxProperties

        self.required = required

    def repr(self) -> dict:
        return {
            'type': 'object',
            **super().repr(),
        }


class Generic(JSONSchemaObject):
    """Use this for specific or partial conditions. It's recommended to use
    specific types instead of the generic one when possible."""

    def __init__(
            self, title: str = None, description: str = None, default=None,
            examples: list = None, enum: list = None, nullable=None, **kws):
        super().__init__(
            title=title, description=description, default=default,
            examples=examples, enum=enum, nullable=nullable)
        for key, value in kws.items():
            setattr(self, key, value)


class JSONSchemaKeyword(JSONSchemaObject, abc.ABC):
    """Abstract class for JSON Schema specific logical keywords."""

    __slots__ = tuple([*JSONSchemaObject.__slots__, '_items'])

    def __init__(
            self, *items: Union[JSONSchemaObject, dict],
            title: str = None, description: str = None, default=None,
            examples: list = None, enum: list = None, nullable=None):

        super().__init__(
            title=title, description=description, default=default,
            examples=examples, enum=enum, nullable=nullable)

        if items:
            self._items = []
            for item in items:
                if isinstance(item, JSONSchemaObject):
                    item = item.repr()
                self._items.append(item)
        else:
            raise JsonSchemaException('JSON Schema keyword items must present')


class AnyOf(JSONSchemaKeyword):
    """The given data must be valid against any (one or more) of the given subschemas."""

    __slots__ = JSONSchemaKeyword.__slots__

    def repr(self) -> dict:
        return {
            **super().repr(),
            'anyOf': self._items
        }


class OneOf(JSONSchemaKeyword):
    """The given data must be valid against exactly one of the given subschemas."""

    __slots__ = JSONSchemaKeyword.__slots__

    def repr(self) -> dict:
        return {
            **super().repr(),
            'oneOf': self._items
        }


class AllOf(JSONSchemaKeyword):
    """The given data must be valid against all of the given subschemas."""

    __slots__ = JSONSchemaKeyword.__slots__

    def repr(self) -> dict:
        return {
            **super().repr(),
            'allOf': self._items
        }


class Not(JSONSchemaKeyword):
    """Reverse the condition."""

    __slots__ = ('_item',)

    def __init__(self, item: Union[JSONSchemaObject, dict]):
        """
        :param items: condition to be reversed
        """

        if isinstance(item, JSONSchemaObject):
            self._item = item.repr()
        else:
            self._item = item

    def repr(self):
        return {
            'not': self._item
        }


custom_formatters = {
    'uuid': r'^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}$'
}


def validate(schema=None, **kws):
    """JSONSchema validator decorator.

    Usage examples:

    >>> @validate(Object(properties={'name': String(), 'value': Integer(minimum=41)}))
    ... def test(name, value=None):
    ...     pass
    >>> test('a', value=42)
    >>> test('a', value=40)
    Traceback (most recent call last):
     ...
    kaiju.rest.exceptions.ValidationError: data.value must be bigger than or equal to 41

    >>> @validate(Object(properties={'i': Array(items=Integer(multipleOf=10))}))
    ... def test(i):
    ...     return
    >>> test([100, 200, 300])
    >>> test([])
    >>> test([100, 200, 117])
    Traceback (most recent call last):
     ...
    kaiju.rest.exceptions.ValidationError: data.i[2] must be multiple of 10

    >>> @validate(Object(properties={'i': OneOf(String(), Integer(minimum=1))}))
    ... def test(i):
    ...     return
    >>> test(5)
    >>> test('shi...')
    >>> test(-1)
    Traceback (most recent call last):
     ...
    kaiju.rest.exceptions.ValidationError: data.i must be valid exactly by one of oneOf definition

    Parameter based syntax is allowed with limited capabilities:

    >>> @validate(name=String(minLength=3), value=Integer())
    ... def test(name, value):
    ...     return
    >>> test('test', 1)
    >>> test(name='a', value=10)
    Traceback (most recent call last):
     ...
    kaiju.rest.exceptions.ValidationError: data.name must be longer than or equal to 3 characters

    :param validator: may be an already compiled function (any) or a dict
        object compatible with JSONSchema specification.
    """

    if schema:
        if isinstance(schema, JSONSchemaObject):
            schema = schema.repr()
    else:
        schema = Object(properties=kws).repr()

    validator = fastjsonschema.compile(schema, formats=custom_formatters)

    def _schema(f):

        arg_names = inspect.getfullargspec(f).args

        def _wrapper(*args, **kws):
            _args = dict(zip(arg_names, args))
            kws.update(_args)
            try:
                kws = validator(kws)
            except JsonSchemaException as exc:
                raise ValidationError(
                    msg=str(exc), violates=exc.rule,
                    key=exc.name, value=exc.value, expected=exc.rule_definition,
                    schema=exc.definition)
            else:
                return f(**kws)

        _wrapper.__func__ = f
        f.__json_schema__ = _wrapper.__json_schema__ = schema

        return _wrapper

    return _schema
